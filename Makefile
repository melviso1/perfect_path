# definitions

CC = 
CXX = g++
AR = ar
LINK = g++
CPPFLAGS = -std=c++1z  -Iinclude  -O0  -g3 
SRC = src
LINKFLAGS = 

all : perfect_path

perfect_path: serial.runtime.o serial.type_record.o serial.type_map.o serial.oxstream.o serial.ixstream.o kernel.step.o kernel.symbol.o kernel.link.o kernel.level.o kernel.iterated_step.o kernel.meta.level_digest.o char_symbol.o alphabet.o text_path.o main.o 
	$(CXX) serial.runtime.o serial.type_record.o serial.type_map.o serial.oxstream.o serial.ixstream.o kernel.step.o kernel.symbol.o kernel.link.o kernel.level.o kernel.iterated_step.o kernel.meta.level_digest.o char_symbol.o alphabet.o text_path.o main.o  $(LINKFLAGS) -o perfect_path

serial.runtime.o: src/serial.runtime.cpp include/serial.runtime.h
	$(CXX) $(CPPFLAGS) -c src/serial.runtime.cpp -o serial.runtime.o

serial.type_record.o: src/serial.type_record.cpp include/serial.type_record.h
	$(CXX) $(CPPFLAGS) -c src/serial.type_record.cpp -o serial.type_record.o

serial.type_map.o: src/serial.type_map.cpp include/serial.type_map.h
	$(CXX) $(CPPFLAGS) -c src/serial.type_map.cpp -o serial.type_map.o

serial.type_instance.o: src/serial.type_instance.cpp include/serial.type_instance.h
	$(CXX) $(CPPFLAGS) -c src/serial.type_instance.cpp -o serial.type_instance.o

serial.runtime_instance.o: src/serial.runtime_instance.cpp include/serial.runtime_instance.h
	$(CXX) $(CPPFLAGS) -c src/serial.runtime_instance.cpp -o serial.runtime_instance.o

serial.oxstream.o: src/serial.oxstream.cpp include/serial.oxstream.h
	$(CXX) $(CPPFLAGS) -c src/serial.oxstream.cpp -o serial.oxstream.o

serial.ixstream.o: src/serial.ixstream.cpp include/serial.ixstream.h
	$(CXX) $(CPPFLAGS) -c src/serial.ixstream.cpp -o serial.ixstream.o

kernel.step.o: src/kernel.step.cpp include/kernel.step.h
	$(CXX) $(CPPFLAGS) -c src/kernel.step.cpp -o kernel.step.o

kernel.symbol.o: src/kernel.symbol.cpp include/kernel.symbol.h
	$(CXX) $(CPPFLAGS) -c src/kernel.symbol.cpp -o kernel.symbol.o

kernel.link.o: src/kernel.link.cpp include/kernel.link.h
	$(CXX) $(CPPFLAGS) -c src/kernel.link.cpp -o kernel.link.o

kernel.level.o: src/kernel.level.cpp include/kernel.level.h
	$(CXX) $(CPPFLAGS) -c src/kernel.level.cpp -o kernel.level.o

kernel.iterated_step.o: src/kernel.iterated_step.cpp include/kernel.iterated_step.h
	$(CXX) $(CPPFLAGS) -c src/kernel.iterated_step.cpp -o kernel.iterated_step.o

kernel.meta.level_digest.o: src/kernel.meta.level_digest.cpp include/kernel.meta.level_digest.h
	$(CXX) $(CPPFLAGS) -c src/kernel.meta.level_digest.cpp -o kernel.meta.level_digest.o

char_symbol.o: src/char_symbol.cpp include/char_symbol.h
	$(CXX) $(CPPFLAGS) -c src/char_symbol.cpp -o char_symbol.o

alphabet.o: src/alphabet.cpp include/alphabet.h
	$(CXX) $(CPPFLAGS) -c src/alphabet.cpp -o alphabet.o

text_path.o: src/text_path.cpp include/text_path.h
	$(CXX) $(CPPFLAGS) -c src/text_path.cpp -o text_path.o

main.o: src/main.cpp include/main.h
	$(CXX) $(CPPFLAGS) -c src/main.cpp -o main.o

clean:
	rm -f *.o perfect_path

.PHONY: clean all

