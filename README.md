# perfect_path

This is a litte project for sequencial contextualization chatbot.

# Testing

Use the provide binary files for testing (Windows / Linux)
Take account that current learning_data is small and only trained in spanish.

# More information on
    * https://canal-arte.net/node/5
    * https://emos.dev/alice-automatas

[def]: https://vimeo.com/833106012/3f03a490a1?embedded=true&source=vimeo_logo&owner=201684639

[![Watch the video](https://i.vimeocdn.com/video/1679350533-c402cde0f5485be306d205a2622dc1b61954e12094ece151c77798f1ffc5147c-d?mw=1200&mh=675)][def]


