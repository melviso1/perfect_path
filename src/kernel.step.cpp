// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.step.begin}
// {user.before.class.step.end}



void kernel::step::__init__(kernel::level* ptr_level, kernel::symbol* ptr_symbol)
{
    //init from relations:
    
        _first_to_link = nullptr;
        _last_to_link = nullptr;
        _count_to_link = 0;
        
    
        _first_from_link = nullptr;
        _last_from_link = nullptr;
        _count_from_link = 0;
        
    //init to relations:
    
        //multi_owned_passive level -> step
        {
            assert(this);
            assert(ptr_level);
            ptr_level->_count_step++;
        
            _ref_level = ptr_level;
        
            if (ptr_level->_last_step)
            {
                _next_level = nullptr;
                _prev_level = ptr_level->_last_step;
                _prev_level->_next_level = this;
                ptr_level->_last_step = this;
            }
            else
            {
                _prev_level = nullptr;
                _next_level = nullptr;
                ptr_level->_first_step = ptr_level->_last_step = this;
            }
        }
        
    
    //single_passive active_level -> active_step
    _ref_active_level = nullptr;
    
    
    //uniquevaluetree_owned_passive symbol -> step
    assert(this);
    _ref_symbol = nullptr;
    _parent_symbol = nullptr;
    _left_symbol = nullptr;
    _right_symbol = nullptr;
    assert(ptr_symbol);
    ptr_symbol->add_step(this);
}


void kernel::step::__exit__()
{
    //init from relations:
    
        {
            for (kernel::link* item = get_first_to_link(); item; item = get_first_to_link())
            {
                delete item;
            }
        }
        
    
        {
            for (kernel::link* item = get_first_from_link(); item; item = get_first_from_link())
            {
                delete item;
            }
        }
        
    //init to relations:
    
        assert(this);
        if ( _ref_level != nullptr )
        {
            _ref_level->remove_step(this);
        }
        
    
        assert(this);
        if (_ref_active_level)
        {
            assert(_ref_active_level->_ref_active_step == this);
            _ref_active_level->_ref_active_step = nullptr;
            _ref_active_level = nullptr;
        }
        
    
        if (_ref_symbol)
        {
            _ref_symbol->remove_step(this);
        }
        
}
/**
* Constructor.
**/
kernel::step::step(const serial::type_record* _)
:    serial::runtime{}
,    serial::runtime_instance<step>{}
,    _ref_level{nullptr}
,    _prev_level{nullptr}
,    _next_level{nullptr}
,    _ref_active_level{nullptr}
,    _ref_symbol{nullptr}
,    _parent_symbol{nullptr}
,    _left_symbol{nullptr}
,    _right_symbol{nullptr}
,    _first_to_link{nullptr}
,    _last_to_link{nullptr}
,    _count_to_link{0}
,    _first_to_link_iterator{nullptr}
,    _last_to_link_iterator{nullptr}
,    _first_from_link{nullptr}
,    _last_from_link{nullptr}
,    _count_from_link{0}
,    _first_from_link_iterator{nullptr}
,    _last_from_link_iterator{nullptr}
,    _activation_count{0}
,    _success_predictor{0}
,    _max_activation_count{0}
,    _backward_count{0}
{
}

/**
* Constructor.
**/
kernel::step::step(kernel::level* ptr_level, kernel::symbol* ptr_symbol)
:    serial::runtime{}
,    serial::runtime_instance<step>{}
,    _ref_level{nullptr}
,    _prev_level{nullptr}
,    _next_level{nullptr}
,    _ref_active_level{nullptr}
,    _ref_symbol{nullptr}
,    _parent_symbol{nullptr}
,    _left_symbol{nullptr}
,    _right_symbol{nullptr}
,    _first_to_link{nullptr}
,    _last_to_link{nullptr}
,    _count_to_link{0}
,    _first_to_link_iterator{nullptr}
,    _last_to_link_iterator{nullptr}
,    _first_from_link{nullptr}
,    _last_from_link{nullptr}
,    _count_from_link{0}
,    _first_from_link_iterator{nullptr}
,    _last_from_link_iterator{nullptr}
,    _activation_count{0}
,    _success_predictor{0}
,    _max_activation_count{0}
,    _backward_count{0}
{
    	__init__( ptr_level, ptr_symbol );
}



void kernel::step::add_to_link_first(kernel::link* item)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_from_step == nullptr);
        _count_to_link++;
        item->_ref_from_step = this;
        if (_first_to_link)
        {
            _first_to_link->_prev_from_step = item;
            item->_next_from_step = _first_to_link;
            _first_to_link = item;
        }
        else
        {
            _first_to_link = _last_to_link = item;
        }
}


void kernel::step::add_to_link_last(kernel::link* item)
{
    
        assert(this);
        assert(item);
        assert(item->_ref_from_step == nullptr);
        _count_to_link++;
        item->_ref_from_step = this;
        if (_last_to_link)
        {
            _last_to_link->_next_from_step = item;
            item->_prev_from_step = _last_to_link;
            _last_to_link = item;
        }
        else
        {
           _first_to_link = _last_to_link = item;
        }
}


void kernel::step::add_to_link_after(kernel::link* item, kernel::link* pos)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_from_step == nullptr);
    
        assert(pos);
        assert(pos->_ref_from_step == this);
    
        _count_to_link++;
    
        item->_ref_from_step = this;
        item->_prev_from_step = pos;
        item->_next_from_step = pos->_next_from_step;
        pos->_next_from_step  = item;
    
        if (item->_next_from_step)
        {
            item->_next_from_step->_prev_from_step = item;
        }
        else
        {
            _last_to_link = item;
        }
}


void kernel::step::add_to_link_before(kernel::link* item, kernel::link* pos)
{
    
        assert( this );
    
        assert( item );
        assert( item->_ref_from_step == nullptr );
    
        assert( pos );
        assert(pos->_ref_from_step == this);
    
        _count_to_link++;
    
        item->_ref_from_step = this;
        item->_next_from_step = pos;
        item->_prev_from_step = pos->_prev_from_step;
        pos->_prev_from_step  = item;
    
        if (item->_prev_from_step)
        {
            item->_prev_from_step->_next_from_step = item;
        }
        else
        {
            _first_to_link = item;
        }
}


void kernel::step::remove_to_link(kernel::link* item)
{
    
    
        assert(this);
    
        assert(item);
        assert(item->_ref_from_step == this);
    
        if(  _first_to_link_iterator != nullptr )
        {
            _first_to_link_iterator->check(item);
        }
    
        _count_to_link--;
    
        if (item->_next_from_step)
        {
            item->_next_from_step->_prev_from_step = item->_prev_from_step;
        }
        else
        {
            _last_to_link = item->_prev_from_step;
        }
    
        if (item->_prev_from_step)
        {
            item->_prev_from_step->_next_from_step = item->_next_from_step;
        }
        else
        {
            _first_to_link = item->_next_from_step;
        }
    
        item->_prev_from_step = nullptr;
        item->_next_from_step = nullptr;
        item->_ref_from_step = nullptr;
}


void kernel::step::replace_to_link(kernel::link* item, kernel::link* new_item)
{
    
        assert(this);
        assert(item);
        assert(item->_ref_from_step == this);
    
        assert(new_item);
        assert(new_item->_ref_from_step == nullptr);
    
    
        if ( _first_to_link_iterator != nullptr )
        {
            _first_to_link_iterator->check(item, new_item);
        }
    
        if ( item->_next_from_step != nullptr )
        {
            item->_next_from_step->_prev_from_step = new_item;
        }
        else
        {
            _last_to_link = new_item;
        }
        if ( item->_prev_from_step != nullptr )
        {
            item->_prev_from_step->_next_from_step = new_item;
        }
        else
        {
            _first_to_link = new_item;
        }
    
        new_item->_next_from_step = item->_next_from_step;
        new_item->_prev_from_step = item->_prev_from_step;
        item->_next_from_step = nullptr;
        item->_prev_from_step = nullptr;
    
        item->_ref_from_step = nullptr;
        new_item->_ref_from_step = this;
}


void kernel::step::delete_all_to_link()
{
    
        assert(this);
        for (kernel::link* item = get_first_to_link(); item; item = get_first_to_link())
        {
              delete item;
        }
}


kernel::link* kernel::step::get_first_to_link() const
{
    
        assert(this);
        return _first_to_link;
}


kernel::link* kernel::step::get_last_to_link() const
{
    
        assert(this);
        return _last_to_link;
}


kernel::link* kernel::step::get_next_to_link(kernel::link* pos) const
{
    
        assert(this);
        if ( pos == nullptr )
        {
            return _first_to_link;
        }
        assert(pos);
        assert(pos->_ref_from_step == this);
        return pos->_next_from_step;
}


kernel::link* kernel::step::get_prev_to_link(kernel::link* pos) const
{
    
        assert(this);
    
        if ( pos == nullptr )
        {
            return _last_to_link;
        }
    
        assert(pos);
        assert(pos->_ref_from_step == this);
        return pos->_prev_from_step;
}


size_t kernel::step::get_to_link_count() const
{
    
        assert(this);
        return _count_to_link;
}


void kernel::step::move_to_link_first(kernel::link* item)
{
    
        assert(item);
        assert(item->_ref_from_step);
        item->_ref_from_step->remove_to_link(item);
        add_to_link_first(item);
}


void kernel::step::move_to_link_last(kernel::link* item)
{
    
        assert(item);
        assert(item->_ref_from_step);
        item->_ref_from_step->remove_to_link(item);
        add_to_link_last(item);
}


void kernel::step::move_to_link_after(kernel::link* item, kernel::link* pos)
{
    
        assert(item);
        assert(item->_ref_from_step);
        item->_ref_from_step->remove_to_link(item);
        add_to_link_after(item, pos);
}


void kernel::step::move_to_link_before(kernel::link* item, kernel::link* pos)
{
    
        assert(item);
        assert(item->_ref_from_step);
        item->_ref_from_step->remove_to_link(item);
        add_to_link_before(item, pos);
        
}


void kernel::step::sort_to_link(int (*compare)(kernel::link*,kernel::link*))
{
    
    
        for (kernel::link* a = get_first_to_link(); a != nullptr ; a = get_next_to_link(a))
        {
            kernel::link* b = get_next_to_link(a);
    
            while ( b != nullptr && compare(a, b) > 0 )
            {
                kernel::link* c = get_prev_to_link(a);
                while ( c != nullptr  && compare(c, b) > 0 )
                    c = get_prev_to_link(c);
                if (c != nullptr )
                {
                    move_to_link_after(b, c);
                }
                else
                {
                    move_to_link_first(b);
                }
                b = get_next_to_link(a);
            }
        }
}


void kernel::step::add_from_link_first(kernel::link* item)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_to_step == nullptr);
        _count_from_link++;
        item->_ref_to_step = this;
        if (_first_from_link)
        {
            _first_from_link->_prev_to_step = item;
            item->_next_to_step = _first_from_link;
            _first_from_link = item;
        }
        else
        {
            _first_from_link = _last_from_link = item;
        }
}


void kernel::step::add_from_link_last(kernel::link* item)
{
    
        assert(this);
        assert(item);
        assert(item->_ref_to_step == nullptr);
        _count_from_link++;
        item->_ref_to_step = this;
        if (_last_from_link)
        {
            _last_from_link->_next_to_step = item;
            item->_prev_to_step = _last_from_link;
            _last_from_link = item;
        }
        else
        {
           _first_from_link = _last_from_link = item;
        }
}


void kernel::step::add_from_link_after(kernel::link* item, kernel::link* pos)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_to_step == nullptr);
    
        assert(pos);
        assert(pos->_ref_to_step == this);
    
        _count_from_link++;
    
        item->_ref_to_step = this;
        item->_prev_to_step = pos;
        item->_next_to_step = pos->_next_to_step;
        pos->_next_to_step  = item;
    
        if (item->_next_to_step)
        {
            item->_next_to_step->_prev_to_step = item;
        }
        else
        {
            _last_from_link = item;
        }
}


void kernel::step::add_from_link_before(kernel::link* item, kernel::link* pos)
{
    
        assert( this );
    
        assert( item );
        assert( item->_ref_to_step == nullptr );
    
        assert( pos );
        assert(pos->_ref_to_step == this);
    
        _count_from_link++;
    
        item->_ref_to_step = this;
        item->_next_to_step = pos;
        item->_prev_to_step = pos->_prev_to_step;
        pos->_prev_to_step  = item;
    
        if (item->_prev_to_step)
        {
            item->_prev_to_step->_next_to_step = item;
        }
        else
        {
            _first_from_link = item;
        }
}


void kernel::step::remove_from_link(kernel::link* item)
{
    
    
        assert(this);
    
        assert(item);
        assert(item->_ref_to_step == this);
    
        if(  _first_from_link_iterator != nullptr )
        {
            _first_from_link_iterator->check(item);
        }
    
        _count_from_link--;
    
        if (item->_next_to_step)
        {
            item->_next_to_step->_prev_to_step = item->_prev_to_step;
        }
        else
        {
            _last_from_link = item->_prev_to_step;
        }
    
        if (item->_prev_to_step)
        {
            item->_prev_to_step->_next_to_step = item->_next_to_step;
        }
        else
        {
            _first_from_link = item->_next_to_step;
        }
    
        item->_prev_to_step = nullptr;
        item->_next_to_step = nullptr;
        item->_ref_to_step = nullptr;
}


void kernel::step::replace_from_link(kernel::link* item, kernel::link* new_item)
{
    
        assert(this);
        assert(item);
        assert(item->_ref_to_step == this);
    
        assert(new_item);
        assert(new_item->_ref_to_step == nullptr);
    
    
        if ( _first_from_link_iterator != nullptr )
        {
            _first_from_link_iterator->check(item, new_item);
        }
    
        if ( item->_next_to_step != nullptr )
        {
            item->_next_to_step->_prev_to_step = new_item;
        }
        else
        {
            _last_from_link = new_item;
        }
        if ( item->_prev_to_step != nullptr )
        {
            item->_prev_to_step->_next_to_step = new_item;
        }
        else
        {
            _first_from_link = new_item;
        }
    
        new_item->_next_to_step = item->_next_to_step;
        new_item->_prev_to_step = item->_prev_to_step;
        item->_next_to_step = nullptr;
        item->_prev_to_step = nullptr;
    
        item->_ref_to_step = nullptr;
        new_item->_ref_to_step = this;
}


void kernel::step::delete_all_from_link()
{
    
        assert(this);
        for (kernel::link* item = get_first_from_link(); item; item = get_first_from_link())
        {
              delete item;
        }
}


kernel::link* kernel::step::get_first_from_link() const
{
    
        assert(this);
        return _first_from_link;
}


kernel::link* kernel::step::get_last_from_link() const
{
    
        assert(this);
        return _last_from_link;
}


kernel::link* kernel::step::get_next_from_link(kernel::link* pos) const
{
    
        assert(this);
        if ( pos == nullptr )
        {
            return _first_from_link;
        }
        assert(pos);
        assert(pos->_ref_to_step == this);
        return pos->_next_to_step;
}


kernel::link* kernel::step::get_prev_from_link(kernel::link* pos) const
{
    
        assert(this);
    
        if ( pos == nullptr )
        {
            return _last_from_link;
        }
    
        assert(pos);
        assert(pos->_ref_to_step == this);
        return pos->_prev_to_step;
}


size_t kernel::step::get_from_link_count() const
{
    
        assert(this);
        return _count_from_link;
}


void kernel::step::move_from_link_first(kernel::link* item)
{
    
        assert(item);
        assert(item->_ref_to_step);
        item->_ref_to_step->remove_from_link(item);
        add_from_link_first(item);
}


void kernel::step::move_from_link_last(kernel::link* item)
{
    
        assert(item);
        assert(item->_ref_to_step);
        item->_ref_to_step->remove_from_link(item);
        add_from_link_last(item);
}


void kernel::step::move_from_link_after(kernel::link* item, kernel::link* pos)
{
    
        assert(item);
        assert(item->_ref_to_step);
        item->_ref_to_step->remove_from_link(item);
        add_from_link_after(item, pos);
}


void kernel::step::move_from_link_before(kernel::link* item, kernel::link* pos)
{
    
        assert(item);
        assert(item->_ref_to_step);
        item->_ref_to_step->remove_from_link(item);
        add_from_link_before(item, pos);
        
}


void kernel::step::sort_from_link(int (*compare)(kernel::link*,kernel::link*))
{
    
    
        for (kernel::link* a = get_first_from_link(); a != nullptr ; a = get_next_from_link(a))
        {
            kernel::link* b = get_next_from_link(a);
    
            while ( b != nullptr && compare(a, b) > 0 )
            {
                kernel::link* c = get_prev_from_link(a);
                while ( c != nullptr  && compare(c, b) > 0 )
                    c = get_prev_from_link(c);
                if (c != nullptr )
                {
                    move_from_link_after(b, c);
                }
                else
                {
                    move_from_link_first(b);
                }
                b = get_next_from_link(a);
            }
        }
}


/**
* Return the first more used link
**/
kernel::link* kernel::step::more_used_to_link()
{
    size_t count=0;
    size_t candidate_count;
    kernel::link *candidate = nullptr;
    to_link_iterator iter_to_link = to_link_iterator(this);
    while(++iter_to_link)
    {
        candidate_count = iter_to_link->get_activation_count();
        if( candidate_count > count )
        {
            candidate = iter_to_link;
            count = candidate_count;
        }
    }
    return candidate;
}


/**
* Return the first more used link
**/
kernel::link* kernel::step::more_used_from_link()
{
    size_t count=0;
    size_t candidate_count;
    kernel::link *candidate = nullptr;
    from_link_iterator iter_from_link{this};
    while(++iter_from_link)
    {
        candidate_count = iter_from_link->get_activation_count();
        if( candidate_count > count )
        {
            candidate = iter_from_link;
            count = candidate_count;
        }
    }
    return candidate;
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::step::save(serial::oxstream& os) const
{
    os.write(_activation_count);
    os.write(_success_predictor);
    os.write(_max_activation_count);
    os.write(_backward_count);
    //serialize to_link childs 
    {
        size_t to_link_count = get_to_link_count();
        os.write(to_link_count);
        to_link_iterator iter{this};
        while(++iter)
        {
            os.save(iter.get());
        }
    }
    //serialize from_link childs 
    {
        size_t from_link_count = get_from_link_count();
        os.write(from_link_count);
        from_link_iterator iter{this};
        while(++iter)
        {
            os.save(iter.get());
        }
    }
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::step::save_contents(serial::oxstream& os) const
{
    os.write(_activation_count);
    os.write(_success_predictor);
    os.write(_max_activation_count);
    os.write(_backward_count);
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::step::save_relations(serial::oxstream& os) const
{
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::step::load(serial::ixstream& is)
{
    _activation_count = is.read<size_t>();
    _success_predictor = is.read<size_t>();
    _max_activation_count = is.read<size_t>();
    _backward_count = is.read<size_t>();
    //serialize to_link child 
    {
       size_t to_link_count = is.read<size_t>();
       while(to_link_count--)
       {
           add_to_link_last(dynamic_cast<link*>(is.load()));
       }
    }
    //serialize from_link child 
    {
       size_t from_link_count = is.read<size_t>();
       while(from_link_count--)
       {
           add_from_link_last(dynamic_cast<link*>(is.load()));
       }
    }
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::step::load_contents(serial::ixstream& is)
{
    _activation_count = is.read<size_t>();
    _success_predictor = is.read<size_t>();
    _max_activation_count = is.read<size_t>();
    _backward_count = is.read<size_t>();
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::step::load_relations(serial::ixstream& is)
{
    
    //serialize to_link child 
    {
       size_t to_link_count = is.read<size_t>();
       while(to_link_count--)
       {
           add_to_link_last(reinterpret_cast<link*>(is.read<size_t>()));
       }
    }
    //serialize from_link child 
    {
       size_t from_link_count = is.read<size_t>();
       while(from_link_count--)
       {
           add_from_link_last(reinterpret_cast<link*>(is.read<size_t>()));
       }
    }
}


void kernel::step::activate_backward()
{
    step *p_step=get_level()->get_active_step();
    activate();
    p_step->_backward_count++;
    p_step->_activation_count--;
}


void kernel::step::activate_forward()
{
    _activation_count++;
    if(_backward_count == 0)
    {
        step *p_step = get_level()->get_active_step();
        assert(p_step);
        kernel::link *p_link = p_step->find_link_to(this);
        assert(p_link);
        ++(*p_link);
    }
    else
    {
        _backward_count--;
    }
    activate();
}


void kernel::step::activate()
{
    auto *active_step = get_level()->get_active_step();
    if( active_step != nullptr )
    {
        get_level()->remove_active_step(active_step);
    }
    get_level()->add_active_step(this);
    
}


/**
* Search a link to Step
**/
kernel::link* kernel::step::find_link_to(kernel::step* p_step)
{
    to_link_iterator iter = to_link_iterator(this);
    while(++iter)
    {
        if( iter->get_to_step() == p_step )
        {
            return iter;
        }
    }
    return nullptr;
}


/**
* This method stores reached activation count into max activation count and reset activation count to zero.
* \param full If true, activation, success and backward counters are reset to zero.
**/
void kernel::step::reset(bool full)
{
    if ( full )
    {
        _max_activation_count = 0;
        _activation_count = 0;
        _backward_count = 0;
        
        to_link_iterator iter_to_link = to_link_iterator(this);
        while(++iter_to_link)
        {
            iter_to_link->set_activation_count(0);
        }
    
    }
    else
    {
        _max_activation_count = _activation_count+_backward_count;
        _activation_count = 0;
        _backward_count = _max_activation_count;
    }
}


/**
* Return the first node linked through to_link if any or nullptr if none. This is considered the natural step successor.
**/
kernel::step* kernel::step::get_natural_next()
{
    if( get_to_link_count() > 0 )
    {
        return get_first_to_link()->get_to_step();
    }
    return nullptr;
}


/**
* Return the first node linked through from_link if any or nullptr if none. This is considered the natural step precursor.
**/
kernel::step* kernel::step::get_natural_prev()
{
    if( get_from_link_count() > 0 )
    {
        return get_first_from_link()->get_from_step();
    }
    return nullptr;
}


/**
* Check if this node belongs to future
**/
bool kernel::step::future()
{
    if(  _activation_count > 0 )
        return false;
    return true;
}


/**
* Return a new link from the step to other step or a existent  link from this to the other.
**/
kernel::link* kernel::step::link_to_step(kernel::step* p_step)
{
    kernel::link *p_link = find_link_to(p_step);
    if( p_link != nullptr )
    {
        return p_link;
    }
    return new kernel::link{this, p_step};
}
/**
* Destructor.
**/
kernel::step::~step()
{
        __exit__();
}
// {user.before.class.to_link_iterator.begin}
// {user.before.class.to_link_iterator.end}



void kernel::step::to_link_iterator::__exit__()
{
    
            if (_next)
            {
                _next->_prev = _prev;
            }
            else
            {
                 _iter_from_step->_last_to_link_iterator = _prev;
            }
    
            if (_prev)
            {
                _prev->_next = _next;
            }
            else
            {
                _iter_from_step->_first_to_link_iterator = _next;
            }
            
}


void kernel::step::to_link_iterator::__init__(const kernel::step* iter_from_step, bool(kernel::link::*method)() const, kernel::link* ref_to_link)
{
    
        assert(iter_from_step);
     
        _iter_from_step = iter_from_step;
        _ref_to_link = _prev_to_link = _next_to_link = ref_to_link;
        _prev = nullptr;
        _next = nullptr;
        _method = method;
        if ( _iter_from_step->_last_to_link_iterator != nullptr )
        {
            _prev = _iter_from_step->_last_to_link_iterator;
            _prev->_next = this;
            _iter_from_step->_last_to_link_iterator = this;
        }
        else
        {
            _iter_from_step->_first_to_link_iterator  = _iter_from_step->_last_to_link_iterator  = this;
        }
        
}


void kernel::step::to_link_iterator::check(kernel::link* item_to_link)
{
    
        for (to_link_iterator* item =  _iter_from_step->_first_to_link_iterator; item; item = item->_next)
        {
            if (item->_prev_to_link == item_to_link)
            {
                item->_prev_to_link = item->_iter_from_step->get_next_to_link(item->_prev_to_link);
                item->_ref_to_link = nullptr;
            }
            if (item->_next_to_link == item_to_link)
            {
                item->_next_to_link = item->_iter_from_step->get_prev_to_link(item->_next_to_link);
                item->_ref_to_link = nullptr;
            }
        }
        
}


void kernel::step::to_link_iterator::check(kernel::link* item_to_link, kernel::link* new_item_to_link)
{
    
        for (to_link_iterator* item = _iter_from_step->_first_to_link_iterator; 
            item != nullptr ; item = item->_next)
        {
            if (item->_ref_to_link == item_to_link)
            {
                item->_ref_to_link =             item->_prev_to_link =             item->_next_to_link = new_item_to_link;
            }
            if (item->_prev_to_link == item_to_link)
            {
                item->_prev_to_link = new_item_to_link;
                item->_ref_to_link = nullptr;
            }
            if (item->_next_to_link == item_to_link)
            {
                item->_next_to_link = new_item_to_link;
                item->_ref_to_link = nullptr;
            }
        }
        
}
/**
* Destructor.
**/
kernel::step::to_link_iterator::~to_link_iterator()
{
    
        __exit__();
        
}

// {user.after.class.to_link_iterator.begin}
// {user.after.class.to_link_iterator.end}

// {user.before.class.from_link_iterator.begin}
// {user.before.class.from_link_iterator.end}



void kernel::step::from_link_iterator::__exit__()
{
    
            if (_next)
            {
                _next->_prev = _prev;
            }
            else
            {
                 _iter_to_step->_last_from_link_iterator = _prev;
            }
    
            if (_prev)
            {
                _prev->_next = _next;
            }
            else
            {
                _iter_to_step->_first_from_link_iterator = _next;
            }
            
}


void kernel::step::from_link_iterator::__init__(const kernel::step* iter_to_step, bool(kernel::link::*method)() const, kernel::link* ref_from_link)
{
    
        assert(iter_to_step);
     
        _iter_to_step = iter_to_step;
        _ref_from_link = _prev_from_link = _next_from_link = ref_from_link;
        _prev = nullptr;
        _next = nullptr;
        _method = method;
        if ( _iter_to_step->_last_from_link_iterator != nullptr )
        {
            _prev = _iter_to_step->_last_from_link_iterator;
            _prev->_next = this;
            _iter_to_step->_last_from_link_iterator = this;
        }
        else
        {
            _iter_to_step->_first_from_link_iterator  = _iter_to_step->_last_from_link_iterator  = this;
        }
        
}


void kernel::step::from_link_iterator::check(kernel::link* item_from_link)
{
    
        for (from_link_iterator* item =  _iter_to_step->_first_from_link_iterator; item; item = item->_next)
        {
            if (item->_prev_from_link == item_from_link)
            {
                item->_prev_from_link = item->_iter_to_step->get_next_from_link(item->_prev_from_link);
                item->_ref_from_link = nullptr;
            }
            if (item->_next_from_link == item_from_link)
            {
                item->_next_from_link = item->_iter_to_step->get_prev_from_link(item->_next_from_link);
                item->_ref_from_link = nullptr;
            }
        }
        
}


void kernel::step::from_link_iterator::check(kernel::link* item_from_link, kernel::link* new_item_from_link)
{
    
        for (from_link_iterator* item = _iter_to_step->_first_from_link_iterator; 
            item != nullptr ; item = item->_next)
        {
            if (item->_ref_from_link == item_from_link)
            {
                item->_ref_from_link =             item->_prev_from_link =             item->_next_from_link = new_item_from_link;
            }
            if (item->_prev_from_link == item_from_link)
            {
                item->_prev_from_link = new_item_from_link;
                item->_ref_from_link = nullptr;
            }
            if (item->_next_from_link == item_from_link)
            {
                item->_next_from_link = new_item_from_link;
                item->_ref_from_link = nullptr;
            }
        }
        
}
/**
* Destructor.
**/
kernel::step::from_link_iterator::~from_link_iterator()
{
    
        __exit__();
        
}

// {user.after.class.from_link_iterator.begin}
// {user.after.class.from_link_iterator.end}


// {user.after.class.step.begin}
// {user.after.class.step.end}


