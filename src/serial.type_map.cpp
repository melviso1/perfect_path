// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.type_map.begin}
// {user.before.class.type_map.end}



void serial::type_map::__exit__()
{
    //init from relations:
    
        while (_top_type)
        {
            delete _top_type;
        }
        
    //init to relations:
}
/**
* Constructor.
**/
serial::type_map::type_map()
:    _top_type{nullptr}
,    _count_type{0}
,    _first_type_iterator{nullptr}
,    _last_type_iterator{nullptr}
{
}



serial::type_record* serial::type_map::find_type(const std::string& value) const
{
    
            serial::type_record* result = 0;
            if (_top_type)
            {
                serial::type_record* item = _top_type;
                while (1)
                {
                    if (item->get_name() == value)
                    {
                        result = item;
                        break;
                    }
                    if (value <= item->get_name())
                    {
                        if (item->_left_map)
                        {
                            item = item->_left_map;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (item->_right_map)
                        {
                            item = item->_right_map;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            if (result)
            {
                serial::type_record* prev_type = get_prev_type(result);
                while (prev_type != nullptr && prev_type->get_name() == value)
                {
                    result = prev_type;
                    prev_type = get_prev_type(result);
                }
            }
            return result;
            
}


void serial::type_map::add_type(serial::type_record* item)
{
    
        assert(this);
        assert(item);
        assert(item->_ref_map == nullptr);
    
        _count_type++;
    
        item->_ref_map = this;
    
        if (_top_type)
        {
            serial::type_record* current = _top_type;
            while (1)
            {
                if (item->get_name() < current->get_name())
                {
                    if (current->_left_map)
                    {
                        current = current->_left_map;
                    }
                    else
                    {
                        current->_left_map = item;
                        item->_parent_map = current;
                        current->_bal_map--;
                        break;
                    }
                }
                else
                {
                    if (current->_right_map)
                    {
                        current = current->_right_map;
                    }
                    else
                    {
                        current->_right_map = item;
                        item->_parent_map = current;
                        current->_bal_map++;
                        break;
                    }
                }
            }
    
            serial::type_record* parent;
            while (current && current->_bal_map)
            {
                parent = current->_parent_map;
                if (parent)
                {
                    if (parent->_left_map == current)
                    {
                        parent->_bal_map--;
                    }
                    else
                    {
                        parent->_bal_map++;
                    }
    
                    if (parent->_bal_map == 2)
                    {
                        if (current->_bal_map == -1)
                        {
                            serial::type_record* sub = current->_left_map;
                            parent->_right_map = sub->_left_map;
                            if (sub->_left_map)
                            {
                                sub->_left_map->_parent_map = parent;
                            }
                            current->_left_map = sub->_right_map;
                            if (sub->_right_map)
                            {
                                sub->_right_map->_parent_map = current;
                            }
                            sub->_parent_map = parent->_parent_map;
                            sub->_left_map = parent;
                            parent->_parent_map = sub;
                            sub->_right_map = current;
                            current->_parent_map = sub;
                            if (sub->_parent_map)
                            {
                                if (sub->_parent_map->_left_map == parent)
                                {
                                    sub->_parent_map->_left_map = sub;
                                }
                                else
                                {
                                    sub->_parent_map->_right_map = sub;
                                }
                            }
                            else
                            {
                                _top_type = sub;
                            }
                            parent->_bal_map = (sub->_bal_map == 1? -1: 0);
                            current->_bal_map = (sub->_bal_map == -1? 1: 0);
                            sub->_bal_map = 0;
                            current = sub;
                        }
                        else
                        {
                            parent->_right_map = current->_left_map;
                            if (current->_left_map)
                            {
                                current->_left_map->_parent_map = parent;
                            }
                            current->_left_map = parent;
                            current->_parent_map = parent->_parent_map;
                            parent->_parent_map = current;
                            if (current->_parent_map)
                            {
                                if (current->_parent_map->_left_map == parent)
                                {
                                    current->_parent_map->_left_map = current;
                                }
                                else
                                {
                                    current->_parent_map->_right_map = current;
                                }
                            }
                            else
                            {
                                _top_type = current;
                            }
                            parent->_bal_map = 0;
                            current->_bal_map = 0;
                        }
                    }
                    else if (parent->_bal_map == -2)
                    {
                        if (current->_bal_map == 1)
                        {
                            serial::type_record* sub = current->_right_map;
                            parent->_left_map = sub->_right_map;
                            if (sub->_right_map)
                            {
                                sub->_right_map->_parent_map = parent;
                            }
                            current->_right_map = sub->_left_map;
                            if (sub->_left_map)
                            {
                                sub->_left_map->_parent_map = current;
                            }
                            sub->_parent_map = parent->_parent_map;
                            sub->_right_map = parent;
                            parent->_parent_map = sub;
                            sub->_left_map = current;
                            current->_parent_map = sub;
                            if (sub->_parent_map)
                            {
                                if (sub->_parent_map->_right_map == parent)
                                {
                                    sub->_parent_map->_right_map = sub;
                                }
                                else
                                {
                                    sub->_parent_map->_left_map = sub;
                                }
                            }
                            else
                            {
                                _top_type = sub;
                            }
                            parent->_bal_map = (sub->_bal_map == -1? 1: 0);
                            current->_bal_map = (sub->_bal_map == 1? -1: 0);
                            sub->_bal_map = 0;
                            current = sub;
                        }
                        else
                        {
                            parent->_left_map = current->_right_map;
                            if (current->_right_map)
                            {
                                current->_right_map->_parent_map = parent;
                            }
                            current->_right_map = parent;
                            current->_parent_map = parent->_parent_map;
                            parent->_parent_map = current;
                            if (current->_parent_map)
                            {
                                if (current->_parent_map->_right_map == parent)
                                {
                                    current->_parent_map->_right_map = current;
                                }
                                else
                                {
                                    current->_parent_map->_left_map = current;
                                }
                            }
                            else
                            {
                                _top_type = current;
                            }
                            parent->_bal_map = 0;
                            current->_bal_map = 0;
                        }
                    }
                    else
                    {
                        current = parent;
                    }
                }
                else
                {
                    current = parent;
                }
            }
        }
        else
        {
            _top_type = item;
        }
        
}


void serial::type_map::remove_type(serial::type_record* item)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_map == this);
    
        if(  _first_type_iterator != nullptr )
        {
            _first_type_iterator->check(item);
        }
    
        _count_type--;
    
        serial::type_record* subParent = item->_parent_map;
        serial::type_record* sub = item;
        /*     item               -                                   */
        /*      ^        ==>                                          */
        /*     - -                                                    */
        if (!item->_left_map && !item->_right_map)
        {
            if (subParent)
            {
                if (subParent->_left_map == item)
                {
                    subParent->_left_map = nullptr;
                    subParent->_bal_map++;
                }
                else
                {
                    subParent->_right_map = nullptr;
                    subParent->_bal_map--;
                }
            }
            else
            {
                _top_type = nullptr;
            }
        }
        else
        {
            if (item->_bal_map > 0)
            {
                sub = item->_right_map;
                while (sub->_left_map)
                {
                    sub = sub->_left_map;
                }
                subParent = sub->_parent_map;
                if (subParent != item)
                {
                    subParent->_left_map = sub->_right_map;
                    if (subParent->_left_map)
                    {
                        subParent->_left_map->_parent_map = subParent;
                    }
                    subParent->_bal_map++;
                }
                else
                {
                    item->_bal_map--;
                }
            }
            else
            {
                sub = item->_left_map;
                while (sub->_right_map)
                {
                    sub = sub->_right_map;
                }
                subParent = sub->_parent_map;
                if (subParent != item)
                {
                    subParent->_right_map = sub->_left_map;
                    if (subParent->_right_map)
                    {
                        subParent->_right_map->_parent_map = subParent;
                    }
                    subParent->_bal_map--;
                }
                else
                {
                    item->_bal_map++;
                }
            }
            sub->_parent_map = item->_parent_map;
            if (item->_parent_map)
            {
                if (item->_parent_map->_left_map == item)
                {
                    item->_parent_map->_left_map = sub;
                }
                else
                {
                    item->_parent_map->_right_map = sub;
                }
            }
            else
            {
                _top_type = sub;
            }
            if (item->_left_map != sub)
            {
                sub->_left_map = item->_left_map;
                if (item->_left_map)
                {
                    item->_left_map->_parent_map = sub;
                }
            }
            if (item->_right_map != sub)
            {
                sub->_right_map = item->_right_map;
                if (item->_right_map)
                {
                    item->_right_map->_parent_map = sub;
                }
            }
            sub->_bal_map = item->_bal_map;
    
            if (subParent == item)
            {
                subParent = sub;
            }
        }
    
        item->_ref_map = nullptr;
        item->_parent_map = nullptr;
        item->_left_map = nullptr;
        item->_right_map = nullptr;
        item->_bal_map = 0;
    
        serial::type_record* parent = subParent;
        while (parent && parent->_bal_map != -1 && parent->_bal_map != 1)
        {
            if (parent->_bal_map == 2)
            {
                serial::type_record* current = parent->_right_map;
                if (current->_bal_map == -1)
                {
                    serial::type_record* sub = current->_left_map;
                    parent->_right_map = sub->_left_map;
                    if (sub->_left_map)
                    {
                        sub->_left_map->_parent_map = parent;
                    }
                    current->_left_map = sub->_right_map;
                    if (sub->_right_map)
                    {
                        sub->_right_map->_parent_map = current;
                    }
                    sub->_parent_map = parent->_parent_map;
                    sub->_left_map = parent;
                    parent->_parent_map = sub;
                    sub->_right_map = current;
                    current->_parent_map = sub;
                    if (sub->_parent_map)
                    {
                        if (sub->_parent_map->_left_map == parent)
                        {
                            sub->_parent_map->_left_map = sub;
                        }
                        else
                        {
                            sub->_parent_map->_right_map = sub;
                        }
                    }
                    else
                    {
                        _top_type = sub;
                    }
                    parent->_bal_map = (sub->_bal_map == 1? -1: 0);
                    current->_bal_map = (sub->_bal_map == -1? 1: 0);
                    sub->_bal_map = 0;
                    parent = sub;
                }
                else if (current->_bal_map == 1)
                {
                    parent->_right_map = current->_left_map;
                    if (current->_left_map)
                    {
                        current->_left_map->_parent_map = parent;
                    }
                    current->_left_map = parent;
                    current->_parent_map = parent->_parent_map;
                    parent->_parent_map = current;
                    if (current->_parent_map)
                    {
                        if (current->_parent_map->_left_map == parent)
                        {
                            current->_parent_map->_left_map = current;
                        }
                        else
                        {
                            current->_parent_map->_right_map = current;
                        }
                    }
                    else
                    {
                        _top_type = current;
                    }
                    parent->_bal_map = 0;
                    current->_bal_map = 0;
                    parent = current;
                }
                else
                {
                    parent->_right_map = current->_left_map;
                    if (current->_left_map)
                    {
                        current->_left_map->_parent_map = parent;
                    }
                    current->_left_map = parent;
                    current->_parent_map = parent->_parent_map;
                    parent->_parent_map = current;
                    if (current->_parent_map)
                    {
                        if (current->_parent_map->_left_map == parent)
                        {
                            current->_parent_map->_left_map = current;
                        }
                        else
                        {
                            current->_parent_map->_right_map = current;
                        }
                    }
                    else
                    {
                        _top_type = current;
                    }
                    parent->_bal_map = 1;
                    current->_bal_map = -1;
                    break;
                }
            }
            else if (parent->_bal_map == -2)
            {
                serial::type_record* current = parent->_left_map;
                if (current->_bal_map == 1)
                {
                    serial::type_record* sub = current->_right_map;
                    parent->_left_map = sub->_right_map;
                    if (sub->_right_map)
                    {
                        sub->_right_map->_parent_map = parent;
                    }
                    current->_right_map = sub->_left_map;
                    if (sub->_left_map)
                    {
                        sub->_left_map->_parent_map = current;
                    }
                    sub->_parent_map = parent->_parent_map;
                    sub->_right_map = parent;
                    parent->_parent_map = sub;
                    sub->_left_map = current;
                    current->_parent_map = sub;
                    if (sub->_parent_map)
                    {
                        if (sub->_parent_map->_right_map == parent)
                        {
                            sub->_parent_map->_right_map = sub;
                        }
                        else
                        {
                            sub->_parent_map->_left_map = sub;
                        }
                    }
                    else
                    {
                        _top_type = sub;
                    }
                    parent->_bal_map = (sub->_bal_map == -1? 1: 0);
                    current->_bal_map = (sub->_bal_map == 1? -1: 0);
                    sub->_bal_map = 0;
                    parent = sub;
                }
                else if (current->_bal_map == -1)
                {
                    parent->_left_map = current->_right_map;
                    if (current->_right_map)
                    {
                        current->_right_map->_parent_map = parent;
                    }
                    current->_right_map = parent;
                    current->_parent_map = parent->_parent_map;
                    parent->_parent_map = current;
                    if (current->_parent_map)
                    {
                        if (current->_parent_map->_right_map == parent)
                        {
                            current->_parent_map->_right_map = current;
                        }
                        else
                        {
                            current->_parent_map->_left_map = current;
                        }
                    }
                    else
                    {
                        _top_type = current;
                    }
                    parent->_bal_map = 0;
                    current->_bal_map = 0;
                    parent = current;
                }
                else
                {
                    parent->_left_map = current->_right_map;
                    if (current->_right_map)
                    {
                        current->_right_map->_parent_map = parent;
                    }
                    current->_right_map = parent;
                    current->_parent_map = parent->_parent_map;
                    parent->_parent_map = current;
                    if (current->_parent_map)
                    {
                        if (current->_parent_map->_right_map == parent)
                        {
                            current->_parent_map->_right_map = current;
                        }
                        else
                        {
                            current->_parent_map->_left_map = current;
                        }
                    }
                    else
                    {
                        _top_type = current;
                    }
                    parent->_bal_map = -1;
                    current->_bal_map = 1;
                    break;
                }
            }
    
            if (parent->_parent_map)
            {
                if (parent->_parent_map->_left_map == parent)
                {
                    parent->_parent_map->_bal_map++;
                }
                else
                {
                    parent->_parent_map->_bal_map--;
                }
            }
    
            parent = parent->_parent_map;
        }
        
}


void serial::type_map::delete_all_type()
{
    
        assert(this);
    
        while (_top_type)
        {
            delete _top_type;
        }
        
}


void serial::type_map::replace_type(serial::type_record* item, serial::type_record* new_item)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_map == this);
    
        assert(new_item);
        assert(new_item->_ref_map == nullptr);
    
        if (item->get_name() == new_item->get_name())
        {
            if(  _first_type_iterator != nullptr )
            {
                _first_type_iterator->check(item, new_item);
            }
            if (_top_type == item)
            {
                _top_type = new_item;
            }
            if (item->_parent_map)
            {
                if (item->_parent_map->_left_map == item)
                {
                    item->_parent_map->_left_map = new_item;
                }
                else if (item->_parent_map->_right_map == item)
                {
                    item->_parent_map->_right_map = new_item;
                }
            }
            /* fix by mel : missing update side references */
            if (item->_left_map)
            {
                item->_left_map->_parent_map = new_item;
            }
            if (item->_right_map)
            {
                item->_right_map->_parent_map = new_item;
            }
            /* end fix */
            new_item->_ref_map = this;
            new_item->_parent_map = item->_parent_map;
            new_item->_left_map = item->_left_map;
            new_item->_right_map = item->_right_map;
            new_item->_bal_map = item->_bal_map;
            item->_ref_map = nullptr;
            item->_parent_map = nullptr;
            item->_left_map = nullptr;
            item->_right_map = nullptr;
            item->_bal_map = 0;
        }
        else
        {
            if(  _first_type_iterator != nullptr )
            {
                _first_type_iterator->check(item);
            }
            remove_type(item);
            add_type(new_item);
        }
        
}


serial::type_record* serial::type_map::get_first_type() const
{
    
        assert(this);
    
        serial::type_record* result = _top_type;
        if (result)
        {
            while (result->_left_map)
            {
                result = result->_left_map;
            }
        }
    
        return result;
        
}


serial::type_record* serial::type_map::get_last_type() const
{
    
        assert(this);
    
        serial::type_record* result = _top_type;
        if (result)
        {
            while (result->_right_map)
            {
                result = result->_right_map;
            }
        }
    
        return result;
        
}


serial::type_record* serial::type_map::get_next_type(serial::type_record* pos) const
{
    
        assert(this);
    
        serial::type_record* result = 0;
        if (pos == nullptr)
        {
            result = _top_type;
            if (result)
            {
                while (result->_left_map)
                {
                    result = result->_left_map;
                }
            }
        }
        else
        {
            assert(pos->_ref_map == this);
    
            if (pos->_right_map)
            {
                result = pos->_right_map;
                while (result->_left_map)
                {
                    result = result->_left_map;
                }
            }
            else
            {
                result = pos->_parent_map;
                while (result && result->_right_map == pos)
                {
                    pos = result;
                    result = pos->_parent_map;
                }
            }
        }
    
        return result;
        
}


serial::type_record* serial::type_map::get_prev_type(serial::type_record* pos) const
{
    
        assert(this);
    
        serial::type_record* result = nullptr;
        if (pos == nullptr)
        {
            result = _top_type;
            if (result)
            {
                while (result->_right_map)
                {
                    result = result->_right_map;
                }
            }
        }
        else
        {
            assert(pos->_ref_map == this);
    
            if (pos->_left_map)
            {
                result = pos->_left_map;
                while (result->_right_map)
                {
                    result = result->_right_map;
                }
            }
            else
            {
                result = pos->_parent_map;
                while (result && result->_left_map == pos)
                {
                    pos = result;
                    result = pos->_parent_map;
                }
            }
        }
    
        return result;
        
}


size_t serial::type_map::get_type_count() const
{
    
        assert(this);
        return _count_type;
        
}


serial::type_map* serial::type_map::get()
{
    static serial::type_map instance{};
    return &instance;
}
/**
* Destructor.
**/
serial::type_map::~type_map()
{
        __exit__();
}
// {user.before.class.type_iterator.begin}
// {user.before.class.type_iterator.end}



void serial::type_map::type_iterator::__exit__()
{
    
            if (_next)
            {
                _next->_prev = _prev;
            }
            else
            {
                 _iter_map->_last_type_iterator = _prev;
            }
    
            if (_prev)
            {
                _prev->_next = _next;
            }
            else
            {
                _iter_map->_first_type_iterator = _next;
            }
            
}


void serial::type_map::type_iterator::__init__(const serial::type_map* iter_map, bool(serial::type_record::*method)() const, serial::type_record* ref_type)
{
    
        assert(iter_map);
     
        _iter_map = iter_map;
        _ref_type = _prev_type = _next_type = ref_type;
        _prev = nullptr;
        _next = nullptr;
        _method = method;
        if ( _iter_map->_last_type_iterator != nullptr )
        {
            _prev = _iter_map->_last_type_iterator;
            _prev->_next = this;
            _iter_map->_last_type_iterator = this;
        }
        else
        {
            _iter_map->_first_type_iterator  = _iter_map->_last_type_iterator  = this;
        }
        
}


void serial::type_map::type_iterator::check(serial::type_record* item_type)
{
    
        for (type_iterator* item =  _iter_map->_first_type_iterator; item; item = item->_next)
        {
            if (item->_prev_type == item_type)
            {
                item->_prev_type = item->_iter_map->get_next_type(item->_prev_type);
                item->_ref_type = nullptr;
            }
            if (item->_next_type == item_type)
            {
                item->_next_type = item->_iter_map->get_prev_type(item->_next_type);
                item->_ref_type = nullptr;
            }
        }
        
}


void serial::type_map::type_iterator::check(serial::type_record* item_type, serial::type_record* new_item_type)
{
    
        for (type_iterator* item = _iter_map->_first_type_iterator; 
            item != nullptr ; item = item->_next)
        {
            if (item->_ref_type == item_type)
            {
                item->_ref_type =             item->_prev_type =             item->_next_type = new_item_type;
            }
            if (item->_prev_type == item_type)
            {
                item->_prev_type = new_item_type;
                item->_ref_type = nullptr;
            }
            if (item->_next_type == item_type)
            {
                item->_next_type = new_item_type;
                item->_ref_type = nullptr;
            }
        }
        
}
/**
* Destructor.
**/
serial::type_map::type_iterator::~type_iterator()
{
    
        __exit__();
        
}

// {user.after.class.type_iterator.begin}
// {user.after.class.type_iterator.end}


// {user.after.class.type_map.begin}
// {user.after.class.type_map.end}


