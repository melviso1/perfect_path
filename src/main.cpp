// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

// {user.before.code.begin}

#include <cstdio>
#include <functional>
// {user.before.code.end}

text_path* text_data = nullptr;
volatile bool interrupt = false;


int main(int argc, char* argv[])
{
        install_signal_handler();
        start_text();    
        text_data->reset(true);
        #if INTERACTIVE
        menu();
        #endif
        end_text();
        return 0;
}


/*
Create a new text path
*/
void start_text()
{
    delete text_data;
    text_data = new text_path{};
}


/*
Create a new text path
*/
void end_text()
{
    delete text_data;
    text_data = nullptr;
}


/*
Used for capture the Ctrl+C
*/
void sig_handler(int s)
{
    cout << "\b\bEnding season...\n";
    interrupt = true;
}


void install_signal_handler()
{
    struct sigaction sigIntHandler;
    
    sigIntHandler.sa_handler = sig_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    
    sigaction(SIGINT, &sigIntHandler, NULL);
}


void menu()
{
    bool quit = false;
    text_path& system = *text_data;
    char c;
    while(!quit)
    {
        clear_screen();
        cout << "\n\n";
        cout << "    1 ... load binary data.\n";
        cout << "    2 ... save binary data.\n";
        cout << "    3 ... digest optimization.\n";
        cout << "    4 ... go dialog.\n";
        cout << "    t ... do internal tests.\n";
        cout << "    q ... quit.\n\n";
        cout << "    select action : " << flush;
        c = std::cin.peek();
        switch(c)
        {
            case '1':
                system.restore("./learning_data");
                break;
            case '2':
                system.backup("./learning_data");
                break;
            case '3':
                system.digest();
                break;
            case '4':
                dialog();
                break;
            case 't':
            case 'T':
                do_all_tests();
                break;
            case 'q':
            case 'Q':
                quit = true;
                cout << "\n\n    Goodbye!\n\n" << flush;
                break;
            default:
                cout << "\n\n    Not a valid option " << flush;
                sleep(2);
                break;
        }
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cin.clear();
    }
}


/*
clear screen
*/
void clear_screen()
{
    #if WINDOWS
    std::system("cls")
    #else
    std::cout << "\033[2J" << std::flush;
    #endif
}


void dialog()
{
    clear_screen();
    string enter;
    interrupt = false;
    text_path& system = *text_data;
    system.reset(true);
    cout << "    Press Ctrl+C any time to return to main menu\n\n";
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    while( ! interrupt )
    {
        cout << "\n    Enter text :";
        getline(cin, enter);
        if( interrupt )
            break;
        text_data->in(enter);
        cout << "\n    Output text :";
        cout << text_data->out(TEXT_SZ);
    }
}


/*
This method checks that the system will preserve the information. For this test, a long text is introduced in the system, then the system is returned to default state and a replay is done, for comparing the output with the input.
*/
bool test_direct_unicity()
{
    text_path& system = *text_data;
    system.init();
    char *buffer = new char[TEXT_SZ];
    for(size_t i=0;i<TEXT_SZ;i++)
        buffer[i] = 32+static_cast<char>(rand() % 224);
    string text{buffer};
    delete [] buffer;
    string produced;
    system.in(text);
    system.reset();
    produced = system.out(text.length());   
    system.info();
    return ( produced == text );
}


bool do_all_tests()
{
    bool result = true;
    auto test = [&result](const std::string& name, std::function<bool()> test_function)
    {
        cout << "------------------------------\n";
        cout << "TEST : " << name <<"\n";
        cout << "------------------------------\n";
        bool result =test_function();
        if ( result )
        {
            cout << "------------------------------\n";
            cout << "TEST " << name << " : PASSED\n";
        }
        else
        {
            cout << "------------------------------\n";
            cout << "TEST " << name << " : FAILED\n";
            result = false;
        }
        cout << "------------------------------\n";
    };
    
    #define TEST(x) test(#x, x)
    TEST(test_history);
    TEST(test_direct_unicity);
    TEST(test_reverse);
    TEST(test_save_and_restore);
    TEST(test_digest);
    TEST(test_no_duplicates);
    TEST(test_digest_reverse);
    TEST(test_load_talk);
    TEST(test_direct_unicity_dialog);
    
    #undef TEST
    
    return result;
}


/*
This method checks that the system will preserve the information. For this test, a sequence of introduced and extracted text is compared with replay.
*/
bool test_direct_unicity_dialog()
{
    const char *file_name="./example.txt";
    text_path& system = *text_data;
    system.init();
    size_t lines = system.load_talk(file_name);
    if ( lines == size_t(-1) )
    {
        //error reading file
        return false;
    }
    string output;
    system.reset();
    ifstream f_in( file_name );
    if( ! f_in.good() )
        return size_t(-1);
    std::string text;
    lines=0;
    while( getline(f_in, text) )
    {
        if( lines % 1 )
        {
            output = system.out(TEXT_SZ); 
            if( output != text )
            {
                cout << "failed line "<<lines<<":\n";
                cout << "produced:"<<output<<"\n";
                cout << "expected:"<<text<<"\n";
                f_in.close();
                return false;
            }
        }
        else
        {
            system.in(text);
        }
        lines++;
    }
    f_in.close();
    return true;
}


bool test_reverse()
{
    text_path& system = *text_data;
    system.init();
    auto revert =[](string s){reverse(s.begin(), s.end()); return s;};
    
    string text1{"Este es un texto a la derecha 1"};
    string text2{"Este es un texto a la derecha 2"};
    string produced1;
    string produced2;
    system.in(text1);
    system.out_reverse(1); //discard terminator
    produced1 = system.out_reverse(TEXT_SZ);
    cout << "introduced text 1:" << text1 << "\n";
    cout << "reversed path 1:" << produced1 << "\n";
    if ( produced1 != revert(text1) )
        return false;
    produced1 = system.out(TEXT_SZ);
    cout << "forward text 1 (again):" << produced1 << "\n";
    if ( produced1 != text1 )
        return false;
    system.in(text2);
    system.out_reverse(1); //discard terminator
    produced2 = system.out_reverse(TEXT_SZ);
    produced1 = system.out_reverse(TEXT_SZ);
    cout << "introduced text 2:" << text2 << "\n";
    cout << "reversed path 2:" << produced2 << "\n";
    cout << "reversed path 1:" << produced1 << "\n";
    if ( produced2 != revert(text2) )
        return false;
    if ( produced1 != revert(text1) )
        return false;
    produced1 = system.out(TEXT_SZ);
    produced2 = system.out(TEXT_SZ);
    cout << "forward text 1 (again):" << produced1 << "\n";
    cout << "forward text 2 (again):" << produced2 << "\n";
    if ( produced1 != text1 )
        return false;
    if ( produced2 != text2 )
        return false;
    
    return true;
}


bool test_save_and_restore()
{
    text_path& system = *text_data;
    system.init();
    #define TEST_SZ 16384
    char *buffer = new char[TEST_SZ];
    for(size_t i=0;i<TEST_SZ;i++)
        buffer[i] = 32+static_cast<char>(rand() % 224);
    string text{buffer};
    string produced;
    system.in(text);
    system.info();
    if(!system.backup("/tmp/test_file"))
    {
        return false;
    }
    if(!system.restore("/tmp/test_file"))
    {
        return false;
    }
    system.reset();
    produced = system.out(text.length());
    system.info();
    return ( produced == text );
}


/*
Check if the length of the level match history.
*/
bool test_history()
{
    text_path& system = *text_data;
    system.init();
    string text1{"Este es un texto a la derecha 1"};
    string text2{"Este es un texto a la derecha 2"};
    string text3{"Este es un texto a la derecha 3"};
    string text4{"Este es un texto a la derecha 4"};
    system.in(text1);
    system.in(text2);
    system.in(text3);
    system.in(text4);
    size_t length = system.length();
    if( length - 4 != text1.size() + text2.size() + text3.size() +  text4.size() )
    {
        cout << "Failed history length\n";
        return false;
    }
    //now do it while replay
    string output;
    system.reset();
    output = system.out(TEXT_SZ);
    length -= output.size();
    cout << output << "\n";
    
    output = system.out(TEXT_SZ); 
    length -= output.size();
    cout << output << "\n";
    
    output = system.out(TEXT_SZ); 
    length -= output.size();
    cout << output << "\n";
    
    output = system.out(TEXT_SZ); 
    length -= output.size();
    cout << output << "\n";
    if( length != 4)
    {
        return false;
    }
    return true;
}


/*
Load a file talk and check the contents.
*/
bool test_load_talk()
{
    text_path& system = *text_data;
    system.init();
    size_t lines = system.load_talk("./example.txt");
    if ( lines == size_t(-1) )
    {
        //error reading file
        return false;
    }
    string output;
    system.reset();
    while(lines--)
    {
        output = system.out(TEXT_SZ); 
        cout << output << "\n";
    }
    return true;
    
}


bool test_digest()
{
    const char *file_name="./example.txt";
    text_path& system = *text_data;
    system.init();
    size_t lines = system.load_talk(file_name);
    if ( lines == size_t(-1) )
    {
        //error reading file
        return false;
    }
    bool result = true;
    size_t length = system.length();
    system.replace(kernel::meta::level_digest(system.get_base_level()).release());
    if ( length != system.length() )
    {
        cout << "failed lenght after digest ("<<length<<"!="<<system.length()<<")!\n";
        result = false;
    }
    system.reset();
    ifstream f_in( file_name );
    if( ! f_in.good() )
    {
        cout << "failed to open dialog!\n";
        f_in.close();
        return false;
    }
    std::string text;
    std::string output;
    lines=0;
    while( getline(f_in, text) )
    {
        if( lines % 1 )
        {
            output = system.out(TEXT_SZ); 
            if( output != text )
            {
                cout << "failed line "<<lines<<":\n";
                cout << "produced:"<<output<<"\n";
                cout << "expected:"<<text<<"\n";
                f_in.close();
                return false;
            }
        }
        else
        {
            system.in(text);
        }
        lines++;
    }
    f_in.close();
    if( lines == 0 )
    {
        cout << "cant read dialog!\n";
        return false;
    }
    return result;
}


/*
This check verify the integrity of levels based on that there is not duplicated links between steps
*/
bool test_no_duplicates()
{
    text_path& system = *text_data;
    /**
    This is a expensive test!!
    **/
    bool result = true;
    size_t count=1;
    size_t duplicated;
    kernel::level *current = system.get_base_level();
    kernel::step *p_step;
    while( current != nullptr )
    {
        cout << "Analizing level "<< count <<":";
        duplicated = 0;
        kernel::level::step_iterator it{current};
        while(++it)
        {
            kernel::step::to_link_iterator tit{it};
            while(++tit)
            {
                p_step = tit->get_to_step();
                kernel::step::to_link_iterator tit2{it,nullptr,tit};
                while(++tit2)
                {
                    if( tit2->get_to_step() == p_step )
                    {
                        duplicated++;
                    }
                }
            }
        }
        if( duplicated != 0 )
        {
            result = false;
            cout << "Found "<<duplicated<<" duplicated links on level "<< count <<"\n";
        }
        else
        {
            cout << "No duplicated links found between steps.\n";
        }
        count++;
        current = current->get_super_level();
    }
    return result;
                
                
}


bool test_digest_reverse()
{
    auto revert =[](string s){reverse(s.begin(), s.end()); return s;};
    text_path& system = *text_data;
    system.init();
    string text{"Habia una vez un caballero que desafio a una dama a decir lo que pensaba de el. La dama se callo y el caballero exasperado tras esperar un rato, le pregunto cuando pensaba decirlo -Ya he dicho lo que pensaba, contesto la dama -Yo no he oido que dijese nada -Exactamente- contesto la dama - eso es lo que pienso de usted: Nada."};
    string produced;
    system.in(text);
    system.replace(kernel::meta::level_digest(system.get_base_level()).release());
    system.reset();
    produced = system.out(text.length());   
    if( produced != text )
    {
        cout << "direct replay failed\n";
        cout << "produced : " << produced << "\n";
        return false;
    }
    string reverted = revert(text);
    produced = system.out_reverse(text.length());
    if ( produced != reverted )
    {
        cout << "inverse replay failed\n\n";
        cout << "expected (" << reverted.length() << " characters) :" << reverted << "\n\n";
        cout << "produced (" << produced.length() << " characters) :" << produced << "\n\n";
        return false;
    }
    return true;
    
}

// {user.after.code.begin}
// {user.after.code.end}

