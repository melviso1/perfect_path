// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.runtime.begin}
// {user.before.class.runtime.end}

/**
* Constructor.
**/
serial::runtime::runtime()
{
}



/**
* This method stores a runtime type in a recoverable way.
**/
void serial::runtime::store_type(std::ostream& os) const
{
    std::string name = get_type().get_name();
    size_t sz = name.size();
    os.write(reinterpret_cast < const char * > (& sz), sizeof(size_t));
    os.write(name.c_str(), sz);
}


/**
* This method restores a runtime instance from stream. It returns the pointer
* to the new instance or nullptr in failed case.
**/
serial::runtime* serial::runtime::restore_type(std::istream& is)
{
    size_t sz;
    is.read(reinterpret_cast<char*>(&sz), sizeof(size_t));
    char *buffer = new char[sz+1];
    is.read(buffer, sz);
    buffer[sz]=0;
    std::string name(buffer);
    delete [] buffer;
    type_record *p_type_record = type_map::get()->find_type(name);
    if ( p_type_record == nullptr )
    {
        return nullptr;
    }
    return p_type_record->create();
}
/**
* Destructor.
**/
serial::runtime::~runtime()
{
}

// {user.after.class.runtime.begin}
// {user.after.class.runtime.end}


