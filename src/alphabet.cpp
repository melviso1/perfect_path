// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.alphabet.begin}
// {user.before.class.alphabet.end}



void alphabet::__exit__()
{
    //init from relations:
    
        while (_top_char_symbol)
        {
            delete _top_char_symbol;
        }
        
    //init to relations:
}
/**
* Constructor.
**/
alphabet::alphabet(const serial::type_record* _)
:    serial::runtime{}
,    serial::runtime_instance<alphabet>{}
,    _top_char_symbol{nullptr}
,    _count_char_symbol{0}
,    _first_char_symbol_iterator{nullptr}
,    _last_char_symbol_iterator{nullptr}
{
}

/**
* Constructor.
**/
alphabet::alphabet()
:    serial::runtime{}
,    serial::runtime_instance<alphabet>{}
,    _top_char_symbol{nullptr}
,    _count_char_symbol{0}
,    _first_char_symbol_iterator{nullptr}
,    _last_char_symbol_iterator{nullptr}
{
    initialize();
}



/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void alphabet::save(serial::oxstream& os) const
{
    
    //serialize char_symbol childs 
    {
        size_t char_symbol_count = get_char_symbol_count();
        os.write(char_symbol_count);
        char_symbol_iterator iter{this};
        while(++iter)
        {
            os.save(iter.get());
        }
    }
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void alphabet::save_contents(serial::oxstream& os) const
{
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void alphabet::save_relations(serial::oxstream& os) const
{
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void alphabet::load(serial::ixstream& is)
{
    
    //serialize char_symbol child 
    {
       size_t char_symbol_count = is.read<size_t>();
       while(char_symbol_count--)
       {
           add_char_symbol(dynamic_cast<char_symbol*>(is.load()));
       }
    }
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void alphabet::load_contents(serial::ixstream& is)
{
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void alphabet::load_relations(serial::ixstream& is)
{
    
    //serialize char_symbol child 
    {
       size_t char_symbol_count = is.read<size_t>();
       while(char_symbol_count--)
       {
           add_char_symbol(reinterpret_cast<char_symbol*>(is.read<size_t>()));
       }
    }
}


char_symbol* alphabet::find_char_symbol(const char value) const
{
    
            char_symbol* result = 0;
            if (_top_char_symbol)
            {
                char_symbol* item = _top_char_symbol;
                while (1)
                {
                    if (item->get_cchar() == value)
                    {
                        result = item;
                        break;
                    }
                    if (value <= item->get_cchar())
                    {
                        if (item->_left_alphabet)
                        {
                            item = item->_left_alphabet;
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (item->_right_alphabet)
                        {
                            item = item->_right_alphabet;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            if (result)
            {
                char_symbol* prev_char_symbol = get_prev_char_symbol(result);
                while (prev_char_symbol != nullptr && prev_char_symbol->get_cchar() == value)
                {
                    result = prev_char_symbol;
                    prev_char_symbol = get_prev_char_symbol(result);
                }
            }
            return result;
            
}


void alphabet::add_char_symbol(char_symbol* item)
{
    
        assert(this);
        assert(item);
        assert(item->_ref_alphabet == nullptr);
    
        _count_char_symbol++;
    
        item->_ref_alphabet = this;
    
        if (_top_char_symbol)
        {
            char_symbol* current = _top_char_symbol;
            while (1)
            {
                if (item->get_cchar() < current->get_cchar())
                {
                    if (current->_left_alphabet)
                    {
                        current = current->_left_alphabet;
                    }
                    else
                    {
                        current->_left_alphabet = item;
                        item->_parent_alphabet = current;
                        current->_bal_alphabet--;
                        break;
                    }
                }
                else
                {
                    if (current->_right_alphabet)
                    {
                        current = current->_right_alphabet;
                    }
                    else
                    {
                        current->_right_alphabet = item;
                        item->_parent_alphabet = current;
                        current->_bal_alphabet++;
                        break;
                    }
                }
            }
    
            char_symbol* parent;
            while (current && current->_bal_alphabet)
            {
                parent = current->_parent_alphabet;
                if (parent)
                {
                    if (parent->_left_alphabet == current)
                    {
                        parent->_bal_alphabet--;
                    }
                    else
                    {
                        parent->_bal_alphabet++;
                    }
    
                    if (parent->_bal_alphabet == 2)
                    {
                        if (current->_bal_alphabet == -1)
                        {
                            char_symbol* sub = current->_left_alphabet;
                            parent->_right_alphabet = sub->_left_alphabet;
                            if (sub->_left_alphabet)
                            {
                                sub->_left_alphabet->_parent_alphabet = parent;
                            }
                            current->_left_alphabet = sub->_right_alphabet;
                            if (sub->_right_alphabet)
                            {
                                sub->_right_alphabet->_parent_alphabet = current;
                            }
                            sub->_parent_alphabet = parent->_parent_alphabet;
                            sub->_left_alphabet = parent;
                            parent->_parent_alphabet = sub;
                            sub->_right_alphabet = current;
                            current->_parent_alphabet = sub;
                            if (sub->_parent_alphabet)
                            {
                                if (sub->_parent_alphabet->_left_alphabet == parent)
                                {
                                    sub->_parent_alphabet->_left_alphabet = sub;
                                }
                                else
                                {
                                    sub->_parent_alphabet->_right_alphabet = sub;
                                }
                            }
                            else
                            {
                                _top_char_symbol = sub;
                            }
                            parent->_bal_alphabet = (sub->_bal_alphabet == 1? -1: 0);
                            current->_bal_alphabet = (sub->_bal_alphabet == -1? 1: 0);
                            sub->_bal_alphabet = 0;
                            current = sub;
                        }
                        else
                        {
                            parent->_right_alphabet = current->_left_alphabet;
                            if (current->_left_alphabet)
                            {
                                current->_left_alphabet->_parent_alphabet = parent;
                            }
                            current->_left_alphabet = parent;
                            current->_parent_alphabet = parent->_parent_alphabet;
                            parent->_parent_alphabet = current;
                            if (current->_parent_alphabet)
                            {
                                if (current->_parent_alphabet->_left_alphabet == parent)
                                {
                                    current->_parent_alphabet->_left_alphabet = current;
                                }
                                else
                                {
                                    current->_parent_alphabet->_right_alphabet = current;
                                }
                            }
                            else
                            {
                                _top_char_symbol = current;
                            }
                            parent->_bal_alphabet = 0;
                            current->_bal_alphabet = 0;
                        }
                    }
                    else if (parent->_bal_alphabet == -2)
                    {
                        if (current->_bal_alphabet == 1)
                        {
                            char_symbol* sub = current->_right_alphabet;
                            parent->_left_alphabet = sub->_right_alphabet;
                            if (sub->_right_alphabet)
                            {
                                sub->_right_alphabet->_parent_alphabet = parent;
                            }
                            current->_right_alphabet = sub->_left_alphabet;
                            if (sub->_left_alphabet)
                            {
                                sub->_left_alphabet->_parent_alphabet = current;
                            }
                            sub->_parent_alphabet = parent->_parent_alphabet;
                            sub->_right_alphabet = parent;
                            parent->_parent_alphabet = sub;
                            sub->_left_alphabet = current;
                            current->_parent_alphabet = sub;
                            if (sub->_parent_alphabet)
                            {
                                if (sub->_parent_alphabet->_right_alphabet == parent)
                                {
                                    sub->_parent_alphabet->_right_alphabet = sub;
                                }
                                else
                                {
                                    sub->_parent_alphabet->_left_alphabet = sub;
                                }
                            }
                            else
                            {
                                _top_char_symbol = sub;
                            }
                            parent->_bal_alphabet = (sub->_bal_alphabet == -1? 1: 0);
                            current->_bal_alphabet = (sub->_bal_alphabet == 1? -1: 0);
                            sub->_bal_alphabet = 0;
                            current = sub;
                        }
                        else
                        {
                            parent->_left_alphabet = current->_right_alphabet;
                            if (current->_right_alphabet)
                            {
                                current->_right_alphabet->_parent_alphabet = parent;
                            }
                            current->_right_alphabet = parent;
                            current->_parent_alphabet = parent->_parent_alphabet;
                            parent->_parent_alphabet = current;
                            if (current->_parent_alphabet)
                            {
                                if (current->_parent_alphabet->_right_alphabet == parent)
                                {
                                    current->_parent_alphabet->_right_alphabet = current;
                                }
                                else
                                {
                                    current->_parent_alphabet->_left_alphabet = current;
                                }
                            }
                            else
                            {
                                _top_char_symbol = current;
                            }
                            parent->_bal_alphabet = 0;
                            current->_bal_alphabet = 0;
                        }
                    }
                    else
                    {
                        current = parent;
                    }
                }
                else
                {
                    current = parent;
                }
            }
        }
        else
        {
            _top_char_symbol = item;
        }
        
}


void alphabet::remove_char_symbol(char_symbol* item)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_alphabet == this);
    
        if(  _first_char_symbol_iterator != nullptr )
        {
            _first_char_symbol_iterator->check(item);
        }
    
        _count_char_symbol--;
    
        char_symbol* subParent = item->_parent_alphabet;
        char_symbol* sub = item;
        /*     item               -                                   */
        /*      ^        ==>                                          */
        /*     - -                                                    */
        if (!item->_left_alphabet && !item->_right_alphabet)
        {
            if (subParent)
            {
                if (subParent->_left_alphabet == item)
                {
                    subParent->_left_alphabet = nullptr;
                    subParent->_bal_alphabet++;
                }
                else
                {
                    subParent->_right_alphabet = nullptr;
                    subParent->_bal_alphabet--;
                }
            }
            else
            {
                _top_char_symbol = nullptr;
            }
        }
        else
        {
            if (item->_bal_alphabet > 0)
            {
                sub = item->_right_alphabet;
                while (sub->_left_alphabet)
                {
                    sub = sub->_left_alphabet;
                }
                subParent = sub->_parent_alphabet;
                if (subParent != item)
                {
                    subParent->_left_alphabet = sub->_right_alphabet;
                    if (subParent->_left_alphabet)
                    {
                        subParent->_left_alphabet->_parent_alphabet = subParent;
                    }
                    subParent->_bal_alphabet++;
                }
                else
                {
                    item->_bal_alphabet--;
                }
            }
            else
            {
                sub = item->_left_alphabet;
                while (sub->_right_alphabet)
                {
                    sub = sub->_right_alphabet;
                }
                subParent = sub->_parent_alphabet;
                if (subParent != item)
                {
                    subParent->_right_alphabet = sub->_left_alphabet;
                    if (subParent->_right_alphabet)
                    {
                        subParent->_right_alphabet->_parent_alphabet = subParent;
                    }
                    subParent->_bal_alphabet--;
                }
                else
                {
                    item->_bal_alphabet++;
                }
            }
            sub->_parent_alphabet = item->_parent_alphabet;
            if (item->_parent_alphabet)
            {
                if (item->_parent_alphabet->_left_alphabet == item)
                {
                    item->_parent_alphabet->_left_alphabet = sub;
                }
                else
                {
                    item->_parent_alphabet->_right_alphabet = sub;
                }
            }
            else
            {
                _top_char_symbol = sub;
            }
            if (item->_left_alphabet != sub)
            {
                sub->_left_alphabet = item->_left_alphabet;
                if (item->_left_alphabet)
                {
                    item->_left_alphabet->_parent_alphabet = sub;
                }
            }
            if (item->_right_alphabet != sub)
            {
                sub->_right_alphabet = item->_right_alphabet;
                if (item->_right_alphabet)
                {
                    item->_right_alphabet->_parent_alphabet = sub;
                }
            }
            sub->_bal_alphabet = item->_bal_alphabet;
    
            if (subParent == item)
            {
                subParent = sub;
            }
        }
    
        item->_ref_alphabet = nullptr;
        item->_parent_alphabet = nullptr;
        item->_left_alphabet = nullptr;
        item->_right_alphabet = nullptr;
        item->_bal_alphabet = 0;
    
        char_symbol* parent = subParent;
        while (parent && parent->_bal_alphabet != -1 && parent->_bal_alphabet != 1)
        {
            if (parent->_bal_alphabet == 2)
            {
                char_symbol* current = parent->_right_alphabet;
                if (current->_bal_alphabet == -1)
                {
                    char_symbol* sub = current->_left_alphabet;
                    parent->_right_alphabet = sub->_left_alphabet;
                    if (sub->_left_alphabet)
                    {
                        sub->_left_alphabet->_parent_alphabet = parent;
                    }
                    current->_left_alphabet = sub->_right_alphabet;
                    if (sub->_right_alphabet)
                    {
                        sub->_right_alphabet->_parent_alphabet = current;
                    }
                    sub->_parent_alphabet = parent->_parent_alphabet;
                    sub->_left_alphabet = parent;
                    parent->_parent_alphabet = sub;
                    sub->_right_alphabet = current;
                    current->_parent_alphabet = sub;
                    if (sub->_parent_alphabet)
                    {
                        if (sub->_parent_alphabet->_left_alphabet == parent)
                        {
                            sub->_parent_alphabet->_left_alphabet = sub;
                        }
                        else
                        {
                            sub->_parent_alphabet->_right_alphabet = sub;
                        }
                    }
                    else
                    {
                        _top_char_symbol = sub;
                    }
                    parent->_bal_alphabet = (sub->_bal_alphabet == 1? -1: 0);
                    current->_bal_alphabet = (sub->_bal_alphabet == -1? 1: 0);
                    sub->_bal_alphabet = 0;
                    parent = sub;
                }
                else if (current->_bal_alphabet == 1)
                {
                    parent->_right_alphabet = current->_left_alphabet;
                    if (current->_left_alphabet)
                    {
                        current->_left_alphabet->_parent_alphabet = parent;
                    }
                    current->_left_alphabet = parent;
                    current->_parent_alphabet = parent->_parent_alphabet;
                    parent->_parent_alphabet = current;
                    if (current->_parent_alphabet)
                    {
                        if (current->_parent_alphabet->_left_alphabet == parent)
                        {
                            current->_parent_alphabet->_left_alphabet = current;
                        }
                        else
                        {
                            current->_parent_alphabet->_right_alphabet = current;
                        }
                    }
                    else
                    {
                        _top_char_symbol = current;
                    }
                    parent->_bal_alphabet = 0;
                    current->_bal_alphabet = 0;
                    parent = current;
                }
                else
                {
                    parent->_right_alphabet = current->_left_alphabet;
                    if (current->_left_alphabet)
                    {
                        current->_left_alphabet->_parent_alphabet = parent;
                    }
                    current->_left_alphabet = parent;
                    current->_parent_alphabet = parent->_parent_alphabet;
                    parent->_parent_alphabet = current;
                    if (current->_parent_alphabet)
                    {
                        if (current->_parent_alphabet->_left_alphabet == parent)
                        {
                            current->_parent_alphabet->_left_alphabet = current;
                        }
                        else
                        {
                            current->_parent_alphabet->_right_alphabet = current;
                        }
                    }
                    else
                    {
                        _top_char_symbol = current;
                    }
                    parent->_bal_alphabet = 1;
                    current->_bal_alphabet = -1;
                    break;
                }
            }
            else if (parent->_bal_alphabet == -2)
            {
                char_symbol* current = parent->_left_alphabet;
                if (current->_bal_alphabet == 1)
                {
                    char_symbol* sub = current->_right_alphabet;
                    parent->_left_alphabet = sub->_right_alphabet;
                    if (sub->_right_alphabet)
                    {
                        sub->_right_alphabet->_parent_alphabet = parent;
                    }
                    current->_right_alphabet = sub->_left_alphabet;
                    if (sub->_left_alphabet)
                    {
                        sub->_left_alphabet->_parent_alphabet = current;
                    }
                    sub->_parent_alphabet = parent->_parent_alphabet;
                    sub->_right_alphabet = parent;
                    parent->_parent_alphabet = sub;
                    sub->_left_alphabet = current;
                    current->_parent_alphabet = sub;
                    if (sub->_parent_alphabet)
                    {
                        if (sub->_parent_alphabet->_right_alphabet == parent)
                        {
                            sub->_parent_alphabet->_right_alphabet = sub;
                        }
                        else
                        {
                            sub->_parent_alphabet->_left_alphabet = sub;
                        }
                    }
                    else
                    {
                        _top_char_symbol = sub;
                    }
                    parent->_bal_alphabet = (sub->_bal_alphabet == -1? 1: 0);
                    current->_bal_alphabet = (sub->_bal_alphabet == 1? -1: 0);
                    sub->_bal_alphabet = 0;
                    parent = sub;
                }
                else if (current->_bal_alphabet == -1)
                {
                    parent->_left_alphabet = current->_right_alphabet;
                    if (current->_right_alphabet)
                    {
                        current->_right_alphabet->_parent_alphabet = parent;
                    }
                    current->_right_alphabet = parent;
                    current->_parent_alphabet = parent->_parent_alphabet;
                    parent->_parent_alphabet = current;
                    if (current->_parent_alphabet)
                    {
                        if (current->_parent_alphabet->_right_alphabet == parent)
                        {
                            current->_parent_alphabet->_right_alphabet = current;
                        }
                        else
                        {
                            current->_parent_alphabet->_left_alphabet = current;
                        }
                    }
                    else
                    {
                        _top_char_symbol = current;
                    }
                    parent->_bal_alphabet = 0;
                    current->_bal_alphabet = 0;
                    parent = current;
                }
                else
                {
                    parent->_left_alphabet = current->_right_alphabet;
                    if (current->_right_alphabet)
                    {
                        current->_right_alphabet->_parent_alphabet = parent;
                    }
                    current->_right_alphabet = parent;
                    current->_parent_alphabet = parent->_parent_alphabet;
                    parent->_parent_alphabet = current;
                    if (current->_parent_alphabet)
                    {
                        if (current->_parent_alphabet->_right_alphabet == parent)
                        {
                            current->_parent_alphabet->_right_alphabet = current;
                        }
                        else
                        {
                            current->_parent_alphabet->_left_alphabet = current;
                        }
                    }
                    else
                    {
                        _top_char_symbol = current;
                    }
                    parent->_bal_alphabet = -1;
                    current->_bal_alphabet = 1;
                    break;
                }
            }
    
            if (parent->_parent_alphabet)
            {
                if (parent->_parent_alphabet->_left_alphabet == parent)
                {
                    parent->_parent_alphabet->_bal_alphabet++;
                }
                else
                {
                    parent->_parent_alphabet->_bal_alphabet--;
                }
            }
    
            parent = parent->_parent_alphabet;
        }
        
}


void alphabet::delete_all_char_symbol()
{
    
        assert(this);
    
        while (_top_char_symbol)
        {
            delete _top_char_symbol;
        }
        
}


void alphabet::replace_char_symbol(char_symbol* item, char_symbol* new_item)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_alphabet == this);
    
        assert(new_item);
        assert(new_item->_ref_alphabet == nullptr);
    
        if (item->get_cchar() == new_item->get_cchar())
        {
            if(  _first_char_symbol_iterator != nullptr )
            {
                _first_char_symbol_iterator->check(item, new_item);
            }
            if (_top_char_symbol == item)
            {
                _top_char_symbol = new_item;
            }
            if (item->_parent_alphabet)
            {
                if (item->_parent_alphabet->_left_alphabet == item)
                {
                    item->_parent_alphabet->_left_alphabet = new_item;
                }
                else if (item->_parent_alphabet->_right_alphabet == item)
                {
                    item->_parent_alphabet->_right_alphabet = new_item;
                }
            }
            /* fix by mel : missing update side references */
            if (item->_left_alphabet)
            {
                item->_left_alphabet->_parent_alphabet = new_item;
            }
            if (item->_right_alphabet)
            {
                item->_right_alphabet->_parent_alphabet = new_item;
            }
            /* end fix */
            new_item->_ref_alphabet = this;
            new_item->_parent_alphabet = item->_parent_alphabet;
            new_item->_left_alphabet = item->_left_alphabet;
            new_item->_right_alphabet = item->_right_alphabet;
            new_item->_bal_alphabet = item->_bal_alphabet;
            item->_ref_alphabet = nullptr;
            item->_parent_alphabet = nullptr;
            item->_left_alphabet = nullptr;
            item->_right_alphabet = nullptr;
            item->_bal_alphabet = 0;
        }
        else
        {
            if(  _first_char_symbol_iterator != nullptr )
            {
                _first_char_symbol_iterator->check(item);
            }
            remove_char_symbol(item);
            add_char_symbol(new_item);
        }
        
}


char_symbol* alphabet::get_first_char_symbol() const
{
    
        assert(this);
    
        char_symbol* result = _top_char_symbol;
        if (result)
        {
            while (result->_left_alphabet)
            {
                result = result->_left_alphabet;
            }
        }
    
        return result;
        
}


char_symbol* alphabet::get_last_char_symbol() const
{
    
        assert(this);
    
        char_symbol* result = _top_char_symbol;
        if (result)
        {
            while (result->_right_alphabet)
            {
                result = result->_right_alphabet;
            }
        }
    
        return result;
        
}


char_symbol* alphabet::get_next_char_symbol(char_symbol* pos) const
{
    
        assert(this);
    
        char_symbol* result = 0;
        if (pos == nullptr)
        {
            result = _top_char_symbol;
            if (result)
            {
                while (result->_left_alphabet)
                {
                    result = result->_left_alphabet;
                }
            }
        }
        else
        {
            assert(pos->_ref_alphabet == this);
    
            if (pos->_right_alphabet)
            {
                result = pos->_right_alphabet;
                while (result->_left_alphabet)
                {
                    result = result->_left_alphabet;
                }
            }
            else
            {
                result = pos->_parent_alphabet;
                while (result && result->_right_alphabet == pos)
                {
                    pos = result;
                    result = pos->_parent_alphabet;
                }
            }
        }
    
        return result;
        
}


char_symbol* alphabet::get_prev_char_symbol(char_symbol* pos) const
{
    
        assert(this);
    
        char_symbol* result = nullptr;
        if (pos == nullptr)
        {
            result = _top_char_symbol;
            if (result)
            {
                while (result->_right_alphabet)
                {
                    result = result->_right_alphabet;
                }
            }
        }
        else
        {
            assert(pos->_ref_alphabet == this);
    
            if (pos->_left_alphabet)
            {
                result = pos->_left_alphabet;
                while (result->_right_alphabet)
                {
                    result = result->_right_alphabet;
                }
            }
            else
            {
                result = pos->_parent_alphabet;
                while (result && result->_left_alphabet == pos)
                {
                    pos = result;
                    result = pos->_parent_alphabet;
                }
            }
        }
    
        return result;
        
}


size_t alphabet::get_char_symbol_count() const
{
    
        assert(this);
        return _count_char_symbol;
        
}


/**
* This method originates a new alphabet. The new alphabet directly originates deletion of previous alphabet and meta-analisys data.
**/
void alphabet::initialize()
{
    //cleanup
    delete_all_char_symbol();
    for(int i=0; i<256; i++)
    {
        new char_symbol(this, static_cast<char>(i));
    }
}


/**
* remove all symbols
**/
void alphabet::clear()
{
    delete_all_char_symbol();
}
/**
* Destructor.
**/
alphabet::~alphabet()
{
        __exit__();
}
// {user.before.class.char_symbol_iterator.begin}
// {user.before.class.char_symbol_iterator.end}



void alphabet::char_symbol_iterator::__exit__()
{
    
            if (_next)
            {
                _next->_prev = _prev;
            }
            else
            {
                 _iter_alphabet->_last_char_symbol_iterator = _prev;
            }
    
            if (_prev)
            {
                _prev->_next = _next;
            }
            else
            {
                _iter_alphabet->_first_char_symbol_iterator = _next;
            }
            
}


void alphabet::char_symbol_iterator::__init__(const alphabet* iter_alphabet, bool(char_symbol::*method)() const, char_symbol* ref_char_symbol)
{
    
        assert(iter_alphabet);
     
        _iter_alphabet = iter_alphabet;
        _ref_char_symbol = _prev_char_symbol = _next_char_symbol = ref_char_symbol;
        _prev = nullptr;
        _next = nullptr;
        _method = method;
        if ( _iter_alphabet->_last_char_symbol_iterator != nullptr )
        {
            _prev = _iter_alphabet->_last_char_symbol_iterator;
            _prev->_next = this;
            _iter_alphabet->_last_char_symbol_iterator = this;
        }
        else
        {
            _iter_alphabet->_first_char_symbol_iterator  = _iter_alphabet->_last_char_symbol_iterator  = this;
        }
        
}


void alphabet::char_symbol_iterator::check(char_symbol* item_char_symbol)
{
    
        for (char_symbol_iterator* item =  _iter_alphabet->_first_char_symbol_iterator; item; item = item->_next)
        {
            if (item->_prev_char_symbol == item_char_symbol)
            {
                item->_prev_char_symbol = item->_iter_alphabet->get_next_char_symbol(item->_prev_char_symbol);
                item->_ref_char_symbol = nullptr;
            }
            if (item->_next_char_symbol == item_char_symbol)
            {
                item->_next_char_symbol = item->_iter_alphabet->get_prev_char_symbol(item->_next_char_symbol);
                item->_ref_char_symbol = nullptr;
            }
        }
        
}


void alphabet::char_symbol_iterator::check(char_symbol* item_char_symbol, char_symbol* new_item_char_symbol)
{
    
        for (char_symbol_iterator* item = _iter_alphabet->_first_char_symbol_iterator; 
            item != nullptr ; item = item->_next)
        {
            if (item->_ref_char_symbol == item_char_symbol)
            {
                item->_ref_char_symbol =             item->_prev_char_symbol =             item->_next_char_symbol = new_item_char_symbol;
            }
            if (item->_prev_char_symbol == item_char_symbol)
            {
                item->_prev_char_symbol = new_item_char_symbol;
                item->_ref_char_symbol = nullptr;
            }
            if (item->_next_char_symbol == item_char_symbol)
            {
                item->_next_char_symbol = new_item_char_symbol;
                item->_ref_char_symbol = nullptr;
            }
        }
        
}
/**
* Destructor.
**/
alphabet::char_symbol_iterator::~char_symbol_iterator()
{
    
        __exit__();
        
}

// {user.after.class.char_symbol_iterator.begin}
// {user.after.class.char_symbol_iterator.end}


// {user.after.class.alphabet.begin}
// {user.after.class.alphabet.end}


