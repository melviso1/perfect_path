// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.oxstream.begin}
// {user.before.class.oxstream.end}

/**
* Constructor.
**/
serial::oxstream::oxstream(std::ostream& os)
: _ostream(os)
, _instance_map{}
{
}



/**
* Clean the buffer for serialized elements.
**/
void serial::oxstream::reset()
{
    _instance_map.clear();
}
/**
* Destructor.
**/
serial::oxstream::~oxstream()
{
}

// {user.after.class.oxstream.begin}
/** serialize a std::string as if integral type **/
template<> void serial::oxstream::write(const std::string& value)
{
    size_t sz = value.length();
    write(sz);
    _ostream.write(value.data(), sz);
}
// {user.after.class.oxstream.end}


