// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.level_digest.begin}
// {user.before.class.level_digest.end}

/**
* Constructor.
**/
kernel::meta::level_digest::level_digest(kernel::level* source)
:    _source{source}
,    _target{new kernel::level{}}
,    _current{nullptr}
,    _length{source->length()}
{
    size_t level_count=0;
    
    while(process_level(level_count++))
    #if 0
    {
        cout << "digest level "<< level_count <<"\n";
        cout << "--------------------\n";
        size_t len = _source->length();
        _source->reset();
        while( len-- )
        {
            char_symbol *current = dynamic_cast<char_symbol*>(_source->get());
            if( current == nullptr )
            {
                break;
            }
            if( current->get_cchar() == 0 )
            {
                cout << "\n";
            }
            else
            {
                cout << current->get_cchar();
            }
        }
    
    }
    #else
    ;
    #endif
    delete _target;
    _target = nullptr;
    if( level_count > 1 )
    {
    
        _target = _source;
    }    
}



/**
* Clone the steps of the input level to this one.
**/
void kernel::meta::level_digest::init_steps()
{
    kernel::symbol *p_symbol;
    const kernel::symbol *p_zero_symbol = _current->get_zero();
    kernel::level::step_iterator it{_current};
    while(++it)
    {
        p_symbol = it->get_symbol();
        if( p_symbol != p_zero_symbol)
        {
            new step{_target, p_symbol};
        }
        else
        {
            while(++it)
            {
                new step{_target, it->get_symbol()};
            }
            return;
        }
    }
}


/**
* Create the links based on the stats.
**/
void kernel::meta::level_digest::init_links()
{
    symbol *p_symbol;
    const symbol *p_zero_symbol = _current->get_zero();
    
    level::step_iterator it{_current};
    while(++it)
    {
        p_symbol = it->get_symbol();
        if( p_symbol != p_zero_symbol )
        {
            kernel::link *candidate = it->more_used_to_link();
            if( candidate != nullptr )
            {
                step *origin = p_symbol->find_step(_target);
                step *target = candidate->get_to_step()->get_symbol()->find_step(_target);
                new kernel::link{origin, target};
            }        
        }
        else
        {
            while(++it)
            {
                kernel::link *candidate = it->more_used_to_link();
                if( candidate != nullptr )
                {
                    kernel::step *origin = it->get_symbol()->find_step(_target);
                    kernel::step *target = candidate->get_to_step()->get_symbol()->find_step(_target);
                    new kernel::link{origin, target};
                }        
            }
            return;
        }
    }
}


/**
* create a upper symbol for each link that is not the more frequent to_node
**/
void kernel::meta::level_digest::update_links()
{
    kernel::symbol *p_symbol;
    const kernel::symbol *p_zero_symbol = _current->get_zero();
    kernel::level  *super = _target->get_super_level();
    
    level::step_iterator it{_current};
    while(++it)
    {
        p_symbol = it->get_symbol();
        if( p_symbol != p_zero_symbol )
        {
            kernel::link *candidate = it->more_used_from_link();
            if( candidate != nullptr )
            {
                //update other links
                step::from_link_iterator lit{it};
                while(++lit)
                {
                    if(lit.get() != candidate)
                    {
                        if( super == nullptr )
                        {
                            super = new kernel::level{};
                            _target->add_super_level(super);
                        }
                        new kernel::step{super, lit};
                    }
                }
            }        
        }
        else
        {
            while(++it)
            {
                kernel::link *candidate = it->more_used_from_link();
                if( candidate != nullptr )
                {
                    //update other links
                    step::from_link_iterator lit{it};
                    while(++lit)
                    {
                        if(lit.get() != candidate)
                        {
                            if( super == nullptr )
                            {
                                super = new kernel::level{};
                                _target->add_super_level(super);
                            }
                            new kernel::step{super, lit};
                        }
                    }
                }        
            }
            return;
        }
    }
}


/**
* Rebuild new level using the history from the previous level.
**/
void kernel::meta::level_digest::reparse()
{
    size_t history_length = _current->length();
    _current->reset();
    while( (history_length--) > 0)
    {
        _target->set(_current->get());
    }
}


/**
* Do digest over specific level.
**/
bool kernel::meta::level_digest::process_level(size_t level_count)
{
    size_t i=level_count;
    _current = _source;
    while(i>0)
    {
        if( nullptr == (_current = _current->get_super_level()) )
        {
            return false;
        }
        i--;
    }
    
    init_steps();
    init_links();
    update_links();
    reparse();
    
    /**
    When the level is zero we must replace _source by _target
    and create a new _target. Otherwhise we must replace
    the level in _source by _target and create a new target
    ***/
    size_t count = 0;
    kernel::level::step_iterator it{_target};
    kernel::step *p_delete = nullptr;
    while(++it)
    {
        if( p_delete != nullptr)
        {
            delete p_delete;
            p_delete = nullptr;
        }
        if( it->_activation_count == 0 )
        {
            if( it->get_symbol() != _target->get_zero() )
            {
                count++;            
                p_delete = it;
            }
        }
    }
    if( p_delete != nullptr)
    {
        delete p_delete;
        p_delete = nullptr;
    }
    if( count != 0 )
    {
        cout << "removed "<< count << " steps!!??\n";
    }
    
    if( level_count == 0 )
    {
        _source = _target;
    }
    else
    {
        kernel::level *container = _current->get_base_level();
        container->remove_super_level(_current);
        container->add_super_level(_target);
        delete _current;
        _current = nullptr;
    }    
    _target = new kernel::level{};
    
        
    return true;
}


/**
* return the digested level and release ownership.
**/
kernel::level* kernel::meta::level_digest::release()
{
    auto result = _target;
    _target = nullptr;
    return result;
}
/**
* Destructor.
**/
kernel::meta::level_digest::~level_digest()
{
    delete _target;
}

// {user.after.class.level_digest.begin}
// {user.after.class.level_digest.end}


