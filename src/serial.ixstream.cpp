// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.ixstream.begin}
// {user.before.class.ixstream.end}

/**
* Constructor.
**/
serial::ixstream::ixstream(std::istream& is)
: _istream(is)
, _instance_map{}
{
}



/**
* This method does the complete restore of a runtime object instance.
**/
serial::runtime* serial::ixstream::load()
{
    //read the object identifier
    size_t index = read<size_t>();
    //search the object in the already serialized map
    auto position = _instance_map.find(index);
    if( position != end(_instance_map) )
    {
       //no need to do anythng other that return previous value
       return position->second;
    }
    runtime *object_ptr = runtime::restore_type(_istream);
    assert( object_ptr != nullptr );
    _instance_map[index] = object_ptr;
    object_ptr->load(*this);
    return object_ptr;
}

// {user.after.class.ixstream.begin}
/** serialize a std::string as if integral type **/
template<> std::string serial::ixstream::read()
{
    size_t sz = read<size_t>();
    char *buffer = new char[sz];
    _istream.read(buffer, sz);
    std::string s(buffer,sz);
    delete [] buffer;
    return s;
}
// {user.after.class.ixstream.end}


