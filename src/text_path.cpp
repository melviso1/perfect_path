// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.text_path.begin}
#include <sstream>
#include <iostream>
#include <fstream>
// {user.before.class.text_path.end}

/**
* Constructor.
**/
text_path::text_path(const serial::type_record* _)
:    serial::runtime{}
,    serial::runtime_instance<text_path>{}
,    _alphabet_{ }
,    _base_level{new kernel::level{}}
{
}

/**
* Constructor.
**/
text_path::text_path()
:    serial::runtime{}
,    serial::runtime_instance<text_path>{}
,    _alphabet_{ }
,    _base_level{new kernel::level{}}
{
}



void text_path::in(const std::string& s)
{
    for(char item:s)
        in(item);
    in(0);     //this is the change toggle
}


void text_path::in(const char* s, size_t sz)
{
    for(size_t i=0;i<sz;i++)
        in(s[i]);
    in(0);     //this is the change toggle
}


std::string text_path::out(size_t max_length)
{
    std::stringstream ss;
    for( size_t count=0; count < max_length; count++ )
    {
        char_symbol *current = dynamic_cast<char_symbol*>(_base_level->get());
        if( current == nullptr )
        {
            break;
        }
        if( current->get_cchar() == 0 )
        {
            break;
        }
        ss << current->get_cchar();
    }
    return ss.str();
}


void text_path::in(char c)
{
    //Relay the level process the symbol
    _base_level->set(_alphabet_.find_char_symbol(c));
}


std::string text_path::out_reverse(size_t max_length)
{
    std::stringstream ss;
    for( size_t count=0; count < max_length; count++ )
    {
        char_symbol *current = dynamic_cast<char_symbol*>(_base_level->get_reverse());
        if( current == nullptr )
        {
            break;
        }
        if( current->get_cchar() == 0 )
        {
            break;
        }
        ss << current->get_cchar();
    }
    return ss.str();
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void text_path::save(serial::oxstream& os) const
{
    _alphabet_.save(os);
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void text_path::save_contents(serial::oxstream& os) const
{
    _alphabet_.save(os);
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void text_path::save_relations(serial::oxstream& os) const
{
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void text_path::load(serial::ixstream& is)
{
    _alphabet_.load(is);
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void text_path::load_contents(serial::ixstream& is)
{
    _alphabet_.load(is);
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void text_path::load_relations(serial::ixstream& is)
{
}


/**
* Almacena la estructura adquirida
**/
bool text_path::backup(std::string file)
{
    fstream f_out(file, ios_base::out | ios_base::binary | ios_base::trunc);
    if( ! f_out.is_open() )
    {
        return false;
    }
    serial::oxstream os(f_out);
    _alphabet_.save(os);
    _base_level->save(os);
    return true;
}


bool text_path::restore(std::string file)
{
    fstream f_in(file, ios_base::in | ios_base::binary);
    if( ! f_in.is_open() )
    {
        return false;
    }
    _alphabet_.clear();
    _base_level->clear();
    serial::ixstream is(f_in);
    _alphabet_.load(is);
    _base_level->load(is);
    return true;
}


/**
* Read a text dialog file. Each line account as an interlocution.
* Take account of that this don't reset current system state before reading the file.
**/
size_t text_path::load_talk(std::string file)
{
    ifstream f_in( file );
    if( ! f_in.good() )
        return size_t(-1);
    std::string text;
    size_t lines=0;
    while( getline(f_in, text) )
    {
        in(text);
        lines++;
    }
    f_in.close();
    return lines;
    
}


/**
* Replace the base level
**/
void text_path::replace(kernel::level* pkernel_level)
{
    if( pkernel_level != _base_level )
    {
        delete _base_level;
        _base_level = pkernel_level;
    }
}


/**
* Do a full digest of the system.
**/
void text_path::digest()
{
    replace(kernel::meta::level_digest(_base_level).release());
}
/**
* Destructor.
**/
text_path::~text_path()
{
    delete _base_level;
}

// {user.after.class.text_path.begin}
// {user.after.class.text_path.end}


