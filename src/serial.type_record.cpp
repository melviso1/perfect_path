// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.type_record.begin}
// {user.before.class.type_record.end}



void serial::type_record::__init__(serial::type_map* ptr_map)
{
    //init from relations:
    
    //init to relations:
    
        //avltree_owned_passive map -> type 
        _ref_map = nullptr;
        _parent_map = nullptr;
        _left_map = nullptr;
        _right_map = nullptr;
        _bal_map = 0;
        assert(ptr_map);
        ptr_map->add_type(this);
        
}


void serial::type_record::__exit__()
{
    //init from relations:
    
    //init to relations:
    
        if (_ref_map)
        {
            _ref_map->remove_type(this);
        }
        
}
/**
* Constructor.
**/
serial::type_record::type_record(const std::string& name)
:_name(name)
{
    __init__( type_map::get() );
}



serial::type_map* serial::type_record::get_map() const
{
    return  _ref_map;
}
/**
* Destructor.
**/
serial::type_record::~type_record()
{
        __exit__();
}

// {user.after.class.type_record.begin}
// {user.after.class.type_record.end}


