// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.symbol.begin}
// {user.before.class.symbol.end}



void kernel::symbol::__exit__()
{
    //init from relations:
    
        { 
            for (kernel::step* item = get_first_step(); item; item = get_first_step())
            {
                delete item; 
            }
        }
        
    //init to relations:
}
/**
* Constructor.
**/
kernel::symbol::symbol(const serial::type_record* _)
:    serial::runtime{}
,    serial::runtime_instance<symbol>{}
,    _first_step{nullptr}
,    _count_step{0}
,    _first_step_iterator{nullptr}
,    _last_step_iterator{nullptr}
{
}

/**
* Constructor.
**/
kernel::symbol::symbol()
:    serial::runtime{}
,    serial::runtime_instance<symbol>{}
,    _first_step{nullptr}
,    _count_step{0}
,    _first_step_iterator{nullptr}
,    _last_step_iterator{nullptr}
{
}



void kernel::symbol::add_step(kernel::step* item)
{
    
        assert(this);
        assert(item);
        assert(item->_ref_symbol == nullptr);
    
        _count_step++;
    
        item->_ref_symbol = this;
    
        if (_first_step)
        {
            kernel::step* current = _first_step;
            size_t bit = 0x1;
            while (1)
            {
                assert(current->get_ref_level() != item->get_ref_level());
    
                if ((reinterpret_cast<size_t>(current->get_ref_level()) & bit) == (reinterpret_cast<size_t>(item->get_ref_level()) & bit))
                {
                    if (current->_left_symbol)
                    {
                        current = current->_left_symbol;
                    }
                    else
                    {
                        current->_left_symbol = item;
                        item->_parent_symbol = current;
                        break;
                    }
                }
                else
                {
                    if (current->_right_symbol)
                    {
                        current = current->_right_symbol;
                    }
                    else
                    {
                        current->_right_symbol = item;
                        item->_parent_symbol = current;
                        break;
                    }
                }
    
                bit <<= 1;
            }
        }
        else
        {
            _first_step = item;
        }
}


void kernel::symbol::remove_step(kernel::step* item)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_symbol == this);
    
        if(  _first_step_iterator != nullptr )
        {
            _first_step_iterator->check(item);
        }
    
        _count_step--;
    
        kernel::step* replacement = 0;
        kernel::step* move = 0;
        if (item->_left_symbol)
        {
            replacement = item->_left_symbol;
            replacement->_parent_symbol = item->_parent_symbol;
            move = item->_right_symbol;
        }
        else if (item->_right_symbol)
        {
            replacement = item->_right_symbol;
            replacement->_parent_symbol = item->_parent_symbol;
        }
    
        kernel::step* parent = item->_parent_symbol;
        if (parent)
        {
            if (parent->_left_symbol == item)
            {
                parent->_left_symbol = replacement;
            }
            else
            {
                parent->_right_symbol = replacement;
            }
        }
        else
        {
            _first_step = replacement;
        }
    
        if (replacement)
        {
            while (1)
            {
                kernel::step* tmp = replacement->_right_symbol;
                replacement->_right_symbol = move;
                if (move)
                {
                    move->_parent_symbol = replacement;
                }
    
                if (!replacement->_left_symbol)
                {
                    if (tmp)
                    {
                        replacement->_left_symbol = tmp;
                        tmp = 0;
                    }
                    else
                    {
                        break;
                    }
                }
                move = tmp;
                replacement = replacement->_left_symbol;
            }
        }
    
        item->_ref_symbol = (kernel::symbol*)0;
        item->_parent_symbol = (kernel::step*)0;
        item->_left_symbol = (kernel::step*)0;
        item->_right_symbol = (kernel::step*)0;
}


void kernel::symbol::remove_all_step()
{
    
        assert(this);
    
        for (kernel::step* item = get_first_step(); item; item = get_first_step())
              remove_step(item);
}


void kernel::symbol::delete_all_step()
{
    
        assert(this);
    
        for (kernel::step* item = get_first_step(); item; item = get_first_step())
              delete item;
}


void kernel::symbol::replace_step(kernel::step* item, kernel::step* new_item)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_symbol == this);
    
        assert(new_item);
        assert(new_item->_ref_symbol == (kernel::symbol*)0);
    
        if (item->get_ref_level() == new_item->get_ref_level())
        {
            if(  _first_step_iterator != nullptr )
            {
                _first_step_iterator->check(item);
            }
    
            if (_first_step == item)
            {
                _first_step = new_item;
            }
            if (item->_parent_symbol)
            {
                if (item->_parent_symbol->_left_symbol == item)
                {
                    item->_parent_symbol->_left_symbol = new_item;
                }
                else if (item->_parent_symbol->_right_symbol == item)
                {
                    item->_parent_symbol->_right_symbol = new_item;
                }
            }
            new_item->_ref_symbol = this;
            new_item->_parent_symbol = item->_parent_symbol;
            new_item->_left_symbol = item->_left_symbol;
            new_item->_right_symbol = item->_right_symbol;
            item->_ref_symbol = (kernel::symbol*)0;
            item->_parent_symbol = (kernel::step*)0;
            item->_left_symbol = (kernel::step*)0;
            item->_right_symbol = (kernel::step*)0;
        }
        else
        {
            if(  _first_step_iterator != nullptr )
            {
                _first_step_iterator->check(item);
            }
            remove_step(item);
            add_step(new_item);
        }
}


kernel::step* kernel::symbol::get_first_step() const
{
    
        assert(this);
        return _first_step;
}


kernel::step* kernel::symbol::get_last_step() const
{
    
        assert(this);
    
        kernel::step* result = _first_step;
        while (result)
        {
            while (result->_right_symbol)
            {
                result = result->_right_symbol;
            }
    
            if (result->_left_symbol)
            {
                result = result->_left_symbol;
            }
            else
            {
                break;
            }
        }
    
        return result;
}


kernel::step* kernel::symbol::get_next_step(kernel::step* pos) const
{
    
        assert(this);
    
        kernel::step* result = 0;
        if (pos == (kernel::step*)0)
            result = _first_step;
        else
        {
            assert(pos->_ref_symbol == this);
    
            if (pos->_left_symbol)
            {
                result = pos->_left_symbol;
            }
            else
            {
                if (pos->_right_symbol)
                {
                    result = pos->_right_symbol;
                }
                else
                {
                    kernel::step* parent = pos->_parent_symbol;
                    while (parent && (parent->_right_symbol == 0 || parent->_right_symbol == pos))
                    {
                        pos = parent;
                        parent = parent->_parent_symbol;
                    }
    
                    if (parent)
                    {
                        result = parent->_right_symbol;
                    }
                }
            }
        }
    
        return result;
}


kernel::step* kernel::symbol::get_prev_step(kernel::step* pos) const
{
    
        assert(this);
    
        kernel::step* result = 0;
        if (pos == (kernel::step*)0)
        {
            result = get_last_step();
        }
        else
        {
            assert(pos->_ref_symbol == this);
    
            if (pos->_parent_symbol)
            {
                if (pos->_parent_symbol->_left_symbol == pos || pos->_parent_symbol->_left_symbol == 0)
                {
                    result = pos->_parent_symbol;
                }
                else /* Right branche and valid left branche */
                {
                    result = pos->_parent_symbol->_left_symbol;
                    while (1)
                    {
                        while (result->_right_symbol)
                        {
                            result = result->_right_symbol;
                        }
    
                        if (result->_left_symbol)
                        {
                            result = result->_left_symbol;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }
    
        return result;
}


size_t kernel::symbol::get_step_count() const
{
    
        assert(this);
        return _count_step;
}


kernel::step* kernel::symbol::find_step(kernel::level* const& value) const
{
    
        kernel::step* result = 0;
        if (_first_step)
        {
            kernel::step* item = _first_step;
            size_t bit = 0x1;
            while (1)
            {
                if (item->get_ref_level() == value)
                {
                    result = item;
                    break;
                }
    
                if ((reinterpret_cast<size_t>(item->get_ref_level()) & bit) == (reinterpret_cast<size_t>(value) & bit))
                {
                    if (item->_left_symbol)
                    {
                        item = item->_left_symbol;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    if (item->_right_symbol)
                    {
                        item = item->_right_symbol;
                    }
                    else
                    {
                        break;
                    }
                }
    
                bit <<= 1;
            }
        }
        return result;
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::symbol::save(serial::oxstream& os) const
{
    
    //serialize step childs 
    {
        size_t step_count = get_step_count();
        os.write(step_count);
        step_iterator iter{this};
        while(++iter)
        {
            os.save(iter.get());
        }
    }
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::symbol::save_contents(serial::oxstream& os) const
{
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::symbol::save_relations(serial::oxstream& os) const
{
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::symbol::load(serial::ixstream& is)
{
    
    //serialize step child 
    {
       size_t step_count = is.read<size_t>();
       while(step_count--)
       {
           add_step(dynamic_cast<step*>(is.load()));
       }
    }
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::symbol::load_contents(serial::ixstream& is)
{
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::symbol::load_relations(serial::ixstream& is)
{
    
    //serialize step child 
    {
       size_t step_count = is.read<size_t>();
       while(step_count--)
       {
           add_step(reinterpret_cast<step*>(is.read<size_t>()));
       }
    }
}


/**
* Create a step or return the existent one associated with the symbol in the level.
**/
kernel::step* kernel::symbol::associated_step(kernel::level* p_level)
{
    kernel::step *p_step = find_step(p_level);
    if ( p_step != nullptr )
    {
        return p_step;
    }
    return new kernel::step{p_level, this};
}
/**
* Destructor.
**/
kernel::symbol::~symbol()
{
        __exit__();
}
// {user.before.class.step_iterator.begin}
// {user.before.class.step_iterator.end}



void kernel::symbol::step_iterator::__exit__()
{
    
            if (_next)
            {
                _next->_prev = _prev;
            }
            else
            {
                 _iter_symbol->_last_step_iterator = _prev;
            }
    
            if (_prev)
            {
                _prev->_next = _next;
            }
            else
            {
                _iter_symbol->_first_step_iterator = _next;
            }
            
}


void kernel::symbol::step_iterator::__init__(const kernel::symbol* iter_symbol, bool(kernel::step::*method)() const, kernel::step* ref_step)
{
    
        assert(iter_symbol);
     
        _iter_symbol = iter_symbol;
        _ref_step = _prev_step = _next_step = ref_step;
        _prev = nullptr;
        _next = nullptr;
        _method = method;
        if ( _iter_symbol->_last_step_iterator != nullptr )
        {
            _prev = _iter_symbol->_last_step_iterator;
            _prev->_next = this;
            _iter_symbol->_last_step_iterator = this;
        }
        else
        {
            _iter_symbol->_first_step_iterator  = _iter_symbol->_last_step_iterator  = this;
        }
        
}


void kernel::symbol::step_iterator::check(kernel::step* item_step)
{
    
        for (step_iterator* item =  _iter_symbol->_first_step_iterator; item; item = item->_next)
        {
            if (item->_prev_step == item_step)
            {
                item->_prev_step = item->_iter_symbol->get_next_step(item->_prev_step);
                item->_ref_step = nullptr;
            }
            if (item->_next_step == item_step)
            {
                item->_next_step = item->_iter_symbol->get_prev_step(item->_next_step);
                item->_ref_step = nullptr;
            }
        }
        
}


void kernel::symbol::step_iterator::check(kernel::step* item_step, kernel::step* new_item_step)
{
    
        for (step_iterator* item = _iter_symbol->_first_step_iterator; 
            item != nullptr ; item = item->_next)
        {
            if (item->_ref_step == item_step)
            {
                item->_ref_step =             item->_prev_step =             item->_next_step = new_item_step;
            }
            if (item->_prev_step == item_step)
            {
                item->_prev_step = new_item_step;
                item->_ref_step = nullptr;
            }
            if (item->_next_step == item_step)
            {
                item->_next_step = new_item_step;
                item->_ref_step = nullptr;
            }
        }
        
}
/**
* Destructor.
**/
kernel::symbol::step_iterator::~step_iterator()
{
    
        __exit__();
        
}

// {user.after.class.step_iterator.begin}
// {user.after.class.step_iterator.end}


// {user.after.class.symbol.begin}
// {user.after.class.symbol.end}


