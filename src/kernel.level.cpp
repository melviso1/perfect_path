// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.level.begin}
// {user.before.class.level.end}



void kernel::level::__exit__()
{
    //init from relations:
    
        {
            for (step* item = get_first_step(); item; item = get_first_step())
            {
                delete item;
            }
        }
        
    
        if (_ref_active_step)
        {
            remove_active_step(_ref_active_step);
        }
        
    
        if (_ref_super_level)
        {
            remove_super_level(_ref_super_level);
        }
        
    //init to relations:
    
        assert(this);
        if (_ref_base_level)
        {
            assert(_ref_base_level->_ref_super_level == this);
            _ref_base_level->_ref_super_level = nullptr;
            _ref_base_level = nullptr;
        }
        
}
/**
* Constructor.
**/
kernel::level::level(const serial::type_record* _)
:    serial::runtime{}
,    serial::runtime_instance<level>{}
,    _next{nullptr}
,    _prev{nullptr}
,    _zero{ }
,    _ref_base_level{nullptr}
,    _first_step{nullptr}
,    _last_step{nullptr}
,    _count_step{0}
,    _first_step_iterator{nullptr}
,    _last_step_iterator{nullptr}
,    _ref_active_step{nullptr}
,    _ref_super_level{nullptr}
{
}

/**
* Constructor.
**/
kernel::level::level()
:    serial::runtime{}
,    serial::runtime_instance<level>{}
,    _next{nullptr}
,    _prev{nullptr}
,    _zero{ }
,    _ref_base_level{nullptr}
,    _first_step{nullptr}
,    _last_step{nullptr}
,    _count_step{0}
,    _first_step_iterator{nullptr}
,    _last_step_iterator{nullptr}
,    _ref_active_step{nullptr}
,    _ref_super_level{nullptr}
{
    (new step(this, &_zero))->activate();
}



/**
* 
* \param seed As next is a recursive process, the fist call must have seed=true in order to precompute next candidates.
**/
kernel::symbol* kernel::level::next(bool seed)
{
    symbol *p_symbol = nullptr;
    if( seed )
    {
        /**
        This is usually the first level, but in digest strategies
        it's interesting to start at higher levels.
        */
        p_symbol = update_next();
    }
    else if( _next != nullptr )
    {
        p_symbol = _next->get_symbol();
    }
    if( p_symbol == nullptr )
    {
        return nullptr;
    }
    kernel::level *base = get_base_level();
    if( !seed &&  base != nullptr )
    {
        link *p_link = dynamic_cast<link*>(p_symbol);
        if( p_link == nullptr )
            return nullptr;
        if( p_link->get_from_step() != base->get_active_step() )
        {
            return nullptr;
        }
    }
    kernel::link *p_link = get_active_step()->link_to_step(_next);
    if(! p_link->is_primary() )
    {
        super_put(p_link);
    }
    else
    {
        super_next();
    }
    get_next()->activate_forward();
    return p_symbol;
}


/**
* 
* \param seed As next is a recursive process, the fist call must have seed=true in order to precompute next candidates.
**/
kernel::symbol* kernel::level::prev(bool seed)
{
    /**
        prev must first return just the current symbol
    **/
    kernel::symbol *result = get_active_step()->get_symbol();
    if (result == get_zero())
        return nullptr;
        
    symbol *p_symbol = nullptr;
    if( seed )
    {
        p_symbol = update_prev();
    }
    else if( _prev != nullptr )
    {
        p_symbol = _prev->get_symbol();
    }
    if( p_symbol == nullptr )
    {
        return nullptr;
    }
    kernel::level *base = get_base_level();
    if(!seed  && base != nullptr )
    {
        step *p_step = get_active_step();
        link *p_link = dynamic_cast<link*>(p_step->get_symbol());
        if( p_link == nullptr )
            return nullptr;
        if( p_link->get_to_step() != base->get_active_step() )
        {
            return nullptr;
        }
    }
    if( get_super_level() != nullptr )
    {
        get_super_level()->prev();
    }
    get_prev()->activate_backward();
    return result;
}


/**
* Adquire a symbol for the context and decide to learn or contextualize it.
* \param seed As next is a recursive process, the fist call must have seed=true in order to precompute next candidates.
**/
void kernel::level::put(kernel::symbol* p_symbol, bool seed)
{
    /***
    Nomenclature:
        In order to simplify exposition:
            * unrealized step means a non-existent step or a existent step that was not used
              until now (with _activation_count and _max_activation_count equal to zero)
            * unrealized link means a non-existent link or a existent link that was not used
              until now (with _activation_count equal to zero)
            * a unrealized element can be realized by eiter create it o using it.
            * ensure a element means returning existent one or create a new element if no
              other exists for the requested dependency.
        To help digest features
    
    The symbol can be new or predicted.
    
        step 0  : ensure a step for the symbol in the level.
        step 1  : ensure a link for the symbol in the level.
    Cases:
        if _next is nullptr or target step, we ended
        
            
    ***/
    //prevent O(n^2) predictive computation
    if( seed )
        update_next();
        
    kernel::step* p_step = p_symbol->associated_step(this);
    kernel::link *p_link = get_active_step()->link_to_step(p_step);
    if(! p_link->is_primary() )
    {
        super_put(p_link);
    }
    else if(p_step == get_next())
    {
        super_next();
    }
    p_step->activate_forward(); 
}


/**
* Put a link to super level. If this doesn't exist, it will be created.
**/
void kernel::level::super_put(kernel::symbol* p_symbol)
{
    kernel::level *super = get_super_level();
    if( super == nullptr )
    {
        //a.2.1
        add_super_level(super = new level());
    }
    super->put(p_symbol);
}


/**
* Put a link to super level. If this doesn't exist, it will be created.
**/
void kernel::level::super_next()
{
    if( get_super_level() != nullptr )
    {
        get_super_level()->next();
    }
    
}


/**
* Update the prev step candidates for the level and upper ones.
* This method speeds up the process of selection nodes.
**/
kernel::symbol* kernel::level::update_prev()
{
    /**
       see next for explain
    **/
    
    level *start_level = this; //end point
    level *end_level = start_level; 
    while( end_level->get_super_level() != nullptr )
    {
        end_level = end_level->get_super_level();
    }
    //Discard out of time
    
    while( end_level->get_active_step()->future() )
    {
        if( end_level == start_level )
        {
            return nullptr;
        }
        end_level = end_level->get_base_level();
    }
    //now, operate in descent mode. For the first value
    //as it's the top upper level, the operative is easy
    symbol *p_symbol = nullptr;
    step *p_active = end_level->get_active_step();
    step *p_prev_step = p_active->get_natural_prev();
    end_level->set_prev(p_prev_step);
    while( end_level != start_level )
    {
        end_level = end_level->get_base_level();
        if( p_active != nullptr )
        {
            p_symbol = p_active->get_symbol();
            link *p_link = dynamic_cast<link*>(p_symbol);
            //p_link can be nullptr if this is the zero symbol
            if ( p_link != nullptr )
            {
                p_active = end_level->get_active_step();
                if( p_link->get_to_step() == p_active)
                {
                    p_prev_step = p_link->get_from_step();
                    end_level->set_prev(p_prev_step);
                    continue;
                }
            }
        }
        p_active = end_level->get_active_step();
        p_prev_step = p_active->get_natural_prev();
        end_level->set_prev(p_prev_step);
    }
    if( p_prev_step == nullptr )
    {
        return nullptr;
    }
    return p_prev_step->get_symbol();
}


/**
* Update the next step candidates for the level and upper ones.
* This method speeds up the process of selection nodes.
**/
kernel::symbol* kernel::level::update_next()
{
    /**
        This code is the recursive definition, easy to
        understand, but relying on unbounded stack
        
    _next = nullptr;
    link *p_link;
    if( get_super_level() == nullptr )
    {
        p_link = get_active_step()->get_first_to_link();
        if ( p_link == nullptr )
            return nullptr;
        _next = p_link->get_to_step();
        return _next->get_symbol();
    }
    symbol *pSymbol = get_super_level()->update_next();
    if ( pSymbol != nullptr )
    {
        p_link = dynamic_cast<link*>(pSymbol);
        if ( p_link != nullptr )
        {
            if ( p_link->get_from_step() == get_active_step() )
            {
                _next = p_link->get_to_step();
                return _next->get_symbol();                
            }
        }
    }
    //natural response
    p_link = get_active_step()->get_first_to_link();
    if ( p_link == nullptr )
        return nullptr;            
    _next = p_link->get_to_step();
    return _next->get_symbol();    
    
    //now, the equivalent, unbounded one
    **/
    
    level *start_level = this; //end point
    level *end_level = start_level; 
    while( end_level->get_super_level() != nullptr )
    {
        end_level = end_level->get_super_level();
    }
    //now, operate in descent mode. For the first value
    //as it's the top upper level, the operative is easy
    symbol *p_symbol = nullptr;
    step *p_next_step = end_level->get_active_step()->get_natural_next();
    end_level->set_next(p_next_step);
    while( end_level != start_level )
    {
        end_level = end_level->get_base_level();
        if( p_next_step != nullptr )
        {
            p_symbol = p_next_step->get_symbol();
            link *p_link = dynamic_cast<link*>(p_symbol);
            if( p_link->get_from_step() == end_level->get_active_step() )
            {
                p_next_step = p_link->get_to_step();
                end_level->set_next(p_next_step);
                continue;
            }
        }
        p_next_step = end_level->get_active_step()->get_natural_next();
        end_level->set_next(p_next_step);
    }
    if( p_next_step == nullptr )
    {
        return nullptr;
    }
    return p_next_step->get_symbol();
}


void kernel::level::info(int level_index)
{
    cout << "level "<<level_index << " : " << get_step_count() << " step nodes, ";
    size_t links = 0;
    step_iterator iter_step = step_iterator(this);
    while(++iter_step)
    {
        links += iter_step->get_to_link_count();
    }
    cout << links << " links between nodes\n";
    if( get_super_level() )
    {
        get_super_level()->info(level_index+1);
    }
}


/**
* Get the full symbol length. This method computes the length of the story at level by adding activation counters.
**/
size_t kernel::level::length()
{
    size_t sz{0};
    step_iterator iter_step = step_iterator(this);
    while(++iter_step)
    {
        sz += iter_step->_activation_count;
    }
    return sz;
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::level::save(serial::oxstream& os) const
{
    _zero.save(os);
    //serialize step childs 
    {
        size_t step_count = get_step_count();
        os.write(step_count);
        step_iterator iter{this};
        while(++iter)
        {
            os.save(iter.get());
        }
    }
    //serialize active_step child 
    {
        auto *ptr = const_cast<level*>(this)->get_active_step();
        size_t active_step_count = (ptr == nullptr)? 0: 1;
        os.write(active_step_count);
        if( active_step_count == 1 )
        {
            os.save( ptr );
       }
    }
    //serialize super_level child 
    {
        auto *ptr = const_cast<level*>(this)->get_super_level();
        size_t super_level_count = (ptr == nullptr)? 0: 1;
        os.write(super_level_count);
        if( super_level_count == 1 )
        {
            os.save( ptr );
       }
    }
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::level::save_contents(serial::oxstream& os) const
{
    _zero.save(os);
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::level::save_relations(serial::oxstream& os) const
{
    
    //local serialize active_step child 
    {
        auto *ptr = const_cast<level*>(this)->get_active_step();
        size_t active_step_count = (ptr == nullptr)? 0: 1;
        os.write(active_step_count);
        if( active_step_count == 1 )
        {
            os.write( reinterpret_cast<size_t>(ptr) );
       }
    }
    //local serialize super_level child 
    {
        auto *ptr = const_cast<level*>(this)->get_super_level();
        size_t super_level_count = (ptr == nullptr)? 0: 1;
        os.write(super_level_count);
        if( super_level_count == 1 )
        {
            os.write( reinterpret_cast<size_t>(ptr) );
       }
    }
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::level::load(serial::ixstream& is)
{
    _zero.load(is);
    //serialize step child 
    {
       size_t step_count = is.read<size_t>();
       while(step_count--)
       {
           add_step_last(dynamic_cast<step*>(is.load()));
       }
    }
    //serialize active_step child 
    {
       size_t active_step_count = is.read<size_t>();
       while(active_step_count--)
       {
           add_active_step(dynamic_cast<step*>(is.load()));
       }
    }
    //serialize super_level child 
    {
       size_t super_level_count = is.read<size_t>();
       while(super_level_count--)
       {
           add_super_level(dynamic_cast<level*>(is.load()));
       }
    }
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::level::load_contents(serial::ixstream& is)
{
    _zero.load(is);
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::level::load_relations(serial::ixstream& is)
{
    
    //serialize step child 
    {
       size_t step_count = is.read<size_t>();
       while(step_count--)
       {
           add_step_last(reinterpret_cast<step*>(is.read<size_t>()));
       }
    }
    //serialize active_step child 
    {
       size_t active_step_count = is.read<size_t>();
       while(active_step_count--)
       {
           add_active_step(reinterpret_cast<step*>(is.read<size_t>()));
       }
    }
    //serialize super_level child 
    {
       size_t super_level_count = is.read<size_t>();
       while(super_level_count--)
       {
           add_super_level(reinterpret_cast<level*>(is.read<size_t>()));
       }
    }
}


/**
* Set the level state and upper to initial state.
* This operation is recursively applied on upper levels.
* \param full if full is true, the step counters are also set to zero and history length is losed.
**/
void kernel::level::reset(bool full)
{
    _zero.find_step(this)->activate();
    step_iterator iter_step = step_iterator(this);
    while(++iter_step)
    {
        iter_step->reset(full);
    }
    /**
        Code equivalence :
            The next code is equivalent to this one, but we don't
        want to rely on unbounded stack:
            
    if( get_super_level() )
    {
        get_super_level()->reset(full);
    }
        linear code here:
    **/
    
    level *current_level = this;
    while( (current_level = current_level->get_super_level()) != nullptr )
    {
        current_level->reset(full);
    }
}


/**
* This method removes all the levels, links and steps over the invoked. It can be used before restoring a learning data.
**/
void kernel::level::clear()
{
    if(get_super_level())
    {
        delete get_super_level();
    }
    delete_all_step();
}


void kernel::level::init()
{
    clear();
    (new step(this, &_zero))->activate();
}


void kernel::level::add_step_first(kernel::step* item)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_level == nullptr);
        _count_step++;
        item->_ref_level = this;
        if (_first_step)
        {
            _first_step->_prev_level = item;
            item->_next_level = _first_step;
            _first_step = item;
        }
        else
        {
            _first_step = _last_step = item;
        }
}


void kernel::level::add_step_last(kernel::step* item)
{
    
        assert(this);
        assert(item);
        assert(item->_ref_level == nullptr);
        _count_step++;
        item->_ref_level = this;
        if (_last_step)
        {
            _last_step->_next_level = item;
            item->_prev_level = _last_step;
            _last_step = item;
        }
        else
        {
           _first_step = _last_step = item;
        }
}


void kernel::level::add_step_after(kernel::step* item, kernel::step* pos)
{
    
        assert(this);
    
        assert(item);
        assert(item->_ref_level == nullptr);
    
        assert(pos);
        assert(pos->_ref_level == this);
    
        _count_step++;
    
        item->_ref_level = this;
        item->_prev_level = pos;
        item->_next_level = pos->_next_level;
        pos->_next_level  = item;
    
        if (item->_next_level)
        {
            item->_next_level->_prev_level = item;
        }
        else
        {
            _last_step = item;
        }
}


void kernel::level::add_step_before(kernel::step* item, kernel::step* pos)
{
    
        assert( this );
    
        assert( item );
        assert( item->_ref_level == nullptr );
    
        assert( pos );
        assert(pos->_ref_level == this);
    
        _count_step++;
    
        item->_ref_level = this;
        item->_next_level = pos;
        item->_prev_level = pos->_prev_level;
        pos->_prev_level  = item;
    
        if (item->_prev_level)
        {
            item->_prev_level->_next_level = item;
        }
        else
        {
            _first_step = item;
        }
}


void kernel::level::remove_step(kernel::step* item)
{
    
    
        assert(this);
    
        assert(item);
        assert(item->_ref_level == this);
    
        if(  _first_step_iterator != nullptr )
        {
            _first_step_iterator->check(item);
        }
    
        _count_step--;
    
        if (item->_next_level)
        {
            item->_next_level->_prev_level = item->_prev_level;
        }
        else
        {
            _last_step = item->_prev_level;
        }
    
        if (item->_prev_level)
        {
            item->_prev_level->_next_level = item->_next_level;
        }
        else
        {
            _first_step = item->_next_level;
        }
    
        item->_prev_level = nullptr;
        item->_next_level = nullptr;
        item->_ref_level = nullptr;
}


void kernel::level::replace_step(kernel::step* item, kernel::step* new_item)
{
    
        assert(this);
        assert(item);
        assert(item->_ref_level == this);
    
        assert(new_item);
        assert(new_item->_ref_level == nullptr);
    
    
        if ( _first_step_iterator != nullptr )
        {
            _first_step_iterator->check(item, new_item);
        }
    
        if ( item->_next_level != nullptr )
        {
            item->_next_level->_prev_level = new_item;
        }
        else
        {
            _last_step = new_item;
        }
        if ( item->_prev_level != nullptr )
        {
            item->_prev_level->_next_level = new_item;
        }
        else
        {
            _first_step = new_item;
        }
    
        new_item->_next_level = item->_next_level;
        new_item->_prev_level = item->_prev_level;
        item->_next_level = nullptr;
        item->_prev_level = nullptr;
    
        item->_ref_level = nullptr;
        new_item->_ref_level = this;
}


void kernel::level::delete_all_step()
{
    
        assert(this);
        for (step* item = get_first_step(); item; item = get_first_step())
        {
              delete item;
        }
}


kernel::step* kernel::level::get_first_step() const
{
    
        assert(this);
        return _first_step;
}


kernel::step* kernel::level::get_last_step() const
{
    
        assert(this);
        return _last_step;
}


kernel::step* kernel::level::get_next_step(kernel::step* pos) const
{
    
        assert(this);
        if ( pos == nullptr )
        {
            return _first_step;
        }
        assert(pos);
        assert(pos->_ref_level == this);
        return pos->_next_level;
}


kernel::step* kernel::level::get_prev_step(kernel::step* pos) const
{
    
        assert(this);
    
        if ( pos == nullptr )
        {
            return _last_step;
        }
    
        assert(pos);
        assert(pos->_ref_level == this);
        return pos->_prev_level;
}


size_t kernel::level::get_step_count() const
{
    
        assert(this);
        return _count_step;
}


void kernel::level::move_step_first(kernel::step* item)
{
    
        assert(item);
        assert(item->_ref_level);
        item->_ref_level->remove_step(item);
        add_step_first(item);
}


void kernel::level::move_step_last(kernel::step* item)
{
    
        assert(item);
        assert(item->_ref_level);
        item->_ref_level->remove_step(item);
        add_step_last(item);
}


void kernel::level::move_step_after(kernel::step* item, kernel::step* pos)
{
    
        assert(item);
        assert(item->_ref_level);
        item->_ref_level->remove_step(item);
        add_step_after(item, pos);
}


void kernel::level::move_step_before(kernel::step* item, kernel::step* pos)
{
    
        assert(item);
        assert(item->_ref_level);
        item->_ref_level->remove_step(item);
        add_step_before(item, pos);
        
}


void kernel::level::sort_step(int (*compare)(step*,step*))
{
    
    
        for (step* a = get_first_step(); a != nullptr ; a = get_next_step(a))
        {
            step* b = get_next_step(a);
    
            while ( b != nullptr && compare(a, b) > 0 )
            {
                step* c = get_prev_step(a);
                while ( c != nullptr  && compare(c, b) > 0 )
                    c = get_prev_step(c);
                if (c != nullptr )
                {
                    move_step_after(b, c);
                }
                else
                {
                    move_step_first(b);
                }
                b = get_next_step(a);
            }
        }
}


void kernel::level::add_active_step(kernel::step* item)
{
    
        assert(this);
        assert(_ref_active_step == nullptr);
        assert(item);
        assert(item->_ref_active_level == nullptr);
    
        item->_ref_active_level = this;
        _ref_active_step = item;
}


void kernel::level::remove_active_step(kernel::step* item)
{
    
        assert(this);
        assert(_ref_active_step == item);
        assert(item);
        assert(item->_ref_active_level == this);
    
        item->_ref_active_level = nullptr;
        _ref_active_step = nullptr;
}


void kernel::level::replace_active_step(kernel::step* item, kernel::step* new_item)
{
    
        assert(this);
        assert(_ref_active_step == item);
        assert(item);
        assert(item->_ref_active_level == this);
        assert(new_item);
        assert(new_item->_ref_active_level == nullptr);
    
        item->_ref_active_level = nullptr;
        new_item->_ref_active_level = this;
        _ref_active_step = new_item;
        
}


void kernel::level::move_active_step(kernel::step* item)
{
    
        assert(item);
        assert(item->_ref_active_level);
        item->_ref_active_level->remove_active_step(item);
        add_active_step(item);
}


void kernel::level::add_super_level(kernel::level* item)
{
    
        assert(this);
        assert(_ref_super_level == nullptr);
        assert(item);
        assert(item->_ref_base_level == nullptr);
    
        item->_ref_base_level = this;
        _ref_super_level = item;
}


void kernel::level::remove_super_level(kernel::level* item)
{
    
        assert(this);
        assert(_ref_super_level == item);
        assert(item);
        assert(item->_ref_base_level == this);
    
        item->_ref_base_level = nullptr;
        _ref_super_level = nullptr;
}


void kernel::level::replace_super_level(kernel::level* item, kernel::level* new_item)
{
    
        assert(this);
        assert(_ref_super_level == item);
        assert(item);
        assert(item->_ref_base_level == this);
        assert(new_item);
        assert(new_item->_ref_base_level == nullptr);
    
        item->_ref_base_level = nullptr;
        new_item->_ref_base_level = this;
        _ref_super_level = new_item;
        
}


void kernel::level::move_super_level(kernel::level* item)
{
    
        assert(item);
        assert(item->_ref_base_level);
        item->_ref_base_level->remove_super_level(item);
        add_super_level(item);
}
#if 0


/**
* Move the level backward
**/
kernel::symbol* kernel::level::back()
{
    /**
        The back process must be equal to the next
        process with one slight difference: the
        steps with _activation_count equal to
        _backward_count don't can be taked in account
        because when the system travels back in 
        time that means these steps are not yet
        present
        
    ***/
    symbol *p_symbol = nullptr;
    if( get_base_level() == nullptr )
    {
        p_symbol = update_prev();
    }
    else if( _prev != nullptr )
    {
        p_symbol = _prev->get_symbol();
    }
    if( p_symbol == nullptr )
    {
        return nullptr;
    }
    if( get_base_level() != nullptr )
    {
        link *p_link = dynamic_cast<link*>(get_active_step()->get_symbol());
        if( p_link == nullptr )
            return nullptr;
        if( p_link->get_to_step() != get_base_level()->get_active_step() )
        {
            return nullptr;
        }
    }
    if( get_super_level() != nullptr )
    {
        get_super_level()->prev();
    }
    get_prev()->activate_backward();
    return p_symbol;
}
#endif


kernel::symbol* kernel::level::get_reverse()
{
    return prev(true);
}
/**
* Destructor.
**/
kernel::level::~level()
{
        __exit__();
        delete get_super_level();
}
// {user.before.class.step_iterator.begin}
// {user.before.class.step_iterator.end}



void kernel::level::step_iterator::__exit__()
{
    
            if (_next)
            {
                _next->_prev = _prev;
            }
            else
            {
                 _iter_level->_last_step_iterator = _prev;
            }
    
            if (_prev)
            {
                _prev->_next = _next;
            }
            else
            {
                _iter_level->_first_step_iterator = _next;
            }
            
}


void kernel::level::step_iterator::__init__(const kernel::level* iter_level, bool(kernel::step::*method)() const, kernel::step* ref_step)
{
    
        assert(iter_level);
     
        _iter_level = iter_level;
        _ref_step = _prev_step = _next_step = ref_step;
        _prev = nullptr;
        _next = nullptr;
        _method = method;
        if ( _iter_level->_last_step_iterator != nullptr )
        {
            _prev = _iter_level->_last_step_iterator;
            _prev->_next = this;
            _iter_level->_last_step_iterator = this;
        }
        else
        {
            _iter_level->_first_step_iterator  = _iter_level->_last_step_iterator  = this;
        }
        
}


void kernel::level::step_iterator::check(kernel::step* item_step)
{
    
        for (step_iterator* item =  _iter_level->_first_step_iterator; item; item = item->_next)
        {
            if (item->_prev_step == item_step)
            {
                item->_prev_step = item->_iter_level->get_next_step(item->_prev_step);
                item->_ref_step = nullptr;
            }
            if (item->_next_step == item_step)
            {
                item->_next_step = item->_iter_level->get_prev_step(item->_next_step);
                item->_ref_step = nullptr;
            }
        }
        
}


void kernel::level::step_iterator::check(kernel::step* item_step, kernel::step* new_item_step)
{
    
        for (step_iterator* item = _iter_level->_first_step_iterator; 
            item != nullptr ; item = item->_next)
        {
            if (item->_ref_step == item_step)
            {
                item->_ref_step =             item->_prev_step =             item->_next_step = new_item_step;
            }
            if (item->_prev_step == item_step)
            {
                item->_prev_step = new_item_step;
                item->_ref_step = nullptr;
            }
            if (item->_next_step == item_step)
            {
                item->_next_step = new_item_step;
                item->_ref_step = nullptr;
            }
        }
        
}
/**
* Destructor.
**/
kernel::level::step_iterator::~step_iterator()
{
    
        __exit__();
        
}

// {user.after.class.step_iterator.begin}
// {user.after.class.step_iterator.end}


// {user.after.class.level.begin}
// {user.after.class.level.end}


