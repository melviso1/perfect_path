// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.link.begin}
// {user.before.class.link.end}



void kernel::link::__init__(kernel::step* ptr_from_step, kernel::step* ptr_to_step)
{
    //init from relations:
    
    //init to relations:
    
        //multi_owned_passive from_step -> to_link
        {
            assert(this);
            assert(ptr_from_step);
            ptr_from_step->_count_to_link++;
        
            _ref_from_step = ptr_from_step;
        
            if (ptr_from_step->_last_to_link)
            {
                _next_from_step = nullptr;
                _prev_from_step = ptr_from_step->_last_to_link;
                _prev_from_step->_next_from_step = this;
                ptr_from_step->_last_to_link = this;
            }
            else
            {
                _prev_from_step = nullptr;
                _next_from_step = nullptr;
                ptr_from_step->_first_to_link = ptr_from_step->_last_to_link = this;
            }
        }
        
    
        //multi_owned_passive to_step -> from_link
        {
            assert(this);
            assert(ptr_to_step);
            ptr_to_step->_count_from_link++;
        
            _ref_to_step = ptr_to_step;
        
            if (ptr_to_step->_last_from_link)
            {
                _next_to_step = nullptr;
                _prev_to_step = ptr_to_step->_last_from_link;
                _prev_to_step->_next_to_step = this;
                ptr_to_step->_last_from_link = this;
            }
            else
            {
                _prev_to_step = nullptr;
                _next_to_step = nullptr;
                ptr_to_step->_first_from_link = ptr_to_step->_last_from_link = this;
            }
        }
        
}


void kernel::link::__exit__()
{
    //init from relations:
    
    //init to relations:
    
        assert(this);
        if ( _ref_from_step != nullptr )
        {
            _ref_from_step->remove_to_link(this);
        }
        
    
        assert(this);
        if ( _ref_to_step != nullptr )
        {
            _ref_to_step->remove_from_link(this);
        }
        
}
/**
* Constructor.
**/
kernel::link::link(const serial::type_record* _)
:    serial::runtime{}
,    kernel::symbol{_}
,    serial::runtime_instance<link>{}
,    _ref_from_step{nullptr}
,    _prev_from_step{nullptr}
,    _next_from_step{nullptr}
,    _ref_to_step{nullptr}
,    _prev_to_step{nullptr}
,    _next_to_step{nullptr}
,    _activation_count{0}
{
}

/**
* Constructor.
**/
kernel::link::link(kernel::step* ptr_from_step, kernel::step* ptr_to_step)
:    serial::runtime{}
,    kernel::symbol{}
,    serial::runtime_instance<link>{}
,    _ref_from_step{nullptr}
,    _prev_from_step{nullptr}
,    _next_from_step{nullptr}
,    _ref_to_step{nullptr}
,    _prev_to_step{nullptr}
,    _next_to_step{nullptr}
,    _activation_count{0}
{
    	__init__( ptr_from_step, ptr_to_step );
}



/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::link::save(serial::oxstream& os) const
{
    //save bases first
    kernel::symbol::save(os);
    os.write(_activation_count);
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::link::save_contents(serial::oxstream& os) const
{
    //save bases first
    kernel::symbol::save_contents(os);
    os.write(_activation_count);
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::link::save_relations(serial::oxstream& os) const
{
    //save bases first
    kernel::symbol::save_relations(os);
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::link::load(serial::ixstream& is)
{
    //load bases first
    kernel::symbol::load(is);
    _activation_count = is.read<size_t>();
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::link::load_contents(serial::ixstream& is)
{
    //load bases first
    kernel::symbol::load_contents(is);
    _activation_count = is.read<size_t>();
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::link::load_relations(serial::ixstream& is)
{
    //load bases first
    kernel::symbol::load_relations(is);
}


/**
* overload full virtual tables
**/
const serial::type_record& kernel::link::get_type() const
{
    return serial::runtime_instance<link>::get_type();
}
/**
* Destructor.
**/
kernel::link::~link()
{
        __exit__();
}

// {user.after.class.link.begin}
// {user.after.class.link.end}


