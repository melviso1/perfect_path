// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.char_symbol.begin}
// {user.before.class.char_symbol.end}



void char_symbol::__init__(alphabet* ptr_alphabet)
{
    //init from relations:
    
    //init to relations:
    
        //avltree_owned_passive alphabet -> char_symbol 
        _ref_alphabet = nullptr;
        _parent_alphabet = nullptr;
        _left_alphabet = nullptr;
        _right_alphabet = nullptr;
        _bal_alphabet = 0;
        assert(ptr_alphabet);
        ptr_alphabet->add_char_symbol(this);
        
}


void char_symbol::__exit__()
{
    //init from relations:
    
    //init to relations:
    
        if (_ref_alphabet)
        {
            _ref_alphabet->remove_char_symbol(this);
        }
        
}
/**
* Constructor.
**/
char_symbol::char_symbol(const serial::type_record* _)
:    serial::runtime{}
,    symbol{_}
,    serial::runtime_instance<char_symbol>{}
,    _ref_alphabet{nullptr}
,    _parent_alphabet{nullptr}
,    _left_alphabet{nullptr}
,    _right_alphabet{nullptr}
,    _bal_alphabet{0}
,    _cchar{}
{
}

/**
* Constructor.
**/
char_symbol::char_symbol(alphabet* ptr_alphabet, char cchar)
:    serial::runtime{}
,    symbol{}
,    serial::runtime_instance<char_symbol>{}
,    _ref_alphabet{nullptr}
,    _parent_alphabet{nullptr}
,    _left_alphabet{nullptr}
,    _right_alphabet{nullptr}
,    _bal_alphabet{0}
,    _cchar{cchar}
{
    	__init__( ptr_alphabet );
}



alphabet* char_symbol::get_alphabet() const
{
    return  _ref_alphabet;
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void char_symbol::save(serial::oxstream& os) const
{
    //save bases first
    symbol::save(os);
    os.write(_cchar);
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void char_symbol::save_contents(serial::oxstream& os) const
{
    //save bases first
    symbol::save_contents(os);
    os.write(_cchar);
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void char_symbol::save_relations(serial::oxstream& os) const
{
    //save bases first
    symbol::save_relations(os);
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void char_symbol::load(serial::ixstream& is)
{
    //load bases first
    symbol::load(is);
    _cchar = is.read<char>();
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void char_symbol::load_contents(serial::ixstream& is)
{
    //load bases first
    symbol::load_contents(is);
    _cchar = is.read<char>();
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void char_symbol::load_relations(serial::ixstream& is)
{
    //load bases first
    symbol::load_relations(is);
}


/**
* overload full virtual tables
**/
const serial::type_record& char_symbol::get_type() const
{
    return serial::runtime_instance<char_symbol>::get_type();
}
/**
* Destructor.
**/
char_symbol::~char_symbol()
{
        __exit__();
}

// {user.after.class.char_symbol.begin}
// {user.after.class.char_symbol.end}


