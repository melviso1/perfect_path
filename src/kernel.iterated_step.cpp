// {user.before.include.begin}
// {user.before.include.end}

#include "perfect_path.h"

#include <assert.h>

// {user.before.class.iterated_step.begin}
// {user.before.class.iterated_step.end}

/**
* Constructor.
**/
kernel::iterated_step::iterated_step(const serial::type_record* _)
:    serial::runtime{}
,    kernel::step{_}
,    serial::runtime_instance<iterated_step>{}
,    _period{}
,    _phase{}
{
}

/**
* Constructor.
**/
kernel::iterated_step::iterated_step(size_t period, kernel::step* step)
:    serial::runtime{}
,    kernel::step{step->get_level(),step->get_symbol()}
,    serial::runtime_instance<iterated_step>{}
,    _period{period}
,    _phase{period-1}
{
}



/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::iterated_step::save(serial::oxstream& os) const
{
    //save bases first
    kernel::step::save(os);
    os.write(_period);
    os.write(_phase);
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::iterated_step::save_contents(serial::oxstream& os) const
{
    //save bases first
    kernel::step::save_contents(os);
    os.write(_period);
    os.write(_phase);
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::iterated_step::save_relations(serial::oxstream& os) const
{
    //save bases first
    kernel::step::save_relations(os);
}


/**
* 
*             This method serializes the class and related descent ones. This method is used
*             for serializing the class for permanent storage.
*             
**/
void kernel::iterated_step::load(serial::ixstream& is)
{
    //load bases first
    kernel::step::load(is);
    _period = is.read<size_t>();
    _phase = is.read<size_t>();
}


/**
* 
*             This method serializes only the data members of the class (and bases), not his relations.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::iterated_step::load_contents(serial::ixstream& is)
{
    //load bases first
    kernel::step::load_contents(is);
    _period = is.read<size_t>();
    _phase = is.read<size_t>();
}


/**
* 
*             This method serializes only the relations of the class (and bases) in memory.
*             This method is intended to be used for transactional support.
*             
**/
void kernel::iterated_step::load_relations(serial::ixstream& is)
{
    //load bases first
    kernel::step::load_relations(is);
}


/**
* overload full virtual tables
**/
const serial::type_record& kernel::iterated_step::get_type() const
{
    return serial::runtime_instance<iterated_step>::get_type();
}
/**
* Destructor.
**/
kernel::iterated_step::~iterated_step()
{
}

// {user.after.class.iterated_step.begin}
// {user.after.class.iterated_step.end}


