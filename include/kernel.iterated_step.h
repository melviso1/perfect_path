/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(ITERATED_STEP_H_INCLUDED)
#define ITERATED_STEP_H_INCLUDED


// {user.before.class iterated_step.begin}
// {user.before.class iterated_step.end}

namespace kernel{

/**
* An iterated step is created when any symbol is produced at least twice.
**/
class iterated_step : public kernel::step,public serial::runtime_instance<iterated_step>
{

    // {user.inside.first.class iterated_step.begin}
    // {user.inside.first.class iterated_step.end}

    public:
    iterated_step(const serial::type_record* _);
    virtual void save(serial::oxstream& os) const;
    virtual void save_contents(serial::oxstream& os) const;
    virtual void save_relations(serial::oxstream& os) const;
    virtual void load(serial::ixstream& is);
    virtual void load_contents(serial::ixstream& is);
    virtual void load_relations(serial::ixstream& is);
    virtual const serial::type_record& get_type() const;
    private:
    size_t _period ;
    size_t _phase ;
    public:
    inline const size_t& get_period() const;
    iterated_step(size_t period, kernel::step* step);
    virtual ~iterated_step();
    // {user.inside.last.class iterated_step.begin}
    // {user.inside.last.class iterated_step.end}
};

}  // end namespace kernel

// {user.after.class iterated_step.begin}
// {user.after.class iterated_step.end}

#endif //ITERATED_STEP_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_ITERATED_STEP)
#define INCLUDE_INLINES_ITERATED_STEP



inline const size_t& kernel::iterated_step::get_period() const
{
    return _period;
}

#endif //(INCLUDE_INLINES_ITERATED_STEP)
#endif //INCLUDE_INLINES


