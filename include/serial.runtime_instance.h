/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(RUNTIME_INSTANCE_H_INCLUDED)
#define RUNTIME_INSTANCE_H_INCLUDED


// {user.before.template<typename T> class runtime_instance.begin}
// {user.before.template<typename T> class runtime_instance.end}

namespace serial{

template<typename T> class runtime_instance : public virtual serial::runtime
{

    // {user.inside.first.template<typename T> class runtime_instance.begin}
    // {user.inside.first.template<typename T> class runtime_instance.end}

    static serial::type_instance<T> _type ;
    protected:
    runtime_instance();
    public:
    virtual const serial::type_record& get_type() const;
    virtual ~runtime_instance();
    // {user.inside.last.template<typename T> class runtime_instance.begin}
    // {user.inside.last.template<typename T> class runtime_instance.end}
};

}  // end namespace serial

// {user.after.template<typename T> class runtime_instance.begin}
// {user.after.template<typename T> class runtime_instance.end}

#endif //RUNTIME_INSTANCE_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_RUNTIME_INSTANCE)
#define INCLUDE_INLINES_RUNTIME_INSTANCE

// {user.before.class.runtime_instance.begin}
// {user.before.class.runtime_instance.end}

template<typename T> serial::type_instance<T> serial::runtime_instance<T>::_type;
/**
* Constructor.
**/
template<typename T> serial::runtime_instance<T>::runtime_instance()
:    serial::runtime{}
{
}



/**
* make accessible the custom type to runtime base class.
**/
template<typename T> const serial::type_record& serial::runtime_instance<T>::get_type() const
{
    return _type;
}
/**
* Destructor.
**/
template<typename T> serial::runtime_instance<T>::~runtime_instance()
{
}

// {user.after.class.runtime_instance.begin}
// {user.after.class.runtime_instance.end}


#endif //(INCLUDE_INLINES_RUNTIME_INSTANCE)
#endif //INCLUDE_INLINES


