/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(CHAR_SYMBOL_H_INCLUDED)
#define CHAR_SYMBOL_H_INCLUDED


// {user.before.class char_symbol.begin}
// {user.before.class char_symbol.end}


class char_symbol : public kernel::symbol,public serial::runtime_instance<char_symbol>
{

    // {user.inside.first.class char_symbol.begin}
    // {user.inside.first.class char_symbol.end}

    friend class ::alphabet;
    alphabet* _ref_alphabet ;
    char_symbol* _parent_alphabet ;
    char_symbol* _left_alphabet ;
    char_symbol* _right_alphabet ;
    int _bal_alphabet ;
    public:
    inline alphabet* const& get_ref_alphabet() const;
    alphabet* get_alphabet() const;
    char_symbol(const serial::type_record* _);
    virtual void save(serial::oxstream& os) const;
    virtual void save_contents(serial::oxstream& os) const;
    virtual void save_relations(serial::oxstream& os) const;
    virtual void load(serial::ixstream& is);
    virtual void load_contents(serial::ixstream& is);
    virtual void load_relations(serial::ixstream& is);
    virtual const serial::type_record& get_type() const;
    private:
    void __init__(alphabet* ptr_alphabet);
    void __exit__();
    char _cchar ;
    public:
    inline const char get_cchar() const;
    char_symbol(alphabet* ptr_alphabet, char cchar);
    virtual ~char_symbol();
    // {user.inside.last.class char_symbol.begin}
    // {user.inside.last.class char_symbol.end}
};

// {user.after.class char_symbol.begin}
// {user.after.class char_symbol.end}

#endif //CHAR_SYMBOL_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_CHAR_SYMBOL)
#define INCLUDE_INLINES_CHAR_SYMBOL



inline alphabet* const& char_symbol::get_ref_alphabet() const
{
    return _ref_alphabet;
}


inline const char char_symbol::get_cchar() const
{
    return _cchar;
}

#endif //(INCLUDE_INLINES_CHAR_SYMBOL)
#endif //INCLUDE_INLINES


