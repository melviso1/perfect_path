/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(TYPE_INSTANCE_H_INCLUDED)
#define TYPE_INSTANCE_H_INCLUDED


// {user.before.template<typename T> class type_instance.begin}
// {user.before.template<typename T> class type_instance.end}

namespace serial{

/**
* This class represents the registration of a specific runtime type.
* Each specialization of this template class is a singleton implemented
* as static member of a specialized instance of runtime_class.
**/
template<typename T> class type_instance : public serial::type_record
{

    // {user.inside.first.template<typename T> class type_instance.begin}
    // {user.inside.first.template<typename T> class type_instance.end}

    public:
    type_instance();
    virtual serial::runtime* create() const;
    virtual ~type_instance();
    // {user.inside.last.template<typename T> class type_instance.begin}
    // {user.inside.last.template<typename T> class type_instance.end}
};

}  // end namespace serial

// {user.after.template<typename T> class type_instance.begin}
// {user.after.template<typename T> class type_instance.end}

#endif //TYPE_INSTANCE_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_TYPE_INSTANCE)
#define INCLUDE_INLINES_TYPE_INSTANCE

// {user.before.class.type_instance.begin}
// {user.before.class.type_instance.end}

/**
* Constructor.
**/
template<typename T> serial::type_instance<T>::type_instance()
: type_record(typeid(T).name())
{
}



/**
* This method is used for creating a new instances of a runtime class, on the fly.
* In order to avoid interferences with standard c++, these constructors use a type
* record pointer intended only used as a resolution identification. These kind of
* constructors will be used and maintained by beatle in an automated manner.
**/
template<typename T> serial::runtime* serial::type_instance<T>::create() const
{
    return dynamic_cast<serial::runtime* >(new T{this});
}
/**
* Destructor.
**/
template<typename T> serial::type_instance<T>::~type_instance()
{
}

// {user.after.class.type_instance.begin}
// {user.after.class.type_instance.end}


#endif //(INCLUDE_INLINES_TYPE_INSTANCE)
#endif //INCLUDE_INLINES


