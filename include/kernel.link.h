/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(LINK_H_INCLUDED)
#define LINK_H_INCLUDED


// {user.before.class link.begin}
// {user.before.class link.end}

namespace kernel{

class link : public kernel::symbol,public serial::runtime_instance<link>
{

    // {user.inside.first.class link.begin}
    // {user.inside.first.class link.end}

    friend class ::kernel::step;
    kernel::step* _ref_from_step ;
    kernel::link* _prev_from_step ;
    kernel::link* _next_from_step ;
    public:
    inline kernel::step* const& get_ref_from_step() const;
    inline kernel::step* get_from_step() const;
    private:
    kernel::step* _ref_to_step ;
    kernel::link* _prev_to_step ;
    kernel::link* _next_to_step ;
    public:
    inline kernel::step* const& get_ref_to_step() const;
    inline kernel::step* get_to_step() const;
    private:
    void __init__(kernel::step* ptr_from_step, kernel::step* ptr_to_step);
    void __exit__();
    public:
    link(const serial::type_record* _);
    virtual void save(serial::oxstream& os) const;
    virtual void save_contents(serial::oxstream& os) const;
    virtual void save_relations(serial::oxstream& os) const;
    virtual void load(serial::ixstream& is);
    virtual void load_contents(serial::ixstream& is);
    virtual void load_relations(serial::ixstream& is);
    virtual const serial::type_record& get_type() const;
    /**
    * The activation count of a link can be computed from the statistical information of the nodes, but is a complex and expensive operation. Instead, we store this value in the own link and update it dynamically.
    * The activation count is not altered by back operations but by full reset. Also is not altered by moves from the past.
    **/
    size_t _activation_count ;
    protected:
    inline const size_t& get_activation_count() const;
    inline void set_activation_count(const size_t& activation_count);
    public:
    inline void operator ++();
    link(kernel::step* ptr_from_step, kernel::step* ptr_to_step);
    virtual ~link();
    inline bool is_primary();
    // {user.inside.last.class link.begin}
    // {user.inside.last.class link.end}
};

}  // end namespace kernel

// {user.after.class link.begin}
// {user.after.class link.end}

#endif //LINK_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_LINK)
#define INCLUDE_INLINES_LINK



inline kernel::step* const& kernel::link::get_ref_from_step() const
{
    return _ref_from_step;
}


inline kernel::step* kernel::link::get_from_step() const
{
    return _ref_from_step;
}


inline kernel::step* const& kernel::link::get_ref_to_step() const
{
    return _ref_to_step;
}


inline kernel::step* kernel::link::get_to_step() const
{
    return _ref_to_step;
}


inline const size_t& kernel::link::get_activation_count() const
{
    return _activation_count;
}


inline void kernel::link::set_activation_count(const size_t& activation_count)
{
    _activation_count=activation_count;
}


/**
* increases activation count.
**/
inline void kernel::link::operator ++()
{
    _activation_count++;
}


/**
* A link is primary if is the first link from a node and the first link to a node.
**/
inline bool kernel::link::is_primary()
{
    return ( get_from_step()->get_first_to_link() == this && get_to_step()->get_first_from_link() == this);
}

#endif //(INCLUDE_INLINES_LINK)
#endif //INCLUDE_INLINES


