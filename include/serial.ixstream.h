/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(IXSTREAM_H_INCLUDED)
#define IXSTREAM_H_INCLUDED


// {user.before.class ixstream.begin}
#include <iostream>
#include <type_traits>
// {user.before.class ixstream.end}

namespace serial{

/**
* This class is intended to be used as special istream-like but able to
* handle nested serialization processes.
**/
class ixstream 
{

    // {user.inside.first.class ixstream.begin}
    // {user.inside.first.class ixstream.end}

    std::istream& _istream ;
    /**
    * This member stores the objects already serialized.
    * The process of serialization is responsible for calling
    * reset() before starting the whole process.
    **/
    std::map<size_t, runtime*> _instance_map ;
    public:
    ixstream(std::istream& is);
    serial::runtime* load();
    template<typename T> T read();
    // {user.inside.last.class ixstream.begin}
    // {user.inside.last.class ixstream.end}
};

}  // end namespace serial

// {user.after.class ixstream.begin}
//specific user override for serializing some types
template<> std::string serial::ixstream::read();
// {user.after.class ixstream.end}

#endif //IXSTREAM_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_IXSTREAM)
#define INCLUDE_INLINES_IXSTREAM



/**
* Do a raw serialization of type as buffer. Not intended to be used with pointers.
**/
template<typename T> T serial::ixstream::read()
{
    T value;
    _istream.read(reinterpret_cast<char*>(&value), sizeof(T));
    return value;
}

#endif //(INCLUDE_INLINES_IXSTREAM)
#endif //INCLUDE_INLINES


