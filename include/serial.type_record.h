/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(TYPE_RECORD_H_INCLUDED)
#define TYPE_RECORD_H_INCLUDED


// {user.before.class type_record.begin}
#include <string>
// {user.before.class type_record.end}

namespace serial{

/**
* This class represents the common base class for all
* the runtime types registered in the application.
**/
class type_record 
{

    // {user.inside.first.class type_record.begin}
    // {user.inside.first.class type_record.end}

    friend class ::serial::type_map;
    std::string _name ;
    public:
    inline const std::string& get_name() const;
    private:
    serial::type_map* _ref_map ;
    serial::type_record* _parent_map ;
    serial::type_record* _left_map ;
    serial::type_record* _right_map ;
    int _bal_map ;
    public:
    inline serial::type_map* const& get_ref_map() const;
    serial::type_map* get_map() const;
    private:
    void __init__(serial::type_map* ptr_map);
    void __exit__();
    protected:
    type_record(const std::string& name);
    public:
    virtual serial::runtime* create() const = 0;
    virtual ~type_record();
    // {user.inside.last.class type_record.begin}
    // {user.inside.last.class type_record.end}
};

}  // end namespace serial

// {user.after.class type_record.begin}
// {user.after.class type_record.end}

#endif //TYPE_RECORD_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_TYPE_RECORD)
#define INCLUDE_INLINES_TYPE_RECORD



inline const std::string& serial::type_record::get_name() const
{
    return _name;
}


inline serial::type_map* const& serial::type_record::get_ref_map() const
{
    return _ref_map;
}

#endif //(INCLUDE_INLINES_TYPE_RECORD)
#endif //INCLUDE_INLINES


