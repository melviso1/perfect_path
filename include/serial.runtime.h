/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(RUNTIME_H_INCLUDED)
#define RUNTIME_H_INCLUDED


// {user.before.class runtime.begin}
#include <iostream>
// {user.before.class runtime.end}

namespace serial{

/**
* This class represents a common base class for all
* the classes that must be serializable.
**/
class runtime 
{

    // {user.inside.first.class runtime.begin}
    // {user.inside.first.class runtime.end}

    protected:
    runtime();
    public:
    virtual const serial::type_record& get_type() const = 0;
    void store_type(std::ostream& os) const;
    static serial::runtime* restore_type(std::istream& is);
    virtual void save(serial::oxstream& os) const = 0;
    virtual void load(serial::ixstream& is) = 0;
    virtual ~runtime();
    // {user.inside.last.class runtime.begin}
    // {user.inside.last.class runtime.end}
};

}  // end namespace serial

// {user.after.class runtime.begin}
// {user.after.class runtime.end}

#endif //RUNTIME_H_INCLUDED



