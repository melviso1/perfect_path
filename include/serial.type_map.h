/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(TYPE_MAP_H_INCLUDED)
#define TYPE_MAP_H_INCLUDED


// {user.before.class type_map.begin}
// {user.before.class type_map.end}

namespace serial{

/**
* This class represents a dictionary holding all the
* runtime types registered in the application. This
* class is a singleton only accessible trough a static
* getter member. This is a workaround required by the
* absence of standard initialization order. 
**/
class type_map 
{

    // {user.inside.first.class type_map.begin}
    // {user.inside.first.class type_map.end}

    public:
    // {user.before.class type_iterator.begin}
    // {user.before.class type_iterator.end}


    class type_iterator 
    {

        // {user.inside.first.class type_iterator.begin}
        // {user.inside.first.class type_iterator.end}

        friend class ::serial::type_map;
        serial::type_record* _ref_type ;
        serial::type_record* _prev_type ;
        serial::type_record* _next_type ;
        const serial::type_map* _iter_map ;
        serial::type_map::type_iterator* _prev ;
        serial::type_map::type_iterator* _next ;
        bool(serial::type_record::*_method)() const ;
        void __init__(const serial::type_map* iter_map, bool(serial::type_record::*method)() const=nullptr, serial::type_record* ref_type=nullptr);
        public:
        inline serial::type_map::type_iterator& operator =(const serial::type_map::type_iterator& iterator);
        inline serial::type_record* operator ++();
        inline serial::type_record* operator --();
        inline  operator serial::type_record*();
        inline serial::type_record* operator ->();
        inline serial::type_record* get();
        inline void reset();
        inline bool is_first() const;
        inline bool is_last() const;
        protected:
        void check(serial::type_record* item_type);
        void check(serial::type_record* item_type, serial::type_record* new_item_type);
        private:
        void __exit__();
        public:
        type_iterator(const serial::type_map* iter_map, bool(serial::type_record::*method)() const=nullptr, serial::type_record* ref_type=nullptr);
        type_iterator(const serial::type_map& iter_map, bool(serial::type_record::*method)() const=nullptr, serial::type_record* ref_type=nullptr);
        type_iterator(const serial::type_map::type_iterator& iterator, bool(serial::type_record::*method)() const=nullptr);
        virtual ~type_iterator();
        // {user.inside.last.class type_iterator.begin}
        // {user.inside.last.class type_iterator.end}
    };

    // {user.after.class type_iterator.begin}
    // {user.after.class type_iterator.end}

    friend class ::serial::type_map::type_iterator;
    private:
    serial::type_record* _top_type ;
    size_t _count_type ;
    protected:
    mutable type_iterator* _first_type_iterator ;
    mutable type_iterator* _last_type_iterator ;
    public:
    serial::type_record* find_type(const std::string& value) const;
    void add_type(serial::type_record* item);
    void remove_type(serial::type_record* item);
    void delete_all_type();
    void replace_type(serial::type_record* item, serial::type_record* new_item);
    serial::type_record* get_first_type() const;
    serial::type_record* get_last_type() const;
    serial::type_record* get_next_type(serial::type_record* pos) const;
    serial::type_record* get_prev_type(serial::type_record* pos) const;
    size_t get_type_count() const;
    private:
    void __exit__();
    type_map();
    public:
    static serial::type_map* get();
    virtual ~type_map();
    // {user.inside.last.class type_map.begin}
    // {user.inside.last.class type_map.end}
};

}  // end namespace serial

// {user.after.class type_map.begin}
// {user.after.class type_map.end}

#endif //TYPE_MAP_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_TYPE_MAP)
#define INCLUDE_INLINES_TYPE_MAP



inline serial::type_map::type_iterator& serial::type_map::type_iterator::operator =(const serial::type_map::type_iterator& iterator)
{
    
        __exit__();
        __init__(iterator._iter_map, iterator._method, iterator._ref_type);
        return *this;
        
}


inline serial::type_record* serial::type_map::type_iterator::operator ++()
{
    
        _next_type = _iter_map->get_next_type(_next_type);
        if ( _method != nullptr )
        {
            while (_next_type != nullptr && !(_next_type->*_method)())
            {
                _next_type = _iter_map->get_next_type(_next_type);
            }
        }
        _ref_type = _prev_type = _next_type;
        return _ref_type;
        
}


inline serial::type_record* serial::type_map::type_iterator::operator --()
{
    
        _prev_type = _iter_map->get_prev_type(_prev_type);
        if (_method != 0)
        {
            while (_prev_type && !(_prev_type->*_method)())
            {
                _prev_type = _iter_map->get_prev_type(_prev_type);
            }
        }
        _ref_type = _next_type = _prev_type;
        return _ref_type;
        
}


inline  serial::type_map::type_iterator::operator serial::type_record*()
{
    
        return _ref_type;
        
}


inline serial::type_record* serial::type_map::type_iterator::operator ->()
{
    
        return _ref_type;
        
}


inline serial::type_record* serial::type_map::type_iterator::get()
{
    
        return _ref_type;
        
}


inline void serial::type_map::type_iterator::reset()
{
    
    _ref_type = _prev_type = _next_type = nullptr;
}


inline bool serial::type_map::type_iterator::is_first() const
{
    
        return (_iter_map->get_first_type() == _ref_type);
        
}


inline bool serial::type_map::type_iterator::is_last() const
{
    
        return (_iter_map->get_last_type() == _ref_type);
        
}
/**
* Constructor.
**/
inline serial::type_map::type_iterator::type_iterator(const serial::type_map* iter_map, bool(serial::type_record::*method)() const, serial::type_record* ref_type)
:    _ref_type{nullptr}
,    _prev_type{nullptr}
,    _next_type{nullptr}
,    _iter_map{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
         __init__(iter_map, method, ref_type);
        
}

/**
* Constructor.
**/
inline serial::type_map::type_iterator::type_iterator(const serial::type_map& iter_map, bool(serial::type_record::*method)() const, serial::type_record* ref_type)
:    _ref_type{nullptr}
,    _prev_type{nullptr}
,    _next_type{nullptr}
,    _iter_map{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
             __init__(&iter_map, method, ref_type);
            
}

/**
* Constructor.
**/
inline serial::type_map::type_iterator::type_iterator(const serial::type_map::type_iterator& iterator, bool(serial::type_record::*method)() const)
:    _ref_type{nullptr}
,    _prev_type{nullptr}
,    _next_type{nullptr}
,    _iter_map{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
        __init__(iterator._iter_map, iterator._method, iterator._ref_type);
        
}


#endif //(INCLUDE_INLINES_TYPE_MAP)
#endif //INCLUDE_INLINES


