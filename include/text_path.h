/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(TEXT_PATH_H_INCLUDED)
#define TEXT_PATH_H_INCLUDED


// {user.before.class text_path.begin}
#include <string>
// {user.before.class text_path.end}


/**
* Esta es una implementacion que utiliza la contextualizacion secuencial para generar cadenas de texto.
**/
class text_path : public serial::runtime_instance<text_path>
{

    // {user.inside.first.class text_path.begin}
    // {user.inside.first.class text_path.end}

    public:
    void in(const std::string& s);
    void in(const char* s, size_t sz);
    std::string out(size_t max_length);
    void in(char c);
    std::string out_reverse(size_t max_length);
    text_path(const serial::type_record* _);
    virtual void save(serial::oxstream& os) const;
    virtual void save_contents(serial::oxstream& os) const;
    virtual void save_relations(serial::oxstream& os) const;
    virtual void load(serial::ixstream& is);
    virtual void load_contents(serial::ixstream& is);
    virtual void load_relations(serial::ixstream& is);
    private:
    alphabet _alphabet_ ;
    kernel::level* _base_level ;
    public:
    inline kernel::level* get_base_level() const;
    text_path();
    virtual ~text_path();
    inline void reset(bool full=false);
    inline void info();
    bool backup(std::string file);
    bool restore(std::string file);
    inline size_t length();
    size_t load_talk(std::string file);
    void replace(kernel::level* pkernel_level);
    inline void clear();
    inline void init();
    void digest();
    // {user.inside.last.class text_path.begin}
    // {user.inside.last.class text_path.end}
};

// {user.after.class text_path.begin}
// {user.after.class text_path.end}

#endif //TEXT_PATH_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_TEXT_PATH)
#define INCLUDE_INLINES_TEXT_PATH



inline kernel::level* text_path::get_base_level() const
{
    return _base_level;
}


inline void text_path::reset(bool full)
{
    _base_level->reset(full);
}


inline void text_path::info()
{
    _base_level->info();
}


inline size_t text_path::length()
{
    return _base_level->length();
}


inline void text_path::clear()
{
    _base_level->clear();
}


inline void text_path::init()
{
    _base_level->init();
}

#endif //(INCLUDE_INLINES_TEXT_PATH)
#endif //INCLUDE_INLINES


