/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(LEVEL_DIGEST_H_INCLUDED)
#define LEVEL_DIGEST_H_INCLUDED


// {user.before.class level_digest.begin}
// {user.before.class level_digest.end}

namespace kernel::meta{

/**
* Do a digest over a source level. The digest is done by the following phases:
* 1) Create a step for each origin step asociated to the same symbols (excludding zero symbol).
* 2) Create one link from each step to another step (natural response) using the more frequent link used from node.
* 3) For each link to a node that is not the more frequent one, populate a symbol to upper level
* 4) Do a replay of the source level and take his output as input.
**/
class level_digest 
{

    // {user.inside.first.class level_digest.begin}
    // {user.inside.first.class level_digest.end}

    public:
    kernel::level* _source ;
    kernel::level* _target ;
    /**
    * This is the current level used as source to process.
    **/
    kernel::level* _current ;
    size_t _length ;
    inline kernel::level* const& get_source() const;
    level_digest(kernel::level* source);
    protected:
    void init_steps();
    void init_links();
    void update_links();
    void reparse();
    bool process_level(size_t level_count=0);
    public:
    kernel::level* release();
    virtual ~level_digest();
    // {user.inside.last.class level_digest.begin}
    // {user.inside.last.class level_digest.end}
};

}  // end namespace kernel::meta

// {user.after.class level_digest.begin}
// {user.after.class level_digest.end}

#endif //LEVEL_DIGEST_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_LEVEL_DIGEST)
#define INCLUDE_INLINES_LEVEL_DIGEST



inline kernel::level* const& kernel::meta::level_digest::get_source() const
{
    return _source;
}

#endif //(INCLUDE_INLINES_LEVEL_DIGEST)
#endif //INCLUDE_INLINES


