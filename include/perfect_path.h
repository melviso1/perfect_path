/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/***

 File    :perfect_path.h
 Created :05/06/2023 22:19:24
 Author  :mel viso

 Licensed under wxWidgets license

***/

#if !defined(PERFECT_PATH_H_INCLUDED)
#define PERFECT_PATH_H_INCLUDED

//standard includes
/**
context TEXT_SZ:
**/

#define TEXT_SZ 1024

/**
context INTERACTIVE:
**/

#define INTERACTIVE 1

/**
context WINDOWS:
**/

#define WINDOWS 0

//type definition for std::string
/* unspecified, dynamically created */
//type definition for std::ostream
/* unspecified, dynamically created */
//type definition for std::istream
/* unspecified, dynamically created */
//type definition for std::map
/* unspecified, dynamically created */

//forward references
namespace serial{
class runtime;
class type_record;
class type_map;
template<typename T> class type_instance;
template<typename T> class runtime_instance;
class oxstream;
class ixstream;
}
namespace kernel{
class step;
class symbol;
class link;
class level;
class iterated_step;
}
namespace kernel::meta{
class level_digest;
}
class char_symbol;
class alphabet;
class text_path;

#include "serial.runtime.h"
#include "serial.type_record.h"
#include "serial.type_map.h"
#include "serial.type_instance.h"
#include "serial.runtime_instance.h"
#include "serial.oxstream.h"
#include "serial.ixstream.h"
#include "kernel.step.h"
#include "kernel.symbol.h"
#include "kernel.link.h"
#include "kernel.level.h"
#include "kernel.iterated_step.h"
#include "kernel.meta.level_digest.h"
#include "char_symbol.h"
#include "alphabet.h"
#include "text_path.h"

#define INCLUDE_INLINES
#include "serial.runtime.h"
#include "serial.type_record.h"
#include "serial.type_map.h"
#include "serial.type_instance.h"
#include "serial.runtime_instance.h"
#include "serial.oxstream.h"
#include "serial.ixstream.h"
#include "kernel.step.h"
#include "kernel.symbol.h"
#include "kernel.link.h"
#include "kernel.level.h"
#include "kernel.iterated_step.h"
#include "kernel.meta.level_digest.h"
#include "char_symbol.h"
#include "alphabet.h"
#include "text_path.h"

#include "main.h"

#endif //PERFECT_PATH_H_INCLUDED

