/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(OXSTREAM_H_INCLUDED)
#define OXSTREAM_H_INCLUDED


// {user.before.class oxstream.begin}
#include <iostream>
#include <map>
#include <type_traits>
// {user.before.class oxstream.end}

namespace serial{

/**
* This class is intended to be used as special ostream-like but able to
* handle nested serialization processes.
**/
class oxstream 
{

    // {user.inside.first.class oxstream.begin}
    // {user.inside.first.class oxstream.end}

    std::ostream& _ostream ;
    /**
    * This member stores the objects already serialized.
    * The process of serialization is responsible for calling
    * reset() before starting the whole process.
    **/
    std::map<const runtime*, size_t> _instance_map ;
    public:
    oxstream(std::ostream& os);
    void reset();
    template<class T> void save(const T* object_ptr);
    template<typename T> void write(const T& value);
    virtual ~oxstream();
    // {user.inside.last.class oxstream.begin}
    // {user.inside.last.class oxstream.end}
};

}  // end namespace serial

// {user.after.class oxstream.begin}
//specific user override for serializing some types
template<> void serial::oxstream::write(const std::string& value);
// {user.after.class oxstream.end}

#endif //OXSTREAM_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_OXSTREAM)
#define INCLUDE_INLINES_OXSTREAM



/**
* This method does the complete storage of a runtime object instance
* including his type.
**/
template<class T> void serial::oxstream::save(const T* object_ptr)
{
    /** Due to multiple inheritance runtime cast is non-unique 
        if we don't use virtual inheritance. This is really ugly because 
        of C++ ctor initilization trailing.
    ***/
    const runtime *runtime_ptr = dynamic_cast<const runtime*>(object_ptr);//first verify if the object was already serialized
    auto position = _instance_map.find(runtime_ptr);
    if(  position != end(_instance_map) )
    {
       //only is required to write the reference, because the object was already serialized:
       write(position->second);
       return;
    }
    //get the address of the object as unique identifier.
    size_t value = reinterpret_cast<size_t>(runtime_ptr);
    write(value);
    //add to map sooner preventing nested calls
    _instance_map[runtime_ptr] = value;
    runtime_ptr->store_type(_ostream);
    runtime_ptr->save(*this);
}


/**
* Do a raw serialization of data as buffer. Not intended to be used with pointers.
**/
template<typename T> void serial::oxstream::write(const T& value)
{
    static_assert(std::is_arithmetic<T>::value);
    _ostream.write(reinterpret_cast<const char*>(&value), sizeof(T));
}

#endif //(INCLUDE_INLINES_OXSTREAM)
#endif //INCLUDE_INLINES


