/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(ALPHABET_H_INCLUDED)
#define ALPHABET_H_INCLUDED


// {user.before.class alphabet.begin}
// {user.before.class alphabet.end}


class alphabet : public serial::runtime_instance<alphabet>
{

    // {user.inside.first.class alphabet.begin}
    // {user.inside.first.class alphabet.end}

    public:
    // {user.before.class char_symbol_iterator.begin}
    // {user.before.class char_symbol_iterator.end}


    class char_symbol_iterator 
    {

        // {user.inside.first.class char_symbol_iterator.begin}
        // {user.inside.first.class char_symbol_iterator.end}

        friend class ::alphabet;
        char_symbol* _ref_char_symbol ;
        char_symbol* _prev_char_symbol ;
        char_symbol* _next_char_symbol ;
        const alphabet* _iter_alphabet ;
        alphabet::char_symbol_iterator* _prev ;
        alphabet::char_symbol_iterator* _next ;
        bool(char_symbol::*_method)() const ;
        void __init__(const alphabet* iter_alphabet, bool(char_symbol::*method)() const=nullptr, char_symbol* ref_char_symbol=nullptr);
        public:
        inline alphabet::char_symbol_iterator& operator =(const alphabet::char_symbol_iterator& iterator);
        inline char_symbol* operator ++();
        inline char_symbol* operator --();
        inline  operator char_symbol*();
        inline char_symbol* operator ->();
        inline char_symbol* get();
        inline void reset();
        inline bool is_first() const;
        inline bool is_last() const;
        protected:
        void check(char_symbol* item_char_symbol);
        void check(char_symbol* item_char_symbol, char_symbol* new_item_char_symbol);
        private:
        void __exit__();
        public:
        char_symbol_iterator(const alphabet* iter_alphabet, bool(char_symbol::*method)() const=nullptr, char_symbol* ref_char_symbol=nullptr);
        char_symbol_iterator(const alphabet& iter_alphabet, bool(char_symbol::*method)() const=nullptr, char_symbol* ref_char_symbol=nullptr);
        char_symbol_iterator(const alphabet::char_symbol_iterator& iterator, bool(char_symbol::*method)() const=nullptr);
        virtual ~char_symbol_iterator();
        // {user.inside.last.class char_symbol_iterator.begin}
        // {user.inside.last.class char_symbol_iterator.end}
    };

    // {user.after.class char_symbol_iterator.begin}
    // {user.after.class char_symbol_iterator.end}

    friend class ::char_symbol;
    friend class ::alphabet::char_symbol_iterator;
    alphabet(const serial::type_record* _);
    virtual void save(serial::oxstream& os) const;
    virtual void save_contents(serial::oxstream& os) const;
    virtual void save_relations(serial::oxstream& os) const;
    virtual void load(serial::ixstream& is);
    virtual void load_contents(serial::ixstream& is);
    virtual void load_relations(serial::ixstream& is);
    private:
    char_symbol* _top_char_symbol ;
    size_t _count_char_symbol ;
    protected:
    mutable char_symbol_iterator* _first_char_symbol_iterator ;
    mutable char_symbol_iterator* _last_char_symbol_iterator ;
    public:
    char_symbol* find_char_symbol(const char value) const;
    void add_char_symbol(char_symbol* item);
    void remove_char_symbol(char_symbol* item);
    void delete_all_char_symbol();
    void replace_char_symbol(char_symbol* item, char_symbol* new_item);
    char_symbol* get_first_char_symbol() const;
    char_symbol* get_last_char_symbol() const;
    char_symbol* get_next_char_symbol(char_symbol* pos) const;
    char_symbol* get_prev_char_symbol(char_symbol* pos) const;
    size_t get_char_symbol_count() const;
    private:
    void __exit__();
    public:
    alphabet();
    virtual ~alphabet();
    void initialize();
    void clear();
    // {user.inside.last.class alphabet.begin}
    // {user.inside.last.class alphabet.end}
};

// {user.after.class alphabet.begin}
// {user.after.class alphabet.end}

#endif //ALPHABET_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_ALPHABET)
#define INCLUDE_INLINES_ALPHABET



inline alphabet::char_symbol_iterator& alphabet::char_symbol_iterator::operator =(const alphabet::char_symbol_iterator& iterator)
{
    
        __exit__();
        __init__(iterator._iter_alphabet, iterator._method, iterator._ref_char_symbol);
        return *this;
        
}


inline char_symbol* alphabet::char_symbol_iterator::operator ++()
{
    
        _next_char_symbol = _iter_alphabet->get_next_char_symbol(_next_char_symbol);
        if ( _method != nullptr )
        {
            while (_next_char_symbol != nullptr && !(_next_char_symbol->*_method)())
            {
                _next_char_symbol = _iter_alphabet->get_next_char_symbol(_next_char_symbol);
            }
        }
        _ref_char_symbol = _prev_char_symbol = _next_char_symbol;
        return _ref_char_symbol;
        
}


inline char_symbol* alphabet::char_symbol_iterator::operator --()
{
    
        _prev_char_symbol = _iter_alphabet->get_prev_char_symbol(_prev_char_symbol);
        if (_method != 0)
        {
            while (_prev_char_symbol && !(_prev_char_symbol->*_method)())
            {
                _prev_char_symbol = _iter_alphabet->get_prev_char_symbol(_prev_char_symbol);
            }
        }
        _ref_char_symbol = _next_char_symbol = _prev_char_symbol;
        return _ref_char_symbol;
        
}


inline  alphabet::char_symbol_iterator::operator char_symbol*()
{
    
        return _ref_char_symbol;
        
}


inline char_symbol* alphabet::char_symbol_iterator::operator ->()
{
    
        return _ref_char_symbol;
        
}


inline char_symbol* alphabet::char_symbol_iterator::get()
{
    
        return _ref_char_symbol;
        
}


inline void alphabet::char_symbol_iterator::reset()
{
    
    _ref_char_symbol = _prev_char_symbol = _next_char_symbol = nullptr;
}


inline bool alphabet::char_symbol_iterator::is_first() const
{
    
        return (_iter_alphabet->get_first_char_symbol() == _ref_char_symbol);
        
}


inline bool alphabet::char_symbol_iterator::is_last() const
{
    
        return (_iter_alphabet->get_last_char_symbol() == _ref_char_symbol);
        
}
/**
* Constructor.
**/
inline alphabet::char_symbol_iterator::char_symbol_iterator(const alphabet* iter_alphabet, bool(char_symbol::*method)() const, char_symbol* ref_char_symbol)
:    _ref_char_symbol{nullptr}
,    _prev_char_symbol{nullptr}
,    _next_char_symbol{nullptr}
,    _iter_alphabet{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
         __init__(iter_alphabet, method, ref_char_symbol);
        
}

/**
* Constructor.
**/
inline alphabet::char_symbol_iterator::char_symbol_iterator(const alphabet& iter_alphabet, bool(char_symbol::*method)() const, char_symbol* ref_char_symbol)
:    _ref_char_symbol{nullptr}
,    _prev_char_symbol{nullptr}
,    _next_char_symbol{nullptr}
,    _iter_alphabet{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
             __init__(&iter_alphabet, method, ref_char_symbol);
            
}

/**
* Constructor.
**/
inline alphabet::char_symbol_iterator::char_symbol_iterator(const alphabet::char_symbol_iterator& iterator, bool(char_symbol::*method)() const)
:    _ref_char_symbol{nullptr}
,    _prev_char_symbol{nullptr}
,    _next_char_symbol{nullptr}
,    _iter_alphabet{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
        __init__(iterator._iter_alphabet, iterator._method, iterator._ref_char_symbol);
        
}


#endif //(INCLUDE_INLINES_ALPHABET)
#endif //INCLUDE_INLINES


