/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(MAIN_H_INCLUDED)
#define MAIN_H_INCLUDED
// {{user.before.declarations.begin}}
#include <cstdlib>
#include <csignal>
#include <unistd.h>
#include <iostream>
#include <string>
#include <algorithm> 
#include <sstream>
#include <fstream>

using namespace std;
// {{user.before.declarations.end}}


//non static global variables
extern text_path* text_data ;
extern volatile bool interrupt ;

//non template methods
int  main(int argc, char* argv[]);
void  start_text();
void  end_text();
void  sig_handler(int s);
void  install_signal_handler();
void  menu();
void  clear_screen();
void  dialog();
bool  test_direct_unicity();
bool  do_all_tests();
bool  test_direct_unicity_dialog();
bool  test_reverse();
bool  test_save_and_restore();
bool  test_history();
bool  test_load_talk();
bool  test_digest();
bool  test_no_duplicates();
bool  test_digest_reverse();

// {{user.after.declarations.begin}}
// {{user.after.declarations.end}}

#endif //MAIN_H_INCLUDED


