/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(LEVEL_H_INCLUDED)
#define LEVEL_H_INCLUDED


// {user.before.class level.begin}
// {user.before.class level.end}

namespace kernel{

class level : public serial::runtime_instance<level>
{

    // {user.inside.first.class level.begin}
    // {user.inside.first.class level.end}

    public:
    // {user.before.class step_iterator.begin}
    // {user.before.class step_iterator.end}


    class step_iterator 
    {

        // {user.inside.first.class step_iterator.begin}
        // {user.inside.first.class step_iterator.end}

        friend class ::kernel::level;
        kernel::step* _ref_step ;
        kernel::step* _prev_step ;
        kernel::step* _next_step ;
        const kernel::level* _iter_level ;
        kernel::level::step_iterator* _prev ;
        kernel::level::step_iterator* _next ;
        bool(kernel::step::*_method)() const ;
        void __init__(const kernel::level* iter_level, bool(kernel::step::*method)() const=nullptr, kernel::step* ref_step=nullptr);
        public:
        inline kernel::level::step_iterator& operator =(const kernel::level::step_iterator& iterator);
        inline kernel::step* operator ++();
        inline kernel::step* operator --();
        inline  operator step*();
        inline kernel::step* operator ->();
        inline kernel::step* get();
        inline void reset();
        inline bool is_first() const;
        inline bool is_last() const;
        protected:
        void check(kernel::step* item_step);
        void check(kernel::step* item_step, kernel::step* new_item_step);
        private:
        void __exit__();
        public:
        step_iterator(const kernel::level* iter_level, bool(kernel::step::*method)() const=nullptr, kernel::step* ref_step=nullptr);
        step_iterator(const kernel::level& iter_level, bool(kernel::step::*method)() const=nullptr, kernel::step* ref_step=nullptr);
        step_iterator(const kernel::level::step_iterator& iterator, bool(kernel::step::*method)() const=nullptr);
        virtual ~step_iterator();
        // {user.inside.last.class step_iterator.begin}
        // {user.inside.last.class step_iterator.end}
    };

    // {user.after.class step_iterator.begin}
    // {user.after.class step_iterator.end}

    friend class ::kernel::step;
    friend class ::kernel::level::step_iterator;
    kernel::symbol* next(bool seed=false);
    kernel::symbol* prev(bool seed=false);
    void put(kernel::symbol* p_symbol, bool seed=false);
    void super_put(kernel::symbol* p_symbol);
    void super_next();
    protected:
    kernel::symbol* update_prev();
    kernel::symbol* update_next();
    public:
    void info(int level_index=0);
    size_t length();
    level(const serial::type_record* _);
    virtual void save(serial::oxstream& os) const;
    virtual void save_contents(serial::oxstream& os) const;
    virtual void save_relations(serial::oxstream& os) const;
    virtual void load(serial::ixstream& is);
    virtual void load_contents(serial::ixstream& is);
    virtual void load_relations(serial::ixstream& is);
    private:
    /**
    * Used for compute responses
    **/
    kernel::step* _next ;
    kernel::step* _prev ;
    public:
    /**
    * This is the zero symbol, required for initial state.
    **/
    kernel::symbol _zero ;
    protected:
    inline kernel::step* const& get_next() const;
    inline void set_next(kernel::step* ref_next);
    inline void set_prev(kernel::step* const& prev);
    inline kernel::step* const& get_prev() const;
    public:
    inline const kernel::symbol* get_zero() const;
    void reset(bool full=false);
    void clear();
    void init();
    private:
    kernel::level* _ref_base_level ;
    public:
    inline kernel::level* get_base_level() const;
    private:
    kernel::step* _first_step ;
    kernel::step* _last_step ;
    int _count_step ;
    mutable step_iterator* _first_step_iterator ;
    mutable step_iterator* _last_step_iterator ;
    protected:
    void add_step_first(kernel::step* item);
    void add_step_last(kernel::step* item);
    void add_step_after(kernel::step* item, kernel::step* pos);
    void add_step_before(kernel::step* item, kernel::step* pos);
    void remove_step(kernel::step* item);
    void replace_step(kernel::step* item, kernel::step* new_item);
    public:
    void delete_all_step();
    kernel::step* get_first_step() const;
    kernel::step* get_last_step() const;
    kernel::step* get_next_step(kernel::step* pos) const;
    kernel::step* get_prev_step(kernel::step* pos) const;
    size_t get_step_count() const;
    void move_step_first(kernel::step* item);
    void move_step_last(kernel::step* item);
    void move_step_after(kernel::step* item, kernel::step* pos);
    void move_step_before(kernel::step* item, kernel::step* pos);
    void sort_step(int (*compare)(step*,step*));
    private:
    kernel::step* _ref_active_step ;
    public:
    void add_active_step(kernel::step* item);
    void remove_active_step(kernel::step* item);
    void replace_active_step(kernel::step* item, kernel::step* new_item);
    void move_active_step(kernel::step* item);
    inline kernel::step* get_active_step();
    private:
    kernel::level* _ref_super_level ;
    public:
    void add_super_level(kernel::level* item);
    void remove_super_level(kernel::level* item);
    void replace_super_level(kernel::level* item, kernel::level* new_item);
    void move_super_level(kernel::level* item);
    inline kernel::level* get_super_level();
    level();
    virtual ~level();
    private:
    void __exit__();
    #if 0
    kernel::symbol* back();
    #endif
    public:
    inline kernel::symbol* get();
    kernel::symbol* get_reverse();
    inline void set(kernel::symbol* p_symbol);
    // {user.inside.last.class level.begin}
    // {user.inside.last.class level.end}
};

}  // end namespace kernel

// {user.after.class level.begin}
// {user.after.class level.end}

#endif //LEVEL_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_LEVEL)
#define INCLUDE_INLINES_LEVEL



inline kernel::level::step_iterator& kernel::level::step_iterator::operator =(const kernel::level::step_iterator& iterator)
{
    
        __exit__();
        __init__(iterator._iter_level, iterator._method, iterator._ref_step);
        return *this;
        
}


inline kernel::step* kernel::level::step_iterator::operator ++()
{
    
        _next_step = _iter_level->get_next_step(_next_step);
        if ( _method != nullptr )
        {
            while (_next_step != nullptr && !(_next_step->*_method)())
            {
                _next_step = _iter_level->get_next_step(_next_step);
            }
        }
        _ref_step = _prev_step = _next_step;
        return _ref_step;
        
}


inline kernel::step* kernel::level::step_iterator::operator --()
{
    
        _prev_step = _iter_level->get_prev_step(_prev_step);
        if (_method != 0)
        {
            while (_prev_step && !(_prev_step->*_method)())
            {
                _prev_step = _iter_level->get_prev_step(_prev_step);
            }
        }
        _ref_step = _next_step = _prev_step;
        return _ref_step;
        
}


inline  kernel::level::step_iterator::operator step*()
{
    
        return _ref_step;
        
}


inline kernel::step* kernel::level::step_iterator::operator ->()
{
    
        return _ref_step;
        
}


inline kernel::step* kernel::level::step_iterator::get()
{
    
        return _ref_step;
        
}


inline void kernel::level::step_iterator::reset()
{
    
    _ref_step = _prev_step = _next_step = nullptr;
}


inline bool kernel::level::step_iterator::is_first() const
{
    
        return (_iter_level->get_first_step() == _ref_step);
        
}


inline bool kernel::level::step_iterator::is_last() const
{
    
        return (_iter_level->get_last_step() == _ref_step);
        
}
/**
* Constructor.
**/
inline kernel::level::step_iterator::step_iterator(const kernel::level* iter_level, bool(kernel::step::*method)() const, kernel::step* ref_step)
:    _ref_step{nullptr}
,    _prev_step{nullptr}
,    _next_step{nullptr}
,    _iter_level{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
         __init__(iter_level, method, ref_step);
        
}

/**
* Constructor.
**/
inline kernel::level::step_iterator::step_iterator(const kernel::level& iter_level, bool(kernel::step::*method)() const, kernel::step* ref_step)
:    _ref_step{nullptr}
,    _prev_step{nullptr}
,    _next_step{nullptr}
,    _iter_level{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
             __init__(&iter_level, method, ref_step);
            
}

/**
* Constructor.
**/
inline kernel::level::step_iterator::step_iterator(const kernel::level::step_iterator& iterator, bool(kernel::step::*method)() const)
:    _ref_step{nullptr}
,    _prev_step{nullptr}
,    _next_step{nullptr}
,    _iter_level{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
        __init__(iterator._iter_level, iterator._method, iterator._ref_step);
        
}



inline kernel::step* const& kernel::level::get_next() const
{
    return _next;
}


inline void kernel::level::set_next(kernel::step* ref_next)
{
    _next=ref_next;
}


inline void kernel::level::set_prev(kernel::step* const& prev)
{
    _prev=prev;
}


inline kernel::step* const& kernel::level::get_prev() const
{
    return _prev;
}


inline const kernel::symbol* kernel::level::get_zero() const
{
    return &_zero;
}


inline kernel::level* kernel::level::get_base_level() const
{
    return _ref_base_level;
}


inline kernel::step* kernel::level::get_active_step()
{
    
        return _ref_active_step; 
}


inline kernel::level* kernel::level::get_super_level()
{
    
        return _ref_super_level; 
}


inline kernel::symbol* kernel::level::get()
{
    return next(true);
}


inline void kernel::level::set(kernel::symbol* p_symbol)
{
    put(p_symbol, true);
}

#endif //(INCLUDE_INLINES_LEVEL)
#endif //INCLUDE_INLINES


