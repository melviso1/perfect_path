/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(STEP_H_INCLUDED)
#define STEP_H_INCLUDED


// {user.before.class step.begin}
// {user.before.class step.end}

namespace kernel{

class step : public serial::runtime_instance<step>
{

    // {user.inside.first.class step.begin}
    // {user.inside.first.class step.end}

    public:
    // {user.before.class to_link_iterator.begin}
    // {user.before.class to_link_iterator.end}


    class to_link_iterator 
    {

        // {user.inside.first.class to_link_iterator.begin}
        // {user.inside.first.class to_link_iterator.end}

        friend class ::kernel::step;
        kernel::link* _ref_to_link ;
        kernel::link* _prev_to_link ;
        kernel::link* _next_to_link ;
        const kernel::step* _iter_from_step ;
        kernel::step::to_link_iterator* _prev ;
        kernel::step::to_link_iterator* _next ;
        bool(kernel::link::*_method)() const ;
        void __init__(const kernel::step* iter_from_step, bool(kernel::link::*method)() const=nullptr, kernel::link* ref_to_link=nullptr);
        public:
        inline kernel::step::to_link_iterator& operator =(const kernel::step::to_link_iterator& iterator);
        inline kernel::link* operator ++();
        inline kernel::link* operator --();
        inline  operator kernel::link*();
        inline kernel::link* operator ->();
        inline kernel::link* get();
        inline void reset();
        inline bool is_first() const;
        inline bool is_last() const;
        protected:
        void check(kernel::link* item_to_link);
        void check(kernel::link* item_to_link, kernel::link* new_item_to_link);
        private:
        void __exit__();
        public:
        to_link_iterator(const kernel::step* iter_from_step, bool(kernel::link::*method)() const=nullptr, kernel::link* ref_to_link=nullptr);
        to_link_iterator(const kernel::step& iter_from_step, bool(kernel::link::*method)() const=nullptr, kernel::link* ref_to_link=nullptr);
        to_link_iterator(const kernel::step::to_link_iterator& iterator, bool(kernel::link::*method)() const=nullptr);
        virtual ~to_link_iterator();
        // {user.inside.last.class to_link_iterator.begin}
        // {user.inside.last.class to_link_iterator.end}
    };

    // {user.after.class to_link_iterator.begin}
    // {user.after.class to_link_iterator.end}

    // {user.before.class from_link_iterator.begin}
    // {user.before.class from_link_iterator.end}


    class from_link_iterator 
    {

        // {user.inside.first.class from_link_iterator.begin}
        // {user.inside.first.class from_link_iterator.end}

        friend class ::kernel::step;
        kernel::link* _ref_from_link ;
        kernel::link* _prev_from_link ;
        kernel::link* _next_from_link ;
        const kernel::step* _iter_to_step ;
        kernel::step::from_link_iterator* _prev ;
        kernel::step::from_link_iterator* _next ;
        bool(kernel::link::*_method)() const ;
        void __init__(const kernel::step* iter_to_step, bool(kernel::link::*method)() const=nullptr, kernel::link* ref_from_link=nullptr);
        public:
        inline kernel::step::from_link_iterator& operator =(const kernel::step::from_link_iterator& iterator);
        inline kernel::link* operator ++();
        inline kernel::link* operator --();
        inline  operator kernel::link*();
        inline kernel::link* operator ->();
        inline kernel::link* get();
        inline void reset();
        inline bool is_first() const;
        inline bool is_last() const;
        protected:
        void check(kernel::link* item_from_link);
        void check(kernel::link* item_from_link, kernel::link* new_item_from_link);
        private:
        void __exit__();
        public:
        from_link_iterator(const kernel::step* iter_to_step, bool(kernel::link::*method)() const=nullptr, kernel::link* ref_from_link=nullptr);
        from_link_iterator(const kernel::step& iter_to_step, bool(kernel::link::*method)() const=nullptr, kernel::link* ref_from_link=nullptr);
        from_link_iterator(const kernel::step::from_link_iterator& iterator, bool(kernel::link::*method)() const=nullptr);
        virtual ~from_link_iterator();
        // {user.inside.last.class from_link_iterator.begin}
        // {user.inside.last.class from_link_iterator.end}
    };

    // {user.after.class from_link_iterator.begin}
    // {user.after.class from_link_iterator.end}

    friend class ::kernel::link;
    friend class ::kernel::symbol;
    friend class ::kernel::level;
    friend class ::kernel::step::from_link_iterator;
    friend class ::kernel::step::to_link_iterator;
    private:
    kernel::level* _ref_level ;
    kernel::step* _prev_level ;
    kernel::step* _next_level ;
    public:
    inline kernel::level* const& get_ref_level() const;
    inline kernel::level* get_level() const;
    private:
    kernel::level* _ref_active_level ;
    public:
    inline kernel::level* get_active_level() const;
    private:
    kernel::symbol* _ref_symbol ;
    kernel::step* _parent_symbol ;
    kernel::step* _left_symbol ;
    kernel::step* _right_symbol ;
    public:
    inline kernel::symbol* const& get_ref_symbol() const;
    inline kernel::symbol* get_symbol() const;
    private:
    kernel::link* _first_to_link ;
    kernel::link* _last_to_link ;
    int _count_to_link ;
    mutable to_link_iterator* _first_to_link_iterator ;
    mutable to_link_iterator* _last_to_link_iterator ;
    protected:
    void add_to_link_first(kernel::link* item);
    void add_to_link_last(kernel::link* item);
    void add_to_link_after(kernel::link* item, kernel::link* pos);
    void add_to_link_before(kernel::link* item, kernel::link* pos);
    void remove_to_link(kernel::link* item);
    void replace_to_link(kernel::link* item, kernel::link* new_item);
    public:
    void delete_all_to_link();
    kernel::link* get_first_to_link() const;
    kernel::link* get_last_to_link() const;
    kernel::link* get_next_to_link(kernel::link* pos) const;
    kernel::link* get_prev_to_link(kernel::link* pos) const;
    size_t get_to_link_count() const;
    void move_to_link_first(kernel::link* item);
    void move_to_link_last(kernel::link* item);
    void move_to_link_after(kernel::link* item, kernel::link* pos);
    void move_to_link_before(kernel::link* item, kernel::link* pos);
    void sort_to_link(int (*compare)(kernel::link*,kernel::link*));
    private:
    kernel::link* _first_from_link ;
    kernel::link* _last_from_link ;
    int _count_from_link ;
    mutable from_link_iterator* _first_from_link_iterator ;
    mutable from_link_iterator* _last_from_link_iterator ;
    protected:
    void add_from_link_first(kernel::link* item);
    void add_from_link_last(kernel::link* item);
    void add_from_link_after(kernel::link* item, kernel::link* pos);
    void add_from_link_before(kernel::link* item, kernel::link* pos);
    void remove_from_link(kernel::link* item);
    void replace_from_link(kernel::link* item, kernel::link* new_item);
    public:
    void delete_all_from_link();
    kernel::link* get_first_from_link() const;
    kernel::link* get_last_from_link() const;
    kernel::link* get_next_from_link(kernel::link* pos) const;
    kernel::link* get_prev_from_link(kernel::link* pos) const;
    size_t get_from_link_count() const;
    void move_from_link_first(kernel::link* item);
    void move_from_link_last(kernel::link* item);
    void move_from_link_after(kernel::link* item, kernel::link* pos);
    void move_from_link_before(kernel::link* item, kernel::link* pos);
    void sort_from_link(int (*compare)(kernel::link*,kernel::link*));
    /**
    * This value stores the number of times the node was activated. This value is preserved when moving backward in time. This value is only reset to zero when the level is reset. This case, if not full argument is true, is stored into _max_activation_count.
    **/
    size_t _activation_count ;
    /**
    * Representa el numero de veces que este nodo determino correctamente la respuesta a partir de su enlace.
    * Ojo: solo se cuentan las veces que el nodo predeca correctamente la respuesta, aunque la respuesta hubiese sido diferente debido al condicionamiento intreoducido por otro supernodo.
    **/
    size_t _success_predictor ;
    /**
    * When the system is reset, this field stores the maximum activation count reached by the step, while the _activation_count is reset to zero. This is useful for replay and digest.
    **/
    size_t _max_activation_count ;
    /**
    * This counter is used when backward steps.
    **/
    size_t _backward_count ;
    kernel::link* more_used_to_link();
    kernel::link* more_used_from_link();
    step(const serial::type_record* _);
    virtual void save(serial::oxstream& os) const;
    virtual void save_contents(serial::oxstream& os) const;
    virtual void save_relations(serial::oxstream& os) const;
    virtual void load(serial::ixstream& is);
    virtual void load_contents(serial::ixstream& is);
    virtual void load_relations(serial::ixstream& is);
    void activate_backward();
    void activate_forward();
    protected:
    void activate();
    private:
    void __exit__();
    void __init__(kernel::level* ptr_level, kernel::symbol* ptr_symbol);
    public:
    step(kernel::level* ptr_level, kernel::symbol* ptr_symbol);
    virtual ~step();
    kernel::link* find_link_to(kernel::step* p_step);
    void reset(bool full=false);
    kernel::step* get_natural_next();
    kernel::step* get_natural_prev();
    bool future();
    kernel::link* link_to_step(kernel::step* p_step);
    // {user.inside.last.class step.begin}
    // {user.inside.last.class step.end}
};

}  // end namespace kernel

// {user.after.class step.begin}
// {user.after.class step.end}

#endif //STEP_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_STEP)
#define INCLUDE_INLINES_STEP



inline kernel::step::to_link_iterator& kernel::step::to_link_iterator::operator =(const kernel::step::to_link_iterator& iterator)
{
    
        __exit__();
        __init__(iterator._iter_from_step, iterator._method, iterator._ref_to_link);
        return *this;
        
}


inline kernel::link* kernel::step::to_link_iterator::operator ++()
{
    
        _next_to_link = _iter_from_step->get_next_to_link(_next_to_link);
        if ( _method != nullptr )
        {
            while (_next_to_link != nullptr && !(_next_to_link->*_method)())
            {
                _next_to_link = _iter_from_step->get_next_to_link(_next_to_link);
            }
        }
        _ref_to_link = _prev_to_link = _next_to_link;
        return _ref_to_link;
        
}


inline kernel::link* kernel::step::to_link_iterator::operator --()
{
    
        _prev_to_link = _iter_from_step->get_prev_to_link(_prev_to_link);
        if (_method != 0)
        {
            while (_prev_to_link && !(_prev_to_link->*_method)())
            {
                _prev_to_link = _iter_from_step->get_prev_to_link(_prev_to_link);
            }
        }
        _ref_to_link = _next_to_link = _prev_to_link;
        return _ref_to_link;
        
}


inline  kernel::step::to_link_iterator::operator kernel::link*()
{
    
        return _ref_to_link;
        
}


inline kernel::link* kernel::step::to_link_iterator::operator ->()
{
    
        return _ref_to_link;
        
}


inline kernel::link* kernel::step::to_link_iterator::get()
{
    
        return _ref_to_link;
        
}


inline void kernel::step::to_link_iterator::reset()
{
    
    _ref_to_link = _prev_to_link = _next_to_link = nullptr;
}


inline bool kernel::step::to_link_iterator::is_first() const
{
    
        return (_iter_from_step->get_first_to_link() == _ref_to_link);
        
}


inline bool kernel::step::to_link_iterator::is_last() const
{
    
        return (_iter_from_step->get_last_to_link() == _ref_to_link);
        
}
/**
* Constructor.
**/
inline kernel::step::to_link_iterator::to_link_iterator(const kernel::step* iter_from_step, bool(kernel::link::*method)() const, kernel::link* ref_to_link)
:    _ref_to_link{nullptr}
,    _prev_to_link{nullptr}
,    _next_to_link{nullptr}
,    _iter_from_step{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
         __init__(iter_from_step, method, ref_to_link);
        
}

/**
* Constructor.
**/
inline kernel::step::to_link_iterator::to_link_iterator(const kernel::step& iter_from_step, bool(kernel::link::*method)() const, kernel::link* ref_to_link)
:    _ref_to_link{nullptr}
,    _prev_to_link{nullptr}
,    _next_to_link{nullptr}
,    _iter_from_step{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
             __init__(&iter_from_step, method, ref_to_link);
            
}

/**
* Constructor.
**/
inline kernel::step::to_link_iterator::to_link_iterator(const kernel::step::to_link_iterator& iterator, bool(kernel::link::*method)() const)
:    _ref_to_link{nullptr}
,    _prev_to_link{nullptr}
,    _next_to_link{nullptr}
,    _iter_from_step{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
        __init__(iterator._iter_from_step, iterator._method, iterator._ref_to_link);
        
}



inline kernel::step::from_link_iterator& kernel::step::from_link_iterator::operator =(const kernel::step::from_link_iterator& iterator)
{
    
        __exit__();
        __init__(iterator._iter_to_step, iterator._method, iterator._ref_from_link);
        return *this;
        
}


inline kernel::link* kernel::step::from_link_iterator::operator ++()
{
    
        _next_from_link = _iter_to_step->get_next_from_link(_next_from_link);
        if ( _method != nullptr )
        {
            while (_next_from_link != nullptr && !(_next_from_link->*_method)())
            {
                _next_from_link = _iter_to_step->get_next_from_link(_next_from_link);
            }
        }
        _ref_from_link = _prev_from_link = _next_from_link;
        return _ref_from_link;
        
}


inline kernel::link* kernel::step::from_link_iterator::operator --()
{
    
        _prev_from_link = _iter_to_step->get_prev_from_link(_prev_from_link);
        if (_method != 0)
        {
            while (_prev_from_link && !(_prev_from_link->*_method)())
            {
                _prev_from_link = _iter_to_step->get_prev_from_link(_prev_from_link);
            }
        }
        _ref_from_link = _next_from_link = _prev_from_link;
        return _ref_from_link;
        
}


inline  kernel::step::from_link_iterator::operator kernel::link*()
{
    
        return _ref_from_link;
        
}


inline kernel::link* kernel::step::from_link_iterator::operator ->()
{
    
        return _ref_from_link;
        
}


inline kernel::link* kernel::step::from_link_iterator::get()
{
    
        return _ref_from_link;
        
}


inline void kernel::step::from_link_iterator::reset()
{
    
    _ref_from_link = _prev_from_link = _next_from_link = nullptr;
}


inline bool kernel::step::from_link_iterator::is_first() const
{
    
        return (_iter_to_step->get_first_from_link() == _ref_from_link);
        
}


inline bool kernel::step::from_link_iterator::is_last() const
{
    
        return (_iter_to_step->get_last_from_link() == _ref_from_link);
        
}
/**
* Constructor.
**/
inline kernel::step::from_link_iterator::from_link_iterator(const kernel::step* iter_to_step, bool(kernel::link::*method)() const, kernel::link* ref_from_link)
:    _ref_from_link{nullptr}
,    _prev_from_link{nullptr}
,    _next_from_link{nullptr}
,    _iter_to_step{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
         __init__(iter_to_step, method, ref_from_link);
        
}

/**
* Constructor.
**/
inline kernel::step::from_link_iterator::from_link_iterator(const kernel::step& iter_to_step, bool(kernel::link::*method)() const, kernel::link* ref_from_link)
:    _ref_from_link{nullptr}
,    _prev_from_link{nullptr}
,    _next_from_link{nullptr}
,    _iter_to_step{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
             __init__(&iter_to_step, method, ref_from_link);
            
}

/**
* Constructor.
**/
inline kernel::step::from_link_iterator::from_link_iterator(const kernel::step::from_link_iterator& iterator, bool(kernel::link::*method)() const)
:    _ref_from_link{nullptr}
,    _prev_from_link{nullptr}
,    _next_from_link{nullptr}
,    _iter_to_step{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
        __init__(iterator._iter_to_step, iterator._method, iterator._ref_from_link);
        
}



inline kernel::level* const& kernel::step::get_ref_level() const
{
    return _ref_level;
}


inline kernel::level* kernel::step::get_level() const
{
    return _ref_level;
}


inline kernel::level* kernel::step::get_active_level() const
{
    return _ref_active_level;
}


inline kernel::symbol* const& kernel::step::get_ref_symbol() const
{
    return _ref_symbol;
}


inline kernel::symbol* kernel::step::get_symbol() const
{
    return _ref_symbol;
}

#endif //(INCLUDE_INLINES_STEP)
#endif //INCLUDE_INLINES


