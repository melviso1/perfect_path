/**
BSD 2-Clause License

Copyright (c) 2023, Melchor Viso
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#if !defined(SYMBOL_H_INCLUDED)
#define SYMBOL_H_INCLUDED


// {user.before.class symbol.begin}
// {user.before.class symbol.end}

namespace kernel{

class symbol : public serial::runtime_instance<symbol>
{

    // {user.inside.first.class symbol.begin}
    // {user.inside.first.class symbol.end}

    public:
    // {user.before.class step_iterator.begin}
    // {user.before.class step_iterator.end}


    class step_iterator 
    {

        // {user.inside.first.class step_iterator.begin}
        // {user.inside.first.class step_iterator.end}

        friend class ::kernel::symbol;
        kernel::step* _ref_step ;
        kernel::step* _prev_step ;
        kernel::step* _next_step ;
        const kernel::symbol* _iter_symbol ;
        kernel::symbol::step_iterator* _prev ;
        kernel::symbol::step_iterator* _next ;
        bool(kernel::step::*_method)() const ;
        void __init__(const kernel::symbol* iter_symbol, bool(kernel::step::*method)() const=nullptr, kernel::step* ref_step=nullptr);
        public:
        inline kernel::symbol::step_iterator& operator =(const kernel::symbol::step_iterator& iterator);
        inline kernel::step* operator ++();
        inline kernel::step* operator --();
        inline  operator kernel::step*();
        inline kernel::step* operator ->();
        inline kernel::step* get();
        inline void reset();
        inline bool is_first() const;
        inline bool is_last() const;
        protected:
        void check(kernel::step* item_step);
        void check(kernel::step* item_step, kernel::step* new_item_step);
        private:
        void __exit__();
        public:
        step_iterator(const kernel::symbol* iter_symbol, bool(kernel::step::*method)() const=nullptr, kernel::step* ref_step=nullptr);
        step_iterator(const kernel::symbol& iter_symbol, bool(kernel::step::*method)() const=nullptr, kernel::step* ref_step=nullptr);
        step_iterator(const kernel::symbol::step_iterator& iterator, bool(kernel::step::*method)() const=nullptr);
        virtual ~step_iterator();
        // {user.inside.last.class step_iterator.begin}
        // {user.inside.last.class step_iterator.end}
    };

    // {user.after.class step_iterator.begin}
    // {user.after.class step_iterator.end}

    friend class ::kernel::step;
    friend class ::kernel::symbol::step_iterator;
    private:
    void __exit__();
    kernel::step* _first_step ;
    size_t _count_step ;
    protected:
    mutable step_iterator* _first_step_iterator ;
    mutable step_iterator* _last_step_iterator ;
    public:
    void add_step(kernel::step* item);
    void remove_step(kernel::step* item);
    void remove_all_step();
    void delete_all_step();
    void replace_step(kernel::step* item, kernel::step* new_item);
    kernel::step* get_first_step() const;
    kernel::step* get_last_step() const;
    kernel::step* get_next_step(kernel::step* pos) const;
    kernel::step* get_prev_step(kernel::step* pos) const;
    size_t get_step_count() const;
    kernel::step* find_step(kernel::level* const& value) const;
    symbol(const serial::type_record* _);
    virtual void save(serial::oxstream& os) const;
    virtual void save_contents(serial::oxstream& os) const;
    virtual void save_relations(serial::oxstream& os) const;
    virtual void load(serial::ixstream& is);
    virtual void load_contents(serial::ixstream& is);
    virtual void load_relations(serial::ixstream& is);
    symbol();
    virtual ~symbol();
    kernel::step* associated_step(kernel::level* p_level);
    // {user.inside.last.class symbol.begin}
    // {user.inside.last.class symbol.end}
};

}  // end namespace kernel

// {user.after.class symbol.begin}
// {user.after.class symbol.end}

#endif //SYMBOL_H_INCLUDED

#if defined(INCLUDE_INLINES)
#if !defined(INCLUDE_INLINES_SYMBOL)
#define INCLUDE_INLINES_SYMBOL



inline kernel::symbol::step_iterator& kernel::symbol::step_iterator::operator =(const kernel::symbol::step_iterator& iterator)
{
    
        __exit__();
        __init__(iterator._iter_symbol, iterator._method, iterator._ref_step);
        return *this;
        
}


inline kernel::step* kernel::symbol::step_iterator::operator ++()
{
    
        _next_step = _iter_symbol->get_next_step(_next_step);
        if ( _method != nullptr )
        {
            while (_next_step != nullptr && !(_next_step->*_method)())
            {
                _next_step = _iter_symbol->get_next_step(_next_step);
            }
        }
        _ref_step = _prev_step = _next_step;
        return _ref_step;
        
}


inline kernel::step* kernel::symbol::step_iterator::operator --()
{
    
        _prev_step = _iter_symbol->get_prev_step(_prev_step);
        if (_method != 0)
        {
            while (_prev_step && !(_prev_step->*_method)())
            {
                _prev_step = _iter_symbol->get_prev_step(_prev_step);
            }
        }
        _ref_step = _next_step = _prev_step;
        return _ref_step;
        
}


inline  kernel::symbol::step_iterator::operator kernel::step*()
{
    
        return _ref_step;
        
}


inline kernel::step* kernel::symbol::step_iterator::operator ->()
{
    
        return _ref_step;
        
}


inline kernel::step* kernel::symbol::step_iterator::get()
{
    
        return _ref_step;
        
}


inline void kernel::symbol::step_iterator::reset()
{
    
    _ref_step = _prev_step = _next_step = nullptr;
}


inline bool kernel::symbol::step_iterator::is_first() const
{
    
        return (_iter_symbol->get_first_step() == _ref_step);
        
}


inline bool kernel::symbol::step_iterator::is_last() const
{
    
        return (_iter_symbol->get_last_step() == _ref_step);
        
}
/**
* Constructor.
**/
inline kernel::symbol::step_iterator::step_iterator(const kernel::symbol* iter_symbol, bool(kernel::step::*method)() const, kernel::step* ref_step)
:    _ref_step{nullptr}
,    _prev_step{nullptr}
,    _next_step{nullptr}
,    _iter_symbol{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
         __init__(iter_symbol, method, ref_step);
        
}

/**
* Constructor.
**/
inline kernel::symbol::step_iterator::step_iterator(const kernel::symbol& iter_symbol, bool(kernel::step::*method)() const, kernel::step* ref_step)
:    _ref_step{nullptr}
,    _prev_step{nullptr}
,    _next_step{nullptr}
,    _iter_symbol{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
             __init__(&iter_symbol, method, ref_step);
            
}

/**
* Constructor.
**/
inline kernel::symbol::step_iterator::step_iterator(const kernel::symbol::step_iterator& iterator, bool(kernel::step::*method)() const)
:    _ref_step{nullptr}
,    _prev_step{nullptr}
,    _next_step{nullptr}
,    _iter_symbol{nullptr}
,    _prev{nullptr}
,    _next{nullptr}
,    _method{nullptr}
{
    
        __init__(iterator._iter_symbol, iterator._method, iterator._ref_step);
        
}


#endif //(INCLUDE_INLINES_SYMBOL)
#endif //INCLUDE_INLINES


